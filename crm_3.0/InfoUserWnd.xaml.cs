﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0
{
    /// <summary>
    /// Логика взаимодействия для InfoUserWnd.xaml
    /// </summary>
    public partial class InfoUserWnd : Window
    {
        string search = string.Empty;
        InfoUser info ;
        public InfoUserWnd(InfoUser info)
        {
            InitializeComponent();
           
            this.info = info;
            manager.Text = info.manager == null? "Нет данных" : info.manager;
            name.Text = info.name == null ? "Нет данных" : info.name;
            phone.Text = info.phone  == null ? "Нет данных" : info.phone;
            email.Text = info.email  == null ?  "Нет данных": info.email;
            gridCourse.ItemsSource = info.courseInfoUsers;
            gridCourse.Items.Refresh();

        }

      
    }
}
