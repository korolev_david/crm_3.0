﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0
{
    /// <summary>
    /// Логика взаимодействия для ScoreReag.xaml
    /// </summary>
    public partial class OrderEdit : Window
    {
        private List<Product> products;
        private OrderProduct orderProducts; 
        private DataBase dataBase;
        private Orders order;
        Manager manager;
        MTask task;
        public OrderEdit(Manager mng, MTask mTask)
        {
            InitializeComponent();
            dataBase = new DataBase(mng.idManager);
            manager = mng;
            task = mTask;
        }

        private async void Window_ContentRendered(object sender, EventArgs e)
        {
            await Task.Run(() => { products = dataBase.GetProduct(); });
            listProduct.ItemsSource = products;
            await Task.Run(() => { orderProducts = dataBase.getOrderReg(task.idOrder); });
            if (orderProducts.products == null) orderProducts.products = new List<Product>();
            listScoreProduct.ItemsSource = orderProducts.products.FindAll(x => x.drop == false);
            await Task.Run(() => { order = dataBase.getOrder(task.idOrder); });
            orderInfo.Items.Add(order);

            string[] data = Regex.Split(task.fileterData, "<data>");
            string[] email = Array.FindAll(data, x => x.Contains("@"));
            if (Array.FindAll(email, x => x.Contains(";")).Length > 0)
            {
                string dopEmail = email[Array.FindIndex(email, x => x.Contains(";"))];
                email = Array.FindAll(email, x => !x.Contains(";"));
                string[] dopEmailArr = dopEmail.Split(';');
                foreach (string item in dopEmailArr)
                    if (item != string.Empty) emailBox.Items.Add(item);


            }
            foreach (string item in email)
                if (item != string.Empty) emailBox.Items.Add(item);
           
            toPay.Text = String.Format("К оплате: {0}", orderProducts.products.FindAll(x => x.drop == false).Select(x => x.cost - (x.cost * x.sale / 100)).Sum());
        }

        private void listProduct_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listProduct.SelectedItem != null)
            {
                if(listScoreProduct.Items.Count ==0)
                {
                    var select = (Product)listProduct.SelectedItem;
                    select.add = true;
                    orderProducts.products.Add(select);
                    listScoreProduct.ItemsSource = orderProducts.products.FindAll(x => x.drop == false);
                    listScoreProduct.Items.Refresh();
                    toPay.Text = String.Format("К оплате: {0}", orderProducts.products.FindAll(x => x.drop == false).Select(x => x.cost - (x.cost * x.sale / 100)).Sum());
                }
                
            }
        }

        private void listScoreProduct_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listScoreProduct.SelectedItem != null)
            {
                if (order.payment > 0 )
                {
                    var select = (Product)listScoreProduct.SelectedItem;
                    if (select.add == true)
                    {
                        orderProducts.products.Remove(select);
                    }
                }
                else if( order.payment == 0 )
                {
                    var select = (Product)listScoreProduct.SelectedItem;
                    if (select.add == false)
                    {
                        if(select.idProduct == 0)
                        {
                            orderProducts.products.Remove(select);
                        }
                        select.drop = true; 
                       
                    }
                    else
                    {
                        orderProducts.products.Remove(select);
                    }
                }
                listScoreProduct.ItemsSource = orderProducts.products.FindAll(x => x.drop == false);
                listScoreProduct.Items.Refresh();
                toPay.Text = String.Format("К оплате: {0}", orderProducts.products.FindAll(x=>x.drop == false).Select(x => x.cost - (x.cost * x.sale / 100)).Sum());
               
            }
        }

        private void saleSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (listScoreProduct.SelectedItem != null)
            {
                var select = (Product)listScoreProduct.SelectedItem;
                if(select.add)
                {
                    select.sale = (int)saleSlider.Value;
                    if (select.add == false)
                    {
                        select.reg_sale = true;
                    }

                    listScoreProduct.Items.Refresh();
                    toPay.Text = String.Format("К оплате: {0}", orderProducts.products.FindAll(x => x.drop == false).Select(x => x.cost - (x.cost * x.sale / 100)).Sum());
                }
                else
                {
                    saleSlider.Value = 0;
                }
            }
            else
            {
                saleSlider.Value = 0;
            }
        }

        private async void editOrder_Click(object sender, RoutedEventArgs e)
        {
            if(listScoreProduct.Items.Count == 0)
            {
                MessageBox.Show("Один из продуктов должен быть выбран");
            }
            else
            {
                bool check = false;

                await Task.Run(() => { check = dataBase.editOrder(orderProducts); });
                if (!check) MessageBox.Show("Ошибка данные не внесены");

                this.Close();
            }
                
          
        }

        private void listScoreProduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listScoreProduct.SelectedItem != null)
            {
                var sel = (Product)listScoreProduct.SelectedItem;
                saleSlider.Value = sel.sale;
            }
        }

        private void removeListItem_Click(object sender, RoutedEventArgs e)
        {
            listScoreProduct_MouseDoubleClick(null, null);
        }

        private void AddListItem_Click(object sender, RoutedEventArgs e)
        {
            listProduct_MouseDoubleClick(null, null);
        }

        private void SearchBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (SearchBox.Text != string.Empty)
            {
                listProduct.ItemsSource = products.FindAll(x => x.name.Contains(SearchBox.Text));
                listProduct.Items.Refresh();
            }
            else
            {
                listProduct.ItemsSource = products;
                listProduct.Items.Refresh();
            }
        }
    }
}
