﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using crm_3._0.PageElement;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace crm_3._0
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }
        public Manager manager;
        public List<MTask> mTasks;
        public List<MTask> productSearchList = new List<MTask>();
        private SortFilter sortFilter = new SortFilter();
        public List<WorkingLead> workingLeads = new List<WorkingLead>();
        DataBase dataBase = new DataBase(1101);
        public void setIdManager(int idManager)
        {
            dataBase = new DataBase(idManager);
        }
        private async void Window_ContentRendered(object sender, EventArgs e)
        {
            double version = 5.2;
            double check_vrs = await Task.Run(() => dataBase.get_version());
            if (check_vrs > version)
            {
                Process.Start(Assembly.GetExecutingAssembly().Location.Replace("SwiftCRM 3.0.exe", "Update-2.0.exe"));
                this.Close();
            }
            HendlerAuthorization hendler = new HendlerAuthorization();
            string check = hendler.GetFile();
            if (check != "false")
            {
               
                String[] substrings = check.Split(':');
                string login_ = substrings[0];
                string pass_ = substrings[1];
                await Task.Run(() => { manager = dataBase.inLogin(pass_, login_); });
                if (manager.idManager != 0)
                {
                    LoadDataTask();
                    LoadGrid.Visibility = Visibility.Collapsed;
                    mainGrid.Visibility = Visibility.Visible;
                    dataBase = new DataBase(manager.idManager);
                }
            }
            else
            {
                LoadGrid.Visibility = Visibility.Collapsed;
                inLoginGrid.Visibility = Visibility.Visible;
                inLoginFrame.Content = new InLogin(this);
                this.Width = 350;
                this.Height = 350;
                double screenHeight = SystemParameters.FullPrimaryScreenHeight;
                double screenWidth = SystemParameters.FullPrimaryScreenWidth;
                this.Top = (screenHeight - this.Height) / 0x00000002;
                this.Left = (screenWidth - this.Width) / 0x00000002;
            }

            notificationProcess = new NotificationProcess();
            timer.Interval = TimeSpan.FromSeconds(10);
            timer.Tick += NotificationTimerTick;
            timer.Start();

        }

       

        private async void LoadDataTask()
        {
            loadPanelData.Visibility = Visibility.Visible;
            DateTime from = (DateTime)dateTouchFrom.SelectedDate;
            DateTime before = (DateTime)dateTouchBefore.SelectedDate;
            await Task.Run(() => { mTasks = dataBase.GetMTasks(from, before, manager); });
            mainTaskData.ItemsSource = mTasks;
            mainTaskData.Items.Refresh();
          

            priorityCmbx.ItemsSource = mTasks.GroupBy(x => x.rating)
                        .Select(x => x.First())
                        .ToList();
            productSearchList = mTasks.GroupBy(x => x.nameProduct)
                        .Select(x => x.First())
                        .ToList();
            productSearch.ItemsSource = productSearchList;
            loadPanelData.Visibility = Visibility.Hidden;
        }
        public async void LoadDataTask(DateTime dateFrom, DateTime dateBefor)
        {
            loadPanelData.Visibility = Visibility.Visible;
            DateTime from = dateFrom;
            DateTime before = dateBefor;
            await Task.Run(() => { mTasks = dataBase.GetMTasks(from, before, manager); });
            mainTaskData.ItemsSource = mTasks;
            mainTaskData.Items.Refresh();
        

            priorityCmbx.ItemsSource = mTasks.GroupBy(x => x.rating)
                        .Select(x => x.First())
                        .ToList();
            productSearchList = mTasks.GroupBy(x => x.nameProduct)
                        .Select(x => x.First())
                        .ToList();
            productSearch.ItemsSource = productSearchList;
            loadPanelData.Visibility = Visibility.Hidden;
        }

        private void FilterBut_Click(object sender, RoutedEventArgs e)
        {
            if (FilterData.Visibility == Visibility.Collapsed)
            {
                FilterData.Visibility = Visibility.Visible;
               
            }
            else if (FilterBut.Visibility == Visibility.Visible)
            {
                FilterData.Visibility = Visibility.Collapsed;
               
            }
        }
        public void NextAuthorization(Manager manager)
        {
            this.manager = manager;
            this.Width = 1100;
            this.Height = 700;
            double screenHeight = SystemParameters.FullPrimaryScreenHeight;
            double screenWidth = SystemParameters.FullPrimaryScreenWidth;
            this.Top = (screenHeight - this.Height) / 0x00000002;
            this.Left = (screenWidth - this.Width) / 0x00000002;
            inLoginFrame.Content = null;
            inLoginGrid.Visibility = Visibility.Collapsed;
            mainGrid.Visibility = Visibility.Visible;
            if (mainTaskData.SelectedItem != null)
            {
                var selItem = (MTask)mainTaskData.SelectedItem;
                rigth_menu.Content = new TaskPage(selItem,this);
            }
        }
        private void mainTaskData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mainTaskData.SelectedItem != null)
            {
                var selItem = (MTask)mainTaskData.SelectedItem;
                rigth_menu.Content = new TaskPage(selItem, this);
            }
            else
            {
                rigth_menu.Content = null;
            }
           
        }

        private void search_GotFocus(object sender, RoutedEventArgs e)
        {
            if (search.Text == "Ф.И.О, email, телефон...")
            {
                search.Text = string.Empty;
            }
        }

        private void search_LostFocus(object sender, RoutedEventArgs e)
        {
            if (search.Text == string.Empty)
            {
                search.Text = "Ф.И.О, email, телефон...";
            
            }
        }

        private void search_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (search.Text == "Ф.И.О, email, телефон...")
            {
                search.Text = string.Empty;
            }
        }

        private void search_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            Fillter();

        }

        private void typePosition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (typePosition.SelectedIndex != -1)
            {
                Fillter();
            }
        }

        private void searchProduct_txt_LostFocus(object sender, RoutedEventArgs e)
        {
            if (searchProduct_txt.Text == string.Empty)
            {
                searchProduct_txt.Text = "Поиск по продукту...";
            }
        }

        private void searchProduct_txt_GotFocus(object sender, RoutedEventArgs e)
        {
            if (searchProduct_txt.Text == "Поиск по продукту...")
            {
                searchProduct_txt.Text = string.Empty;
            }
        }

     

        private void searchProduct_txt_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (searchProduct_txt.Text != string.Empty)
            {
                productSearch.ItemsSource = productSearchList.FindAll(x => x.nameProduct.Contains(searchProduct_txt.Text));
            }
            else
            {
                productSearch.ItemsSource = productSearchList;
            }
            
        }

        private void searchProduct_txt_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (searchProduct_txt.Text == string.Empty)
            {
                searchProduct_txt.Text = "Поиск по продукту...";
            }
        }

        private void productSearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Fillter();
        }

        private void searchThems_LostFocus(object sender, RoutedEventArgs e)
        {
            if (searchThems.Text == string.Empty)
            {
                searchThems.Text = "Тема";
            }
        }

        private void searchIdTask_LostFocus(object sender, RoutedEventArgs e)
        {
            if (searchIdTask.Text == string.Empty)
            {
                searchIdTask.Text = "Номер задачи";
            }
        }

        private void searchThems_GotFocus(object sender, RoutedEventArgs e)
        {
            if (searchThems.Text == "Тема")
            {
                searchThems.Text = string.Empty;
            }
        }

        private void searchIdTask_GotFocus(object sender, RoutedEventArgs e)
        {
            if (searchIdTask.Text == "Номер задачи")
            {
                searchIdTask.Text = string.Empty;
            }
        }

        private void searchThems_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (searchThems.Text == string.Empty)
            {
                searchThems.Text = "Тема";
            }
        }

        private void searchIdTask_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (searchIdTask.Text == string.Empty)
            {
                searchIdTask.Text = "Номер задачи";
            }
        }


        private void Fillter()
        {
            string product = string.Empty;
            string them = string.Empty;
            int numberTask = -1;
            int priority = -1;
            int stage = -1;
            string global = string.Empty;
            if (productSearch.SelectedIndex != -1)
            {
                product = ((MTask)productSearch.SelectedItem).nameProduct;
            }
            if (searchThems.Text != "Тема")
            {
                them = searchThems.Text;
            }
            if (typePosition.SelectedIndex != -1)
            {
                ComboBoxItem item = (ComboBoxItem)typePosition.ItemContainerGenerator.ContainerFromItem(typePosition.SelectedItem);
                stage = Convert.ToInt32(item.Tag.ToString());
            }
            if (priorityCmbx.SelectedIndex != -1)
            {
                
                priority = ((MTask)priorityCmbx.SelectedItem).rating;
            }
            if (searchIdTask.Text != "Номер задачи" && searchIdTask.Text != string.Empty)
            {
                numberTask = Convert.ToInt32(searchIdTask.Text);
            }
            if(search.Text != "Ф.И.О, email, телефон...")
            {
                global = search.Text;
            }
            mainTaskData.ItemsSource = sortFilter.GetSortTask(product, them, numberTask, priority, stage, mTasks, global);
            mainTaskData.Items.Refresh();

            rigth_menu.Content = null;
            mainTaskData.SelectedIndex = -1;

        
        }

        private void searchIdTask_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        private void priorityCmbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Fillter();
        }

        private void searchIdTask_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            string str =  searchIdTask.Text;
            int value;
            if (int.TryParse(string.Join("", str.Where(c => char.IsDigit(c))), out value))
            {
                searchIdTask.Text = value.ToString();
                Fillter();
            }
            else
            {
                searchIdTask.Text = string.Empty;
            }
         
        }

        private void searchThems_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            Fillter();
        }

        private void dropFilter_Click(object sender, RoutedEventArgs e)
        {
            productSearch.SelectedIndex = -1;
            priorityCmbx.SelectedIndex = -1;
            searchThems.Text = "Тема";
            searchIdTask.Text = "Номер задачи";
            typePosition.SelectedIndex = -1;
            mainTaskData.ItemsSource = mTasks;
            mainTaskData.Items.Refresh();
        }

        private void dateTouchFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (manager != null && mTasks != null)
            {
                //DateTime dateFrom = ()
                if (((DateTime)dateTouchFrom.SelectedDate).Date < DateTime.Now.Date)
                {
                    dateTouchFrom.SelectedDate = DateTime.Now;
                }
                else
                {
                    LoadDataTask();
                }
            }
           
        }

        private void dateTouchBefore_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (manager != null && mTasks != null)
            {
                if (((DateTime)dateTouchBefore.SelectedDate).Date < DateTime.Now.Date)
                {
                    dateTouchBefore.SelectedDate = DateTime.Now;
                }
                else
                {
                    LoadDataTask();
                }
            }
           
        }

        private void refreshData_Click(object sender, RoutedEventArgs e)
        {
            LoadDataTask();
        }

        private void courseList_Click(object sender, RoutedEventArgs e)
        {
            CourseTreeViewWnd crs = new CourseTreeViewWnd();
            crs.ShowDialog();
            crs.Close();
        }

        private void TaskList_Click(object sender, RoutedEventArgs e)
        {
            TaskWnd taskWnd = new TaskWnd(1, manager);
            taskWnd.ShowDialog();
            taskWnd.Close();
        }
        #region notification
        NotificationHendler notificationH;
        public NotificationProcess notificationProcess;
        DispatcherTimer timer = new DispatcherTimer();

        private void closeNotif_Click(object sender, RoutedEventArgs e)
        {
            notification.BeginAnimation(HeightProperty, GetDoubleAnimation(150, 0,0.5));
            notification.BeginAnimation(WidthProperty, GetDoubleAnimation(300, 0,0.5));
            notificationProcess.RemoveNotificaion(notificationH);
            timer.Start();
        }

        DoubleAnimation GetDoubleAnimation(double start, double end, double time)
        {
            DoubleAnimation anim = new DoubleAnimation();
            anim.From = start;
            anim.To = end;
            anim.Duration = TimeSpan.FromSeconds(time);
            anim.EasingFunction = new QuadraticEase();
            return anim;
        }

        private void NotificationTimerTick(object sender, EventArgs e)
        {
            notificationH = notificationProcess.GetNotification();
            if (notificationH != null)
            {
                SoundPlayer player = new SoundPlayer();
                player.Stream = Properties.Resources.notificationSong;
                player.Play();
               
                numberTaskNf.Text = String.Format("Напоминание задача № {0}", notificationH.idTask);
                infoTaskNf.Text = notificationH.info;
                notification.BeginAnimation(HeightProperty, GetDoubleAnimation(0, 150, 0.5));
                notification.BeginAnimation(WidthProperty, GetDoubleAnimation(0, 300, 0.5));
                notification.BeginAnimation(OpacityProperty, GetDoubleAnimation(0, 1, 1.5));
                timer.Stop();
            }
        }

        #endregion

        private void ShowCastomere_Click(object sender, RoutedEventArgs e)
        {
            CastomereTask castomereTask = new CastomereTask(manager);
            castomereTask.ShowDialog();
            castomereTask.Close();
        }

        private void ShowCalendar_Click(object sender, RoutedEventArgs e)
        {
            var oldWindow = Application.Current.Windows.OfType<Сalendar>().FirstOrDefault();
            if (oldWindow != null)
            {
                oldWindow.Close();
            }
            Сalendar calendar = new Сalendar(manager.idManager, this);
            calendar.Show();
            
        }

        private async void globalSearch_Click(object sender, RoutedEventArgs e)
        {
        
            if (search.Text != string.Empty && search.Text != "Ф.И.О, email, телефон...")
            {
                string searchSend = search.Text;
                object getItem = null;
                DataBase dataBase = new DataBase(manager.idManager);
                await Task.Run(() => { getItem = dataBase.GetMenagerUser(searchSend, manager.idManager); });
                if (getItem.GetType() == typeof( InfoUser))
                {
                    InfoUserWnd infoUserWnd = new InfoUserWnd((InfoUser)getItem);
                    infoUserWnd.Show();

                }
                else if((getItem.GetType() == typeof(List<MTask>)))
                {
                    dropFilter_Click(null, null);
                    loadPanelData.Visibility = Visibility.Visible;
                    mTasks = (List<MTask>)getItem;
                    mainTaskData.ItemsSource = mTasks;
                    mainTaskData.Items.Refresh();
                 

                    priorityCmbx.ItemsSource = mTasks.GroupBy(x => x.rating)
                                .Select(x => x.First())
                                .ToList();
                    productSearchList = mTasks.GroupBy(x => x.nameProduct)
                                .Select(x => x.First())
                                .ToList();
                    productSearch.ItemsSource = productSearchList;
                    loadPanelData.Visibility = Visibility.Hidden;

                }
            }
        }
    }
}
