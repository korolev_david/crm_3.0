﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0
{
    /// <summary>
    /// Логика взаимодействия для MessageList.xaml
    /// </summary>
    public partial class MessageList : Window
    {
        MTask TaskUser;
        DataBase dataBase ;
        ObservableCollection<MessageTask> tsMessage;
        int idWnd = 0;
        string nameManager = string.Empty;
        public MessageList(int idWnd, MTask mTask,Manager manager, string nameManager, ObservableCollection<MessageTask> tsMessage)
        {
            
            InitializeComponent();
            dataBase =  new DataBase(manager.idManager);
            TaskUser = mTask;
            SetList();
            if (idWnd == 1)
            {
                gridMessage.ItemsSource = listCloseTask;
            }
            if (idWnd == 2)
            {
                gridMessage.Visibility = Visibility.Hidden;
                sendMsg.Visibility = Visibility.Hidden;
                info.Visibility = Visibility.Visible;
                next.Visibility = Visibility.Visible;
            }

            this.idWnd = idWnd;
            this.nameManager = nameManager;
            this.tsMessage = tsMessage;
        }
       
        List<SavedMessages> listCloseTask = new List<SavedMessages>();
        List<SavedMessages> listNextDate = new List<SavedMessages>();
        private void SetList()
        {
            listCloseTask.Add(new SavedMessages { name = "Другой продукт", change_id = 1});
            listCloseTask.Add(new SavedMessages { name = "Не будет учиться", change_id = 2 });
            listCloseTask.Add(new SavedMessages { name = "Неверные данные", change_id = 3 });
            listCloseTask.Add(new SavedMessages { name = "Нет денег", change_id = 4 });
            listCloseTask.Add(new SavedMessages { name = "Агрессивный отказ", change_id = 5 });
            listCloseTask.Add(new SavedMessages { name = "Неактуально", change_id = 7 });
            listCloseTask.Add(new SavedMessages { name = "Недозвон", change_id = 8 });
            listCloseTask.Add(new SavedMessages { name = "Не распределен", change_id = 13 });


            listNextDate.Add(new SavedMessages { name = "Приоритетные задачи", change_id = 10 });
            listNextDate.Add(new SavedMessages { name = "Не успел", change_id = 11 });
        }

        private async void sendMsg_Click(object sender, RoutedEventArgs e)
        {
            if (idWnd == 1 && gridMessage.SelectedItem != null)
            {
                var itemSel = (SavedMessages)gridMessage.SelectedItem;
                bool answer = false;
                await Task.Run(() => { answer = dataBase.SendMessageUser(TaskUser.idUser,"Закрытие задачи: "+ itemSel.name+". Продукт: "+TaskUser.nameProduct, 128, nameManager); });
                await Task.Run(() => { dataBase.SetChange(TaskUser.idTask,itemSel.change_id); });
                if (answer)
                {
                    TaskUser.flagClose = true;
                    this.Close();
                }
               
            }
            else if (idWnd == 2 && gridMessage.SelectedItem != null)
            {
                var itemSel = (SavedMessages)gridMessage.SelectedItem;
                int idMs = -1;
                await Task.Run(() => { idMs = dataBase.SetMessageTask(TaskUser.idTask, "Перенос задачи: "+itemSel.name, nameManager); });
                await Task.Run(() => { dataBase.SetChange(TaskUser.idTask, itemSel.change_id); });
                if (idMs != -1)
                {
                    tsMessage.Add(new MessageTask { date = DateTime.Now, id_ms = idMs, message = "Перенос задачи: " + itemSel.name, name = nameManager });
                    TaskUser.flagProcess = true;
                    this.Close();
                }

            }
            else
            {
                MessageBox.Show("Выберите сообщение");
            }
        }

        private async void next_Click(object sender, RoutedEventArgs e)
        {
            int idMs = -1;
            await Task.Run(() => { idMs = dataBase.SetMessageTask(TaskUser.idTask, "Перенос задачи: Не успел", nameManager); });
            await Task.Run(() => { dataBase.SetChange(TaskUser.idTask, 11); });
            if (idMs != -1)
            {
                tsMessage.Add(new MessageTask { date = DateTime.Now, id_ms = idMs, message = "Перенос задачи: Не успел", name = nameManager });
                TaskUser.flagProcess = true;
                this.Close();
            }
        }
    }

   
}
