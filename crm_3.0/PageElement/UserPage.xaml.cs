﻿using crm_3._0.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0.PageElement
{
    /// <summary>
    /// Логика взаимодействия для UserPage.xaml
    /// </summary>
    public partial class UserPage : Page
    {
        MainWindow mainWnd;
        public UserPage(MainWindow wnd)
        {
            InitializeComponent();
            mainWnd = wnd;
            if (mainWnd.mainTaskData.SelectedItem != null)
            {
                var selItem = (MTask)mainWnd.mainTaskData.SelectedItem;
                ReplacingFrame.Content = new СommentR(selItem.idUser, mainWnd.manager.ManagerName, mainWnd.manager);
            }
           
        }
        private void SelectButton()
        {
            Button[] btn = { BackTaskMenu, CommentMenuBut, InfoUserMenuBut, ProductMenuBut, ScoreMenuBut, UrlMenuBut, AddTaskBut };
            foreach (Button elm in btn)
            {
                elm.Tag = 1.ToString();
            }
        }
        private void BackTaskMenu_Click(object sender, RoutedEventArgs e)
        {
            if(mainWnd.mainTaskData.SelectedItem != null)
            {
                mainWnd.rigth_menu.Content = null;
                mainWnd.rigth_menu.NavigationService.RemoveBackEntry();
                var selItem = (MTask)mainWnd.mainTaskData.SelectedItem;
                mainWnd.rigth_menu.Content = new TaskPage(selItem, mainWnd);
            }
          

        }

        private void CommentMenuBut_Click(object sender, RoutedEventArgs e)
        {
            var selItem = (MTask)mainWnd.mainTaskData.SelectedItem;
            ReplacingFrame.Content = new СommentR(selItem.idUser, mainWnd.manager.ManagerName, mainWnd.manager);
            SelectButton();
            CommentMenuBut.Tag = 2.ToString();
        }

        private void InfoUserMenuBut_Click(object sender, RoutedEventArgs e)
        {
            var selItem = (MTask)mainWnd.mainTaskData.SelectedItem;
            ReplacingFrame.Content = new InfoUserR(selItem.idUser, mainWnd);
            SelectButton();
            InfoUserMenuBut.Tag = 2.ToString();
        }

        private void ProductMenuBut_Click(object sender, RoutedEventArgs e)
        {
            var selItem = (MTask)mainWnd.mainTaskData.SelectedItem;
            ReplacingFrame.Content = new ProductR(selItem.idUser, mainWnd.manager);
            SelectButton();
            ProductMenuBut.Tag = 2.ToString();
        }

        private void ScoreMenuBut_Click(object sender, RoutedEventArgs e)
        {
            var selItem = (MTask)mainWnd.mainTaskData.SelectedItem;
            ReplacingFrame.Content = new OrderR(mainWnd, selItem.idUser);
            SelectButton();
            ScoreMenuBut.Tag = 2.ToString();
        }

        private void UrlMenuBut_Click(object sender, RoutedEventArgs e)
        {
            var selItem = (MTask)mainWnd.mainTaskData.SelectedItem;
            ReplacingFrame.Content = new UrlR(selItem.idUser, mainWnd.manager);
            SelectButton();
            UrlMenuBut.Tag = 2.ToString();
        }

        private void AddTaskBut_Click(object sender, RoutedEventArgs e)
        {
            var selItem = (MTask)mainWnd.mainTaskData.SelectedItem;
            Assistance assistance = new Assistance(selItem, mainWnd.manager);
            assistance.ShowDialog();
        }
    }
}
