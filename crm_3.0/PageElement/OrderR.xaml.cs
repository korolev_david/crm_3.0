﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0.PageElement
{
    /// <summary>
    /// Логика взаимодействия для ScoreR.xaml
    /// </summary>
    public partial class OrderR : Page
    {
        MainWindow main;
        int idUser;
        DataBase dataBase ;
        List<Orders> orders = new List<Orders>();

        public OrderR(MainWindow mwn, int idUser)
        {
            InitializeComponent();
            dataBase = new DataBase(mwn.manager.idManager);
            main = mwn;
            this.idUser = idUser;
            Start();
        }

        private async void Start()
        {
            loadMini.Visibility = Visibility.Visible;
            await Task.Run(() => { orders = dataBase.GetOrders(idUser); });
            OrdersDataGrid.ItemsSource = orders;

            var selItem = (MTask)main.mainTaskData.SelectedItem;
            string[] data = Regex.Split(selItem.fileterData, "<data>");
            string[] email = Array.FindAll(data, x => x.Contains("@"));
          
            if (Array.FindAll(email, x => x.Contains(";")).Length > 0)
            {
                string dopEmail = email[Array.FindIndex(email, x => x.Contains(";"))];
                email = Array.FindAll(email, x => !x.Contains(";"));
                string[] dopEmailArr = dopEmail.Split(';');
                foreach (string item in dopEmailArr)
                    if (item != string.Empty) cmb_email.Items.Add(item);


            }
            foreach (string item in email)
                if (item != string.Empty) cmb_email.Items.Add(item);

            loadMini.Visibility = Visibility.Collapsed;
        }

        private async void addOrder_Click(object sender, RoutedEventArgs e)
        {
            var selItem = (MTask)main.mainTaskData.SelectedItem;
            OrderCreate order = new OrderCreate(main.manager, selItem, true);
            order.ShowDialog();
            await Task.Run(() => { orders = dataBase.GetOrders(idUser); });
            OrdersDataGrid.ItemsSource = orders;
            OrdersDataGrid.Items.Refresh();

        }

        private void sendMs_Click(object sender, RoutedEventArgs e)
        {
            if (OrdersDataGrid.SelectedIndex != -1 && cmb_email.SelectedIndex != -1)
            {
                var item =(Orders)OrdersDataGrid.SelectedItem;
                List<Product> products = new List<Product>();
                var selItem = (MTask)main.mainTaskData.SelectedItem;
                string [] items = item.product.Split(';');
                foreach (string name in items)
                {
                    products.Add(new Product { name = name });
                }
                SendMessage sendMessage = new SendMessage(item.idOrder, products, selItem, cmb_email.Items[cmb_email.SelectedIndex].ToString());
                sendMessage.SendMsOrders();
                sendMsPanel.Visibility = Visibility.Collapsed;
                cmb_email.SelectedIndex = -1;
                MessageBox.Show("Сообщение отправленно");


            }
            
        }

        private void cancelMs_Click(object sender, RoutedEventArgs e)
        {
            sendMsPanel.Visibility = Visibility.Collapsed;
        }

        private void sendMsBut_Click(object sender, RoutedEventArgs e)
        {
            if (OrdersDataGrid.SelectedIndex != -1)
            {
                sendMsPanel.Visibility = Visibility.Visible;
            }
        }

       
    }
}
