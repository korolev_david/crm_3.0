﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0.PageElement
{
    /// <summary>
    /// Логика взаимодействия для TaskPage.xaml
    /// </summary>
    public partial class TaskPage : Page
    {
        MTask TaskUser;
        MainWindow mainW;
        ObservableCollection<MessageTask> tsMessage;
        DataBase dataBase ;
        List<MTask> mTasks = new List<MTask>();
      
        public TaskPage(MTask taskUs, MainWindow main)
        {
            InitializeComponent();
            dataBase = new DataBase(main.manager.idManager);
            PaymentBut.Tag = 2.ToString();
            ScoreBut.Tag = 2.ToString();
            PresentationBut.Tag = 2.ToString();
            NeedsBut.Tag = 2.ToString();
            CallBut.Tag = 1.ToString();
            TaskUser = taskUs;
            mainW = main;
            
            SetParam();

        }
        private async void SetParam()
        {
            if (TaskUser.countTask != 0 && TaskUser.countTask != 1)
            {
                showTaskClient.Content = string.Format("Задач клиента: {0}", TaskUser.countTask);
            }
            else
            {
                showTaskClient.Visibility = Visibility.Collapsed;
            }
            titleBox.Text = TaskUser.title;
            productNameBox.Text = TaskUser.nameProduct;
            descriptionBox.Text = TaskUser.description;
            dateCreateBox.SelectedDate = TaskUser.date_creation;
            dateTochBox.SelectedDate = TaskUser.date_touch;
            numberTask.Text = TaskUser.idTask.ToString();
            rangStat.Text = TaskUser.rating.ToString();
            await Task.Run(() => {  tsMessage = dataBase.GetMessageTasks(TaskUser.idTask); });
            taskMessageLixtBox.ItemsSource = tsMessage;
            taskMessageLixtBox.Items.MoveCurrentToLast();
            taskMessageLixtBox.Items.Refresh();
            taskMessageLixtBox.ScrollIntoView(taskMessageLixtBox.Items.CurrentItem);
            FormationStage();
           
        }
     
        private async void SuccessfullyBut_Click(object sender, RoutedEventArgs e)
        {
            SuccessfullyBut.IsEnabled = false;
            int answer = -1;
            if (TaskUser.type < 3 || TaskUser.type == 5)
            {
                await Task.Run(() => { answer = dataBase.NextStage(TaskUser.idStage, TaskUser.idTask, TaskUser.type); });
            }
            else if (TaskUser.type == 3)
            {
                OrderCreate orderCreate = new OrderCreate(mainW.manager, TaskUser,false);
                orderCreate.ShowDialog();
                if (TaskUser.idOrder != -1)
                {
                    await Task.Run(() => { answer = dataBase.NextStage(TaskUser.idStage, TaskUser.idTask, TaskUser.type); });
                }
            }
            if (answer != -1)
            {
                if (answer == 0)
                {
                    mainW.mTasks.Remove(TaskUser);
                    mainW.mainTaskData.Items.Refresh();
                    mainW.rigth_menu.Content = null;
                    mainW.rigth_menu.NavigationService.RemoveBackEntry();
                    if (MessageBox.Show("Создать новую задачу у клиента?","Продолжить?", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        NextTask nextTask = new NextTask(mainW.manager.idManager, TaskUser.idProduct, TaskUser.idUser);
                        nextTask.ShowDialog();
                        nextTask.Close();
                    }
                }
                else
                {
                    int type = TaskUser.type;
                    if (type++ == 4)
                    {
                       
                    }
                    TaskUser.date_touch = DateTime.Now;
                    TaskUser.type++;
                    TaskUser.idStage = answer;
                    FormationStage();
                    mainW.mainTaskData.Items.Refresh();
                }
               
            }
            SuccessfullyBut.IsEnabled = true;

        }
        private void FormationStage()
        {
            Button[] buttons = { CallBut, NeedsBut, PresentationBut, ScoreBut, PaymentBut };
            for (int i = 0; i < TaskUser.type; i++)
            {
                buttons[i].Tag = 1;
            }
            if (TaskUser.type == 4 && TaskUser.idOrder != -1)
                createOrders.Visibility = Visibility.Visible;
        }

        private void ContentInfoBut_Click(object sender, RoutedEventArgs e)
        {
            mainW.rigth_menu.Content = new UserPage(mainW);
        }

        private async void textMessage_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                string text = textMessage.Text;
                int idMs =-1;
                await Task.Run(() => { idMs = dataBase.SetMessageTask(TaskUser.idTask,text,mainW.manager.ManagerName); });
                if (idMs != -1)
                {
                    tsMessage.Add(new MessageTask { id_ms = idMs, date = DateTime.Now, message = text, name = mainW.manager.ManagerName });
                    taskMessageLixtBox.Items.MoveCurrentToLast();
                    taskMessageLixtBox.Items.Refresh();
                    taskMessageLixtBox.ScrollIntoView(taskMessageLixtBox.Items.CurrentItem);
                    textMessage.Text = string.Empty;
                }

            }
        }

        private async void regeditThem_Click(object sender, RoutedEventArgs e)
        {
            if (regeditThem.Tag.ToString() == 0.ToString())
            {
                regeditThem.Tag = 1.ToString();
                titleBox.IsReadOnly = false;
            }
            else
            {
                regeditThem.Tag = 0.ToString();
                titleBox.IsReadOnly = true;
                if (TaskUser.title != titleBox.Text)
                {

                    bool checkAnswer = false;
                    string updateText = titleBox.Text;
                    await Task.Run(() => { checkAnswer = dataBase.UpdateTask(TaskUser.idTask, "title", updateText); });
                    if (!checkAnswer)
                    {
                        MessageBox.Show("Изменения не внесены");
                        titleBox.Text = TaskUser.title;
                    }
                    else
                    {
                        TaskUser.title = titleBox.Text;
                        mainW.mainTaskData.Items.Refresh();
                    }
                }
            }
        }

        private async void dateTochBox_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
           
            
            if(((DateTime)dateTochBox.SelectedDate).Date != TaskUser.date_touch.Date )
            {
                if (mainW.workingLeads.Find(x => x.idUser == TaskUser.idUser) != null)
                {
                    if (TaskUser.flagProcess || mainW.workingLeads.Find(x => x.idUser == TaskUser.idUser).callUser == true)
                    {
                        if (dateTochBox.SelectedDate >= DateTime.Now.Date)
                        {
                            bool checkAnswer = false;
                            DateTime updateDate = (DateTime)dateTochBox.SelectedDate;
                            await Task.Run(() => { checkAnswer = dataBase.UpdateStageTask(TaskUser.idStage, "date_touch", updateDate.ToString("yyyy-MM-dd")); });
                            if (!checkAnswer)
                            {
                                MessageBox.Show("Изменения не внесены");
                                dateTochBox.SelectedDate = TaskUser.date_touch;
                            }
                            else
                            {
                                TaskUser.date_touch = (DateTime)dateTochBox.SelectedDate;
                                if (mainW.mTasks.Find(x => x.idTask == TaskUser.idTask) != null)
                                {
                                    mainW.mTasks.Find(x => x.idTask == TaskUser.idTask).date_touch = (DateTime)dateTochBox.SelectedDate;
                                    mainW.mainTaskData.Items.Refresh();
                                }


                            }
                        }
                        else
                        {
                            dateTochBox.SelectedDate = DateTime.Now.Date;
                        }
                    }
                    else
                    {
                        bool checkAnswer = false;
                        DateTime updateDate = DateTime.Now;
                        updateDate = updateDate.AddDays(1);
                        await Task.Run(() => { checkAnswer = dataBase.UpdateStageTask(TaskUser.idStage, "date_touch", updateDate.ToString("yyyy-MM-dd")); });
                        if (!checkAnswer)
                        {
                            MessageBox.Show("Изменения не внесены");
                            dateTochBox.SelectedDate = TaskUser.date_touch;
                        }
                        else
                        {
                            TaskUser.date_touch = updateDate;
                            if (mainW.mTasks.Find(x => x.idTask == TaskUser.idTask) != null)
                            {
                                mainW.mTasks.Find(x => x.idTask == TaskUser.idTask).date_touch = updateDate;
                                mainW.mainTaskData.Items.Refresh();
                            }


                        }
                        dateTochBox.Text = updateDate.ToString("dd.MM.yyyy");

                    }
                }
                else
                {
                    
                    bool checkAnswer = false;
                    DateTime updateDate = DateTime.Now;
                    updateDate = updateDate.AddDays(1);
                    await Task.Run(() => { checkAnswer = dataBase.UpdateStageTask(TaskUser.idStage, "date_touch", updateDate.ToString("yyyy-MM-dd")); });
                    if (!checkAnswer)
                    {
                        MessageBox.Show("Изменения не внесены");
                        dateTochBox.SelectedDate = TaskUser.date_touch;
                    }
                    else
                    {
                        TaskUser.date_touch = updateDate;
                        if (mainW.mTasks.Find(x => x.idTask == TaskUser.idTask) != null)
                        {
                            mainW.mTasks.Find(x => x.idTask == TaskUser.idTask).date_touch = updateDate;
                            mainW.mainTaskData.Items.Refresh();
                        }


                    }
                    dateTochBox.Text = updateDate.ToString("dd.MM.yyyy");
                }
                //else
                //{
                //    DateTime updateDate = TaskUser.date_touch;
                //    MessageList messageList = new MessageList(2, TaskUser, mainW.manager.ManagerName, tsMessage);
                //    messageList.ShowDialog();
                //    messageList.Close();
                //    if (TaskUser.flagProcess == false)
                //    {
                //        dateTochBox.SelectedDate = updateDate;
                //    }
                //    else
                //    {
                //        taskMessageLixtBox.ItemsSource = tsMessage;
                //        taskMessageLixtBox.Items.MoveCurrentToLast();
                //        taskMessageLixtBox.Items.Refresh();
                //        taskMessageLixtBox.ScrollIntoView(taskMessageLixtBox.Items.CurrentItem);
                //        dateTochBox_SelectedDateChanged(null, null);
                //    }
                //}
                              
            }
            else
            {
                
                DateTime updateDate = TaskUser.date_touch;
                dateTochBox.SelectedDate = updateDate;
            }
        }

        private async void TaskClose_Click(object sender, RoutedEventArgs e)
        {
           
            MessageList messageList = new MessageList(1, TaskUser, mainW.manager, mainW.manager.ManagerName, tsMessage);
            messageList.ShowDialog();
            if(TaskUser.flagClose)
            {
                TaskClose.IsEnabled = false;
                bool answer = false;
                await Task.Run(() => { answer = dataBase.CloseTask(TaskUser.idTask); });
                if (answer)
                {
                    mTasks = (List<MTask>)mainW.mainTaskData.ItemsSource;
                    if (mTasks.Find(x => x.idTask == TaskUser.idTask) != null)
                    {
                        mTasks.Remove(mTasks.Find(x => x.idTask == TaskUser.idTask));
                    }
                    if (mainW.mTasks.Find(x => x.idTask == TaskUser.idTask) != null)
                    {
                        mainW.mTasks.Remove(mainW.mTasks.Find(x => x.idTask == TaskUser.idTask));
                    }
                    mainW.mainTaskData.Items.Refresh();
                    mainW.rigth_menu.Content = null;
                    mainW.rigth_menu.NavigationService.RemoveBackEntry();
                }
                TaskClose.IsEnabled = true;
            }
          
        }

        private void createOrders_Click(object sender, RoutedEventArgs e)
        {
            OrderEdit score = new OrderEdit(mainW.manager, TaskUser);
            score.Show();
        }

        private void callUser_Click(object sender, RoutedEventArgs e)
        {
            var oldWindow = Application.Current.Windows.OfType<PhoneIP>().FirstOrDefault();
            if (oldWindow != null)
            {
                oldWindow.CallDown_Click(null,null);
            }
            PhoneIP phoneIP = new PhoneIP(mainW.manager, TaskUser.fileterData, TaskUser, mainW);
            phoneIP.Show();
        }

        private async void sendMessageTask_Click(object sender, RoutedEventArgs e)
        {
            string text = textMessage.Text;
            int idMs = -1;
            await Task.Run(() => { idMs = dataBase.SetMessageTask(TaskUser.idTask, text, mainW.manager.ManagerName); });
            if (idMs != -1)
            {
                tsMessage.Add(new MessageTask { id_ms = idMs, date = DateTime.Now, message = text, name = mainW.manager.ManagerName });
                taskMessageLixtBox.Items.MoveCurrentToLast();
                taskMessageLixtBox.Items.Refresh();
                taskMessageLixtBox.ScrollIntoView(taskMessageLixtBox.Items.CurrentItem);
                textMessage.Text = string.Empty;
            }
        }

        private async void showTaskClient_Click(object sender, RoutedEventArgs e)
        {
            if (TaskUser != null)
            {
              
               await Task.Run(() => {mTasks = dataBase.GetMTasksUser(TaskUser.idUser); });
               if (TaskUser.flagProcess)
               {
                    mTasks.ForEach(x => x.flagProcess = true);
               }
               mainW.mainTaskData.ItemsSource = mTasks;
               mainW.mainTaskData.Items.Refresh();
            }
        }

        private void hourNotification_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            bool check;
            e.Handled = check = "0123456789".IndexOf(e.Text) < 0;
            if (!check)
            {
                try
                {
                    string checkString = hourNotification.Text + e.Text;
                    if (checkString != string.Empty)
                    {
                        int hour = Convert.ToInt32(checkString);
                        if (hour > 24)
                        {
                            hourNotification.Text = string.Empty;
                        }

                    }
                  
                }
                catch
                {
                    MessageBox.Show("Некорректно введен час.");
                    hourNotification.Text = "";
                }
              

            }
        }

        private void minetsNotification_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            bool check;
            e.Handled = check = "0123456789".IndexOf(e.Text) < 0;
           
            if (!check)
            {
                try
                {
                    string checkString = minetsNotification.Text + e.Text;
                    if (checkString != string.Empty)
                    {
                        int hour = Convert.ToInt32(checkString);
                        if (hour > 59)
                        {
                            minetsNotification.Text = string.Empty;
                        }

                    }

                }
                catch
                {
                    MessageBox.Show("Некорректно введены минуты.");
                    minetsNotification.Text = "";
                }


            }
        }

        private void hourNotification_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = (TextBox)e.Source;
            if (!IsNumber(textBox.Text) && textBox.Text != string.Empty)
            {
                hourNotification.Text = 12.ToString();
            }
           
        }

        private bool IsNumber(string text)
        {
            int number;

            //Allowing only numbers
            if (!(int.TryParse(text, out number)))
            {
                return false;
            }
            return true;
}

        private void minetsNotification_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = (TextBox)e.Source;
            if (!IsNumber(textBox.Text) && textBox.Text != string.Empty)
            {
                minetsNotification.Text = "00".ToString();
            }
        }

        private void infoNotification_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = (TextBox)e.Source;
            if (textBox.Text.Length > 90)
            {
                infoNotification.Text = textBox.Text.Remove(89, 1);
            }
        }

        private void hourNotification_LostFocus(object sender, RoutedEventArgs e)
        {
            if(hourNotification.Text == string.Empty)
            {
                hourNotification.Text = 12.ToString();
            }
        }

        private void minetsNotification_LostFocus(object sender, RoutedEventArgs e)
        {
            if (minetsNotification.Text == string.Empty)
            {
                minetsNotification.Text = "00";
            }
        }

        private void closeNotificationBut_Click(object sender, RoutedEventArgs e)
        {
            createNotificationWnd.Visibility = Visibility.Hidden;
        }

        private void createNotificationBut_Click(object sender, RoutedEventArgs e)
        {
            if (dateNotification.SelectedDate != null)
            {
                if (infoNotification.Text != string.Empty)
                {
                    DateTime dateTime = DateTime.Parse(((DateTime)dateNotification.SelectedDate).ToString("dd.MM.yyyy") + " "+ hourNotification.Text+":"+ minetsNotification.Text +":"+"00");
                    mainW.notificationProcess.SetNotification(TaskUser.idTask, dateTime, infoNotification.Text);
                    createNotificationWnd.Visibility = Visibility.Hidden;
                }
                else
                {
                    MessageBox.Show("Укажите текст напоминания"); 
                }
               
            }
            else
            {
                MessageBox.Show("Выберите дату");
            }
        }

        private void createNotification_Click(object sender, RoutedEventArgs e)
        {
            createNotificationWnd.Visibility = Visibility.Visible;
            dateNotification.SelectedDate = null;
            infoNotification.Text = string.Empty;
        }
    }
}
