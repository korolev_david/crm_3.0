﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0.PageElement
{
    /// <summary>
    /// Логика взаимодействия для ProductR.xaml
    /// </summary>
    public partial class ProductR : Page
    {
        private int id_user;
        DataBase dataBase ;
        List<DataCourse> dataCourses = new List<DataCourse>();
        public ProductR(int idUser, Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.idManager);
            this.id_user = idUser;
            Start();
        }

        private async void Start()
        {
            loadMini.Visibility = Visibility.Visible;
            await Task.Run(() => dataCourses = dataBase.GetDataCourses(id_user));
            OpenCourse.ItemsSource = dataCourses.FindAll(x => x.status == 1);
            loadMini.Visibility = Visibility.Collapsed;
        }

        private void open_course_rb_Checked(object sender, RoutedEventArgs e)
        {
            OpenCourse.ItemsSource = dataCourses.FindAll(x => x.status == 1);
            OpenCourse.Items.Refresh();
        }

        private void close_course_rb_Checked(object sender, RoutedEventArgs e)
        {
            OpenCourse.ItemsSource = dataCourses.FindAll(x => x.status == 2);
            OpenCourse.Items.Refresh();
        }
    }
}
