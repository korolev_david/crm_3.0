﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace crm_3._0.PageElement
{
    /// <summary>
    /// Логика взаимодействия для InfoUserR.xaml
    /// </summary>
    public partial class InfoUserR : Page
    {
        public int id_user ;
        DataBase dataBase ;
        UserInfo userInfo = new UserInfo();
        MainWindow mainWindow;
        MTask selTask;
        DispatcherTimer timer = new DispatcherTimer();
        DateTime date = DateTime.Now.ToUniversalTime();
        public InfoUserR(int idUser, MainWindow mainWindow)
        {
            InitializeComponent();
            dataBase = new DataBase(mainWindow.manager.idManager);
            id_user = idUser;
            sex.Items.Add("Мужской");
            sex.Items.Add("Женский");
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += TimerTick;
            Start();
            this.mainWindow = mainWindow;
            selTask = (MTask)mainWindow.mainTaskData.SelectedItem;
           
        }
       
        private async void Start()
        {
            loadmini.Visibility = Visibility.Visible;
          
            await Task.Run(() => { userInfo = dataBase.GetUserInfo(id_user); });
            if (userInfo != null)
            {
                DateTime dt = new DateTime();
                dateCreated.SelectedDate = userInfo?.date ?? dt;

                if (userInfo.sex == "Мужской") sex.SelectedIndex = 0;
                if (userInfo.sex == "Женский") sex.SelectedIndex = 1;
                phone.Text = userInfo.phone ?? "";
                country.Text = userInfo.country ?? "";
                birthday.Text = userInfo.brithday ?? "";
                nameUser_txt.Text = userInfo.name ?? "";
                email.Text = userInfo.email ?? "";
                if (userInfo.extra_phone != null)
                {
                    foreach (var item in userInfo.extra_phone)
                    {
                        if (item != string.Empty)
                        {
                            phoneCmbx.Items.Add(item);
                        }
                        if (userInfo.extra_phone.Length >= 1)
                        {
                            phoneCmbx.SelectedIndex = 0;
                        }
                        
                    }
                }
                if (userInfo.extra_email != null)
                {
                    foreach (var item in userInfo.extra_email)
                    {
                        if (item != string.Empty)
                        {
                            emailCmbx.Items.Add(item);
                        }                    
                    }
                    if (userInfo.extra_email.Length >= 1)
                    {
                        emailCmbx.SelectedIndex = 0;
                    }
                }
                SetUtc();

                //phone.Text = userInfo.phone ?? "";

                //string[] data = Regex.Split(dataPhone, "<data>");
                //string[] phone = Array.FindAll(data, x => !x.Contains("@") && x.Contains("+"));
                //if (Array.FindAll(phone, x => x.Contains(";")).Length > 0)
                //{
                //    string dopPhone = phone[Array.FindIndex(phone, x => x.Contains(";"))];
                //    phone = Array.FindAll(phone, x => !x.Contains(";"));
                //    string[] dopEmailArr = dopPhone.Split(';');
                //    foreach (string item in dopEmailArr)
                //        if (item != string.Empty) numberCallCombo.Items.Add(item);

                //    foreach (string item in phone)
                //        if (item != string.Empty) numberCallCombo.Items.Add(item);
                //}
                //else if (phone.Length > 0)
                //{
                //    foreach (var item in phone)
                //        numberCallCombo.Items.Add(item);
                //}
                //if (numberCallCombo.Items.Count > 0)
                //{
                //    numberCallCombo.SelectedIndex = 0;
                //}

            }
            else
                MessageBox.Show("Ошибка нет ответ InfoUser");

            loadmini.Visibility = Visibility.Collapsed;
        }
        private void TimerTick(object sender, EventArgs e)
        {
            date = date.AddSeconds(1);
            TimeZ.Text = String.Format("Время клиента: {0}", date.ToString("HH:mm:ss"));
        }
        private void utc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items.Count != 0)
            {
                ComboBoxItem utcSel = (ComboBoxItem)items[0];
                string utcStr = utcSel.Content.ToString();
                utcStr = utcStr.Replace("UTC", "");
                date = DateTime.Now.ToUniversalTime();
                date = date.AddHours(Convert.ToDouble(utcStr));
                TimeZ.Text = String.Format("Время клиента: {0}", date.ToString("HH:mm:ss"));
                timer.Start();
            }
            else
            {
                timer.Stop();
                TimeZ.Text = string.Empty;
            }


        }
        private async void editUTC_Click(object sender, RoutedEventArgs e)
        {

            if (editUTC.Tag.ToString() == 2.ToString())
            {
                editUTC.Tag = 1.ToString();
                utc.IsEnabled = true;

            }
            else
            {
                if (utc.SelectedIndex != -1)
                {
                    ItemCollection collection = utc.Items;
                    List<ComboBoxItem> boxItems = collection.Cast<ComboBoxItem>().ToList();
                    string UTC = boxItems.Find(x => x.IsSelected == true).Content.ToString();
                    bool answer = false;
                    await Task.Run(() => { answer = dataBase.EditUTC(id_user, UTC); });
                    if (!answer)
                    {
                        MessageBox.Show("Ошибка. Данные не внесены");
                        utc.SelectedIndex = -1;
                    }
                }
                editUTC.Tag = 2.ToString();
                utc.IsEnabled = false;

            }
        }

        private async void EditName_Click(object sender, RoutedEventArgs e)
        {
            if (EditName.Tag.ToString() == 2.ToString())
            {
                EditName.Tag = 1.ToString();
                nameUser_txt.IsReadOnly = false;

            }
            else
            {
                bool answer = false;
                string name = nameUser_txt.Text;
                await Task.Run(() => { answer = dataBase.EditName(id_user, name); });
                if (!answer)
                {
                    MessageBox.Show("Ошибка. Данные не внесены");
                    nameUser_txt.Text = userInfo.name ?? "";
                }

                EditName.Tag = 2.ToString();
                nameUser_txt.IsReadOnly = true;
            }
        }
        private void SetUtc()
        {
            ItemCollection collection = utc.Items;

            foreach (ComboBoxItem item in collection)
            {
                if (item.Content.ToString() == userInfo.utc)
                {

                    item.IsSelected = true;
                    timer.Start();
                    break;
                }
            }
        }
        private async void edit_sex_Click(object sender, RoutedEventArgs e)
        {
            if (edit_sex.Tag.ToString() == "2")
            {
                edit_sex.Tag = 1.ToString();
                sex.IsEnabled = true;
            }
            else
            {
                edit_sex.Tag = 2.ToString();
                sex.IsEnabled = false;
                if (sex.SelectedIndex != -1)
                {
                    string sexSelect = sex.Items[sex.SelectedIndex].ToString();
                    if (sexSelect != userInfo.sex)
                    {
                        string value = sexSelect;
                        bool answer = false;
                        await Task.Run(() => answer = dataBase.UpdateDataUser(id_user, 101, value));
                        if (!answer)
                        {
                            sex.Text = userInfo.sex;
                            MessageBox.Show("Данные не внесены");
                        }
                    }

                }


            }
        }

    

        private async void edit_country_Click(object sender, RoutedEventArgs e)
        {
            if (edit_country.Tag.ToString() == "2")
            {
                edit_country.Tag = 1.ToString();
                country.IsReadOnly = false;
            }
            else
            {
                edit_country.Tag = 2.ToString();
                country.IsReadOnly = true;
                if (country.Text != userInfo.country)
                {
                    string value = country.Text;
                    bool answer = false;
                    await Task.Run(() => answer = dataBase.UpdateDataUser(id_user, 82, value));
                    if (!answer)
                    {
                        country.Text = userInfo.country;
                        MessageBox.Show("Данные не внесены");
                    }
                }
            }
        }

        private async void edit_birthday_Click(object sender, RoutedEventArgs e)
        {
            if (edit_birthday.Tag.ToString() == "2")
            {
                edit_birthday.Tag = 1.ToString();
                birthday.IsReadOnly = false;
            }
            else
            {
                edit_birthday.Tag = 2.ToString();
                birthday.IsReadOnly = true;
                if (birthday.Text != userInfo.brithday)
                {
                    string value = birthday.Text;
                    bool answer = false;
                    await Task.Run(() => answer = dataBase.UpdateDataUser(id_user, 124, value));
                    if (!answer)
                    {
                        birthday.Text = userInfo.brithday;
                        MessageBox.Show("Данные не внесены");
                    }
                }
            }
        }

        private void copy_email_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(email.Text);
        }

        private void copy_phone_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(phone.Text);
        }

        private async void edit_extra_p1_Click(object sender, RoutedEventArgs e)
        {
            if (phoneCmbx.SelectedIndex != -1)
            {
                if (edit_extra_p1.Tag.ToString() == "2")
                {
                    edit_extra_p1.Tag = 1.ToString();
                    extra_p1.IsReadOnly = false;
                }
                else
                {
                    edit_extra_p1.Tag = 2.ToString();
                    extra_p1.IsReadOnly = true;
                    string value = string.Empty;
                    string oldValue = phoneCmbx.Text;
                    if (userInfo.extra_phone != null)
                    {
                        userInfo.extra_phone_d = userInfo.extra_phone_d.Replace(oldValue, extra_p1.Text);
                        foreach (var item in userInfo.extra_phone)
                        {
                            if (item != string.Empty)
                            {
                                value += item + ";";
                            }
                        }
                    }
                    if (value == string.Empty && value == "")
                    {
                        value = ";";
                    }
                    bool answer = false;
                    await Task.Run(() => answer = dataBase.UpdateDataUser(id_user, 81, value));
                    await Task.Run(() => selTask.fileterData = dataBase.GetFormData(id_user));
                    if (!answer)
                    {
                        MessageBox.Show("Данные не внесены");
                    }
                    else
                    {
                        phoneCmbx.Items.Clear();
                        if (userInfo.extra_phone != null)
                        {
                            foreach (var item in userInfo.extra_phone)
                            {
                                phoneCmbx.Items.Add(item);
                            }
                        }
                    }
                }
            }
           
        }



        private async void edit_extra_e1_Click(object sender, RoutedEventArgs e)
        {
            if (emailCmbx.SelectedIndex != -1)
            {
                if (edit_extra_e1.Tag.ToString() == "2")
                {
                    edit_extra_e1.Tag = 1.ToString();
                    extra_e1.IsReadOnly = false;
                }
                else
                {
                    edit_extra_e1.Tag = 2.ToString();
                    extra_e1.IsReadOnly = true;
                    string value = string.Empty;
                    string oldValue = emailCmbx.Text;
                    if (userInfo.extra_email != null)
                    {
                        userInfo.extra_email_d = userInfo.extra_email_d.Replace(oldValue, extra_e1.Text);
                        foreach (var item in userInfo.extra_email)
                        {
                            if (item != string.Empty)
                            {
                                value += item + ";";
                            }
                        }

                    }
                    if (value == string.Empty && value == "")
                    {
                        value = ";";
                    }
                    bool answer = false;
                    await Task.Run(() => answer = dataBase.UpdateDataUser(id_user, 80, value));
                    await Task.Run(() => selTask.fileterData = dataBase.GetFormData(id_user));
                    if (!answer)
                    {
                        MessageBox.Show("Данные не внесены");
                    }
                    else
                    {
                        emailCmbx.Items.Clear();
                        if (userInfo.extra_email != null)
                        {
                            foreach (var item in userInfo.extra_email)
                            {
                                emailCmbx.Items.Add(item);
                            }
                        }
                    }
                }

            }
         
        }

        private void phoneCmbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (phoneCmbx.SelectedIndex != -1)
            {
                var items = e.AddedItems;
                if (items != null)
                {
                    extra_p1.Text = items[0].ToString();
                }
            }
                       
        }

        private void emailCmbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (emailCmbx.SelectedIndex != -1)
            {
                var items = e.AddedItems;
                if (items != null)
                {
                    extra_e1.Text = items[0].ToString();
                }

            }
            
               
        }

    

        private async void addExtraE_Click(object sender, RoutedEventArgs e)
        {
            if (extra_add_e.Text != string.Empty)
            {
                string value = string.Empty;
                if (userInfo.extra_email != null)
                {
                    foreach (var item in userInfo.extra_email)
                    {
                        if (item != string.Empty)
                        {
                            value += item + ";";
                        }
                       
                    }
                    value += extra_add_e.Text+";";
                }
                else
                {
                    value += extra_add_e.Text + ";";
                }
                bool answer = false;
                await Task.Run(() => answer = dataBase.UpdateDataUser(id_user, 80, value));
                await Task.Run(() => selTask.fileterData = dataBase.GetFormData(id_user));
                if (!answer)
                {
                    MessageBox.Show("Данные не внесены");
                }
                else
                {
                    extra_add_e.Text = string.Empty;
                    userInfo.extra_email_d = value;
                    emailCmbx.Items.Clear();
                    if (userInfo.extra_email != null)
                    {
                        foreach (var item in userInfo.extra_email)
                        {
                            emailCmbx.Items.Add(item);
                        }
                    }
                }
            }
        }

        private async void addExtraP_Click(object sender, RoutedEventArgs e)
        {
            if (extra_add_p.Text != string.Empty)
            {
                string value = string.Empty;
                if (userInfo.extra_phone != null)
                {
                    foreach (var item in userInfo.extra_phone)
                    {
                        if (item != string.Empty)
                        {
                            value += item + ";";
                        }

                    }
                    value += extra_add_p.Text + ";";
                }
                else
                {
                    value += extra_add_p.Text + ";";
                }
                bool answer = false;
                await Task.Run(() => answer = dataBase.UpdateDataUser(id_user, 81, value));
                await Task.Run(() => selTask.fileterData = dataBase.GetFormData(id_user));
                if (!answer)
                {
                    MessageBox.Show("Данные не внесены");
                }
                else
                {
                    extra_add_p.Text = string.Empty;
                    userInfo.extra_phone_d = value;
                    phoneCmbx.Items.Clear();
                    if (userInfo.extra_phone != null)
                    {
                        foreach (var item in userInfo.extra_phone)
                        {
                            phoneCmbx.Items.Add(item);
                        }
                    }
                }
            }
        }

        
    }
}
