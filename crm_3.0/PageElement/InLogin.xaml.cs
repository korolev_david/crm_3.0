﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0.PageElement
{
    /// <summary>
    /// Логика взаимодействия для InLogin.xaml
    /// </summary>
    public partial class InLogin : Page
    {
        MainWindow mainWnd;
        Manager manager;
        public InLogin(MainWindow mwnd)
        {
            InitializeComponent();
            mainWnd = mwnd;
        }
     
        private async void loginIn_Click(object sender, RoutedEventArgs e)
        {
            error_info.Visibility = Visibility.Collapsed;
            if (Login.Text != string.Empty && Pass.Password != string.Empty)
            {
                bool CheckSave = (Boolean)CheckSaveAutho.IsChecked;
                DataBase dataBase = new DataBase(1101);
                string _login = Login.Text;
                string _pass = Pass.Password;
                await Task.Run(() => { manager = dataBase.inLogin(_pass, _login); });
                if (manager.idManager != 0)
                {
                    mainWnd.setIdManager(manager.idManager);
                    if (CheckSave)
                    {
                        HendlerAuthorization hendler = new HendlerAuthorization();
                        hendler.SetFile(_login, _pass);
                        NextWnd();
                    }
                    else
                    {
                        NextWnd();
                    }
                }
                else
                {
                    error_info.Visibility = Visibility.Visible;
                }
                    
            }
        }

        private void NextWnd()
        {
            mainWnd.NextAuthorization(manager);
        }

      
    }
}
