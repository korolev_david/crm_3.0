﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0
{
    /// <summary>
    /// Логика взаимодействия для Сalendar.xaml
    /// </summary>
    public partial class Сalendar : Window
    {
        public Сalendar(int manager, MainWindow mainWindow)
        {
            InitializeComponent();

            calendarParams.Add(new СalendarParam { name = "Monday",      position = 0}) ;
            calendarParams.Add(new СalendarParam { name = "Tuesday",     position = -1 });
            calendarParams.Add(new СalendarParam { name = "Wednesday",   position = -2 });
            calendarParams.Add(new СalendarParam { name = "Thursday",    position = -3 });
            calendarParams.Add(new СalendarParam { name = "Friday",      position = -4 });
            calendarParams.Add(new СalendarParam { name = "Saturday",    position = -5 });
            calendarParams.Add(new СalendarParam { name = "Sunday",       position = -6 });

            dateSelectd = new DateTime(dateNow.Year, dateNow.Month, 1);
            dataBase = new DataBase(manager);
            this.manager = manager;
            monthText.Text = dateSelectd.ToString("MMMM");
            yearText.Text = dateSelectd.ToString("yyyy");
            this.mainWindow = mainWindow;
        }
        DateTime dateNow = DateTime.Now;
        DateTime dateProgress;
        DateTime dateSelectd;
        MainWindow mainWindow;
        List<СalendarParam> calendarParams = new List<СalendarParam>();
        DataBase dataBase;
        List<CalendarData> calendarDatas = new List<CalendarData>();
        int manager;

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            dateProgress = dateSelectd;

            var findParamDate = calendarParams.Find(x => x.name == dateSelectd.DayOfWeek.ToString());
            int setStartDay = findParamDate.position;
            Button[] buttons = GetButton();
            Border[] borders = GetBorders();
            TextBlock[] textBlocks = GetTextBlocks();
            DateTime datePosition;

            for (int i = 0; i < buttons.Length; i++)
            {

                datePosition = dateProgress.AddDays(setStartDay);
                buttons[i].Content = datePosition.ToString("dd");
                if(datePosition.Month != dateSelectd.Month)
                {
                    buttons[i].IsEnabled = false;
                }
                

                setStartDay++;
            }

            ShowVeiwerData();


        }
        private async void ShowVeiwerData()
        {
            dateProgress = dateSelectd;

            string textDay = string.Empty;
            var findParamDate = calendarParams.Find(x => x.name == dateSelectd.DayOfWeek.ToString());
            int setStartDay = findParamDate.position;
            Button[] buttons = GetButton();
            Border[] borders = GetBorders();
            TextBlock[] textBlocks = GetTextBlocks();
            DateTime datePosition = new DateTime();
            


            for (int i = 0; i < buttons.Length; i++)
            {

                datePosition = dateProgress.AddDays(setStartDay);
                buttons[i].Tag = datePosition.Date;
                textDay = datePosition.ToString("dd");
                if (textDay[0] == '0')
                {
                    textDay = textDay.Replace("0", "");
                }
                buttons[i].Content = textDay  ;
                if (datePosition.Month != dateSelectd.Month)
                {
                    buttons[i].IsEnabled = false;
                    borders[i].Visibility = Visibility.Hidden;
                    textBlocks[i].Visibility = Visibility.Hidden;
                }
                else
                {
                    buttons[i].IsEnabled = true;

                }
                borders[i].Visibility = Visibility.Hidden;
                textBlocks[i].Visibility = Visibility.Hidden;


                setStartDay++;
            }
            calendarGrid.IsEnabled = false;

            await Task.Run(() => { calendarDatas = dataBase.GetCalendarDatas(manager, datePosition); });
            findParamDate = calendarParams.Find(x => x.name == dateSelectd.DayOfWeek.ToString());
            dateProgress = dateSelectd;
            setStartDay = findParamDate.position;
            for (int i = 0; i < buttons.Length; i++)
            {
                datePosition = dateProgress.AddDays(setStartDay);
                if(calendarDatas.Find(x => x.date.Date == datePosition.Date) != null)
                {
                    var item = calendarDatas.Find(x => x.date.Date == datePosition.Date);
                    borders[i].Visibility = Visibility.Visible;
                    textBlocks[i].Visibility = Visibility.Visible;
                    textBlocks[i].Text = item.count_.ToString();
                }
                if (datePosition.Month != dateSelectd.Month)
                {
                   
                    borders[i].IsEnabled =false;
                    textBlocks[i].IsEnabled = false;
                }
                else
                {
                    borders[i].IsEnabled = true;
                    textBlocks[i].IsEnabled = true;
                }
                setStartDay++;
            }
            calendarGrid.IsEnabled = true;

        }


        private Button[] GetButton()
        {
            return new Button[42] {
            day1, day2, day3, day4, day5,
            day6, day7, day8, day9, day10,
            day11, day12, day13, day14, day15,
            day16, day17, day18, day19, day20,
            day21, day22, day23, day24, day25,
            day26, day27, day28, day29, day30,
            day31, day32, day33, day34, day35,
            day36, day37, day38, day39, day40,
            day41, day42};
        }
        private Border[] GetBorders()
        {
            return new Border[42] { brText1, brText2, brText3, brText4, brText5, brText6, brText7, brText8, brText9, brText10, brText11
            , brText12, brText13, brText14, brText15, brText16, brText17, brText18, brText19, brText20, brText21, brText22, brText23, brText24
            , brText25, brText26, brText27, brText28, brText29, brText30, brText31, brText32,brText33,brText34,brText35,brText36,brText37,
            brText38,brText39,brText40,brText41,brText42};
        }

        private TextBlock[] GetTextBlocks()
        {
            return new TextBlock[42] { countDay1, countDay2, countDay3, countDay4, countDay5, countDay6, countDay7, countDay8, countDay9, countDay10
            , countDay11, countDay12, countDay13, countDay14, countDay15, countDay16, countDay17, countDay18, countDay19, countDay20, countDay21, countDay22
            , countDay23, countDay24, countDay25, countDay26, countDay27, countDay28, countDay29, countDay30, countDay31,countDay32,countDay33,countDay34,
            countDay35, countDay36, countDay37, countDay38, countDay39, countDay40, countDay41, countDay42};

        }
        
        private void CalendarSearch_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            DateTime date = DateTime.Parse(btn.Tag.ToString());
            mainWindow.LoadDataTask(date, date);
        }

        private void monthNextBut_Click(object sender, RoutedEventArgs e)
        {
            dateSelectd = dateSelectd.AddMonths(+1);
            monthText.Text = dateSelectd.ToString("MMMM");
            yearText.Text = dateSelectd.ToString("yyyy");
            ShowVeiwerData();
        }

        private void monthBehaenBut_Click(object sender, RoutedEventArgs e)
        {
            dateSelectd = dateSelectd.AddMonths(-1);
            monthText.Text = dateSelectd.ToString("MMMM");
            yearText.Text = dateSelectd.ToString("yyyy");
            ShowVeiwerData();

        }
    }
}
