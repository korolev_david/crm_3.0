﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0
{
    /// <summary>
    /// Логика взаимодействия для TaskWnd.xaml
    /// </summary>
    public partial class TaskWnd : Window
    {
        Manager manager;
        public TaskWnd(int idKind, Manager manager)
        {
            InitializeComponent();
            this.manager = manager;
            dateStart.SelectedDate = DateTime.Now.AddDays(-20);
            this.id_kind = idKind;
        }
        int id_kind;
        private async void view_data_Click(object sender, RoutedEventArgs e)
        {
            DataBase db = new DataBase(manager.idManager);
            var dt_s = dateStart.SelectedDate;  //dateStart.SelectedDate;
            var dt_e = dateEnd.SelectedDate;
            dt_e = (DateTime)dt_e.Value.AddDays(1);
            List<TaskList> lst = new List<TaskList>();
            lst = await Task.Run(() => db.LstTask((DateTime)dt_s, (DateTime)dt_e, id_kind));
            task_data.ItemsSource = lst;
        }

        private void Back_fone_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private async void Window_ContentRendered(object sender, EventArgs e)
        {
            DataBase db = new DataBase(manager.idManager);
            var dt_s = dateStart.SelectedDate;  //dateStart.SelectedDate;
            var dt_e = dateEnd.SelectedDate;
            dt_e = (DateTime)dt_e.Value.AddDays(1);
            List<TaskList> lst = new List<TaskList>();
            lst = await Task.Run(() => db.LstTask((DateTime)dt_s, (DateTime)dt_e, id_kind));
            task_data.ItemsSource = lst;
        }
    }
}
