﻿using crm_3._0.Hendler;
using crm_3._0.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0
{
    /// <summary>
    /// Логика взаимодействия для NextTask.xaml
    /// </summary>
    public partial class NextTask : Window
    {
        DataBase dataBase ;
        List<ProductList> prd = new List<ProductList>();
        int mng;
        int productIdDone;
        int userId;
        public NextTask(int mng, int productIdDone,int userId)
        {
            InitializeComponent();
            dataBase = new DataBase(mng);
            this.mng = mng;
            this.productIdDone = productIdDone;
            this.userId = userId;
        }

        private async void Window_ContentRendered(object sender, EventArgs e)
        {
            await Task.Run(() => { prd = dataBase.GetProductLists(); });
            cmbxProduct.ItemsSource = prd;
        }

        private void searchProduct_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (searchProduct.Text != string.Empty)
            {
                cmbxProduct.ItemsSource = prd.FindAll(x => x.name.Contains(searchProduct.Text));

            }
            else
                cmbxProduct.ItemsSource = prd;
        }

        private async void creationTask_Click(object sender, RoutedEventArgs e)
        {
            if (dateCreation.SelectedDate != null && cmbxProduct.SelectedIndex != -1)
            {
                DateTime date = (DateTime)dateCreation.SelectedDate;
                int productId = ((ProductList)cmbxProduct.SelectedItem).id;
                string themStr = thems.Text;
                string desc = description.Text;
                if (productIdDone != productId)
                {
                    if (thems.Text != string.Empty && description.Text != string.Empty)
                    {
                        bool check = true;
                        await Task.Run(() => { check = dataBase.CreatTask(userId, date, productId, 1, mng, themStr, desc); });
                        if (check)
                        {
                            MessageBox.Show("Задача создана");
                            this.Close();
                        }
                        else
                            MessageBox.Show("Ошибка! Задча не создана");
                    }
                    else
                    {
                        MessageBox.Show("Заполнение тему и описание!");
                    }

                }
                else
                    MessageBox.Show("Задача с таким продуктом выполнена");
            }
            else
            {
                MessageBox.Show("Выберите дату и продукт в выпадающем списке!");

            }
            

        }

        private void creationClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void description_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (description.Text.Length > 150)
            {

                description.Text = description.Text.Remove(150, description.Text.Length - 150);

            }
        }

        private void thems_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (thems.Text.Length > 60)
            {

                thems.Text = thems.Text.Remove(60, thems.Text.Length - 60);

            }
        }
    }
}
