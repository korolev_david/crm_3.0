﻿#pragma checksum "..\..\MainWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "FEDCFEBE39CDAD29413FE3AC750F7E7CC7099EC6E02AF5C3F28BFB3F6C9AE8BC"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using crm_3._0;
using crm_3._0.Animation;


namespace crm_3._0 {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid inLoginGrid;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Frame inLoginFrame;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LoadGrid;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid mainGrid;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid notification;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox numberTaskNf;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock infoTaskNf;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button closeNotif;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ShowCalendar;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock countTask;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ShowCastomere;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button courseList;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button TaskList;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid mainTaskData;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal crm_3._0.Animation.LoadMini loadPanelData;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid FilterData;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateTouchFrom;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateTouchBefore;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox typePosition;
        
        #line default
        #line hidden
        
        
        #line 168 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox priorityCmbx;
        
        #line default
        #line hidden
        
        
        #line 171 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox searchProduct_txt;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox productSearch;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox searchThems;
        
        #line default
        #line hidden
        
        
        #line 176 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox searchIdTask;
        
        #line default
        #line hidden
        
        
        #line 178 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button dropFilter;
        
        #line default
        #line hidden
        
        
        #line 179 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button refreshData;
        
        #line default
        #line hidden
        
        
        #line 188 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button FilterBut;
        
        #line default
        #line hidden
        
        
        #line 189 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox search;
        
        #line default
        #line hidden
        
        
        #line 190 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button globalSearch;
        
        #line default
        #line hidden
        
        
        #line 194 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Frame rigth_menu;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SwiftCRM 3.0;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 10 "..\..\MainWindow.xaml"
            ((crm_3._0.MainWindow)(target)).ContentRendered += new System.EventHandler(this.Window_ContentRendered);
            
            #line default
            #line hidden
            return;
            case 2:
            this.inLoginGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.inLoginFrame = ((System.Windows.Controls.Frame)(target));
            return;
            case 4:
            this.LoadGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.mainGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.notification = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.numberTaskNf = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.infoTaskNf = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.closeNotif = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\MainWindow.xaml"
            this.closeNotif.Click += new System.Windows.RoutedEventHandler(this.closeNotif_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.ShowCalendar = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\MainWindow.xaml"
            this.ShowCalendar.Click += new System.Windows.RoutedEventHandler(this.ShowCalendar_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.countTask = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.ShowCastomere = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\MainWindow.xaml"
            this.ShowCastomere.Click += new System.Windows.RoutedEventHandler(this.ShowCastomere_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.courseList = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\MainWindow.xaml"
            this.courseList.Click += new System.Windows.RoutedEventHandler(this.courseList_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.TaskList = ((System.Windows.Controls.Button)(target));
            
            #line 57 "..\..\MainWindow.xaml"
            this.TaskList.Click += new System.Windows.RoutedEventHandler(this.TaskList_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.mainTaskData = ((System.Windows.Controls.DataGrid)(target));
            
            #line 68 "..\..\MainWindow.xaml"
            this.mainTaskData.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.mainTaskData_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 16:
            this.loadPanelData = ((crm_3._0.Animation.LoadMini)(target));
            return;
            case 17:
            this.FilterData = ((System.Windows.Controls.Grid)(target));
            return;
            case 18:
            this.dateTouchFrom = ((System.Windows.Controls.DatePicker)(target));
            
            #line 132 "..\..\MainWindow.xaml"
            this.dateTouchFrom.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dateTouchFrom_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 19:
            this.dateTouchBefore = ((System.Windows.Controls.DatePicker)(target));
            
            #line 139 "..\..\MainWindow.xaml"
            this.dateTouchBefore.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dateTouchBefore_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 20:
            this.typePosition = ((System.Windows.Controls.ComboBox)(target));
            
            #line 148 "..\..\MainWindow.xaml"
            this.typePosition.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.typePosition_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 21:
            this.priorityCmbx = ((System.Windows.Controls.ComboBox)(target));
            
            #line 168 "..\..\MainWindow.xaml"
            this.priorityCmbx.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.priorityCmbx_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 22:
            this.searchProduct_txt = ((System.Windows.Controls.TextBox)(target));
            
            #line 171 "..\..\MainWindow.xaml"
            this.searchProduct_txt.LostFocus += new System.Windows.RoutedEventHandler(this.searchProduct_txt_LostFocus);
            
            #line default
            #line hidden
            
            #line 171 "..\..\MainWindow.xaml"
            this.searchProduct_txt.GotFocus += new System.Windows.RoutedEventHandler(this.searchProduct_txt_GotFocus);
            
            #line default
            #line hidden
            
            #line 171 "..\..\MainWindow.xaml"
            this.searchProduct_txt.PreviewKeyUp += new System.Windows.Input.KeyEventHandler(this.searchProduct_txt_PreviewKeyUp);
            
            #line default
            #line hidden
            
            #line 171 "..\..\MainWindow.xaml"
            this.searchProduct_txt.TextInput += new System.Windows.Input.TextCompositionEventHandler(this.searchProduct_txt_TextInput);
            
            #line default
            #line hidden
            return;
            case 23:
            this.productSearch = ((System.Windows.Controls.ComboBox)(target));
            
            #line 172 "..\..\MainWindow.xaml"
            this.productSearch.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.productSearch_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 24:
            this.searchThems = ((System.Windows.Controls.TextBox)(target));
            
            #line 175 "..\..\MainWindow.xaml"
            this.searchThems.LostFocus += new System.Windows.RoutedEventHandler(this.searchThems_LostFocus);
            
            #line default
            #line hidden
            
            #line 175 "..\..\MainWindow.xaml"
            this.searchThems.GotFocus += new System.Windows.RoutedEventHandler(this.searchThems_GotFocus);
            
            #line default
            #line hidden
            
            #line 175 "..\..\MainWindow.xaml"
            this.searchThems.TextInput += new System.Windows.Input.TextCompositionEventHandler(this.searchThems_TextInput);
            
            #line default
            #line hidden
            
            #line 175 "..\..\MainWindow.xaml"
            this.searchThems.PreviewKeyUp += new System.Windows.Input.KeyEventHandler(this.searchThems_PreviewKeyUp);
            
            #line default
            #line hidden
            return;
            case 25:
            this.searchIdTask = ((System.Windows.Controls.TextBox)(target));
            
            #line 176 "..\..\MainWindow.xaml"
            this.searchIdTask.LostFocus += new System.Windows.RoutedEventHandler(this.searchIdTask_LostFocus);
            
            #line default
            #line hidden
            
            #line 176 "..\..\MainWindow.xaml"
            this.searchIdTask.GotFocus += new System.Windows.RoutedEventHandler(this.searchIdTask_GotFocus);
            
            #line default
            #line hidden
            
            #line 176 "..\..\MainWindow.xaml"
            this.searchIdTask.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.searchIdTask_PreviewTextInput);
            
            #line default
            #line hidden
            
            #line 176 "..\..\MainWindow.xaml"
            this.searchIdTask.PreviewKeyUp += new System.Windows.Input.KeyEventHandler(this.searchIdTask_PreviewKeyUp);
            
            #line default
            #line hidden
            return;
            case 26:
            this.dropFilter = ((System.Windows.Controls.Button)(target));
            
            #line 178 "..\..\MainWindow.xaml"
            this.dropFilter.Click += new System.Windows.RoutedEventHandler(this.dropFilter_Click);
            
            #line default
            #line hidden
            return;
            case 27:
            this.refreshData = ((System.Windows.Controls.Button)(target));
            
            #line 179 "..\..\MainWindow.xaml"
            this.refreshData.Click += new System.Windows.RoutedEventHandler(this.refreshData_Click);
            
            #line default
            #line hidden
            return;
            case 28:
            this.FilterBut = ((System.Windows.Controls.Button)(target));
            
            #line 188 "..\..\MainWindow.xaml"
            this.FilterBut.Click += new System.Windows.RoutedEventHandler(this.FilterBut_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.search = ((System.Windows.Controls.TextBox)(target));
            
            #line 189 "..\..\MainWindow.xaml"
            this.search.GotFocus += new System.Windows.RoutedEventHandler(this.search_GotFocus);
            
            #line default
            #line hidden
            
            #line 189 "..\..\MainWindow.xaml"
            this.search.LostFocus += new System.Windows.RoutedEventHandler(this.search_LostFocus);
            
            #line default
            #line hidden
            
            #line 189 "..\..\MainWindow.xaml"
            this.search.TextInput += new System.Windows.Input.TextCompositionEventHandler(this.search_TextInput);
            
            #line default
            #line hidden
            
            #line 189 "..\..\MainWindow.xaml"
            this.search.PreviewKeyUp += new System.Windows.Input.KeyEventHandler(this.search_PreviewKeyUp);
            
            #line default
            #line hidden
            return;
            case 30:
            this.globalSearch = ((System.Windows.Controls.Button)(target));
            
            #line 190 "..\..\MainWindow.xaml"
            this.globalSearch.Click += new System.Windows.RoutedEventHandler(this.globalSearch_Click);
            
            #line default
            #line hidden
            return;
            case 31:
            this.rigth_menu = ((System.Windows.Controls.Frame)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

