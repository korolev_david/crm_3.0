﻿using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0
{
    /// <summary>
    /// Логика взаимодействия для CourseTreeViewWnd.xaml
    /// </summary>
    public partial class CourseTreeViewWnd : Window
    {
        public CourseTreeViewWnd()
        {
            InitializeComponent();
        }

        private void Back_fone_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private  async void Window_ContentRendered(object sender, EventArgs e)
        {
            
             DataBase connect_ = new DataBase(1101);
             treewee.ItemsSource = await Task.Run(() => connect_.GetСourses());
             load.Visibility = Visibility.Hidden;
        }

        private void close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
