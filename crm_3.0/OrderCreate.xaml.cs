﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0
{
    /// <summary>
    /// Логика взаимодействия для Score.xaml
    /// </summary>
    public partial class OrderCreate : Window
    {
        private DataBase dataBase ;
        private List<Product> products;
        private List<Product> productsAdd = new List<Product>();
        Manager manager;
        MTask task;
        bool freeCreation;
        public OrderCreate(Manager mng, MTask mTask, bool freeCreation)
        {
            InitializeComponent();
            dataBase = new DataBase(mng.idManager);
            manager = mng;
            task = mTask;
            this.freeCreation = freeCreation;



        }
        
        private async void Window_ContentRendered(object sender, EventArgs e)
        {
            await Task.Run(() => { products= dataBase.GetProduct(); });
            listProduct.ItemsSource = products;
            string[] data = Regex.Split(task.fileterData, "<data>");
            string[] email = Array.FindAll(data, x => x.Contains("@"));
            if (Array.FindAll(email, x => x.Contains(";")).Length > 0)
            {
                string dopEmail = email[Array.FindIndex(email, x => x.Contains(";"))];
                email = Array.FindAll(email, x => !x.Contains(";"));
                string[] dopEmailArr = dopEmail.Split(';');
                foreach(string item in dopEmailArr)
                    if(item != string.Empty) emailBox.Items.Add(item);


            }
            foreach (string item in email)
               if(item != string.Empty) emailBox.Items.Add(item);
        }

        private void SearchBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (SearchBox.Text != string.Empty)
            {
                listProduct.ItemsSource = products.FindAll(x=> x.name.ToLower().Contains(SearchBox.Text.ToLower()));
            }
            else
            {
                listProduct.ItemsSource = products;
            }
        }

        private void listProduct_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listProduct.SelectedItem != null)
            {
                if(productsAdd.Count == 0)
                {
                    productsAdd.Add((Product)listProduct.SelectedItem);
                    listScoreProduct.ItemsSource = productsAdd;
                    listScoreProduct.Items.Refresh();
                    toPay.Text = String.Format("К оплате: {0}", productsAdd.Select(x => x.cost - (x.cost * x.sale / 100)).Sum());
                }
                
            }

        }

        private void saleSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (listScoreProduct.SelectedItem != null)
            {
              var sel = (Product)listScoreProduct.SelectedItem;
              sel.sale = (int)saleSlider.Value;
              listScoreProduct.Items.Refresh();
              toPay.Text = String.Format("К оплате: {0}", productsAdd.Select(x => x.cost - (x.cost * x.sale / 100)).Sum());
             
            }
            else
            {
                saleSlider.Value = 0;
            }
        }

        private void listScoreProduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listScoreProduct.SelectedItem != null)
            {
                var sel = (Product)listScoreProduct.SelectedItem;
                saleSlider.Value = sel.sale;
            }
          
        }

        private void listScoreProduct_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listScoreProduct.SelectedItem != null)
            {
                productsAdd.Remove((Product)listScoreProduct.SelectedItem);
                listScoreProduct.Items.Refresh();
                toPay.Text = String.Format("К оплате: {0}", productsAdd.Select(x => x.cost - (x.cost * x.sale / 100)).Sum());
            }
        }

        private void AddListItem_Click(object sender, RoutedEventArgs e)
        {
            listProduct_MouseDoubleClick(null, null);
        }

        private void removeListItem_Click(object sender, RoutedEventArgs e)
        {
            listScoreProduct_MouseDoubleClick(null, null);
        }

        private  void creatScore_Click(object sender, RoutedEventArgs e)
        {
            if (!(bool)sendMs.IsChecked)
            {
                if (emailBox.SelectedIndex != -1)
                {
                    if (task.idOrder == -1 || freeCreation)
                    {
                        int idOrder = CreateScore();
                        SendMessage sendMessage = new SendMessage(idOrder, products, task, emailBox.Items[emailBox.SelectedIndex].ToString());
                        sendMessage.SendMsOrders();
                        task.idOrder = idOrder;
                    }
                }
                else
                {
                    MessageBox.Show("Выберите Email");
                }
            }
            else
            {
                if (task.idOrder == -1)
                {
                    int idOrder = CreateScore();
                    task.idOrder = idOrder;
                }
            }
              
            
        }
        private int CreateScore()
        {
            string scoreProduct = string.Empty;
            int idOrder = -1;
            if (productsAdd.Count != 0)
            {
                foreach (var item in productsAdd)
                {
                    scoreProduct += item.idProduct + "," + item.sale + ";";
                }


                Task.Run(() => { idOrder = dataBase.SetOrders(task.idUser, manager.idManager, scoreProduct, task.idTask); }).Wait();
                if (idOrder != -1)
                {

                    MessageBox.Show("Счет выставлен");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ошибка счет не выставлен!");
                }

            }
            else
            {
                MessageBox.Show("Добавьте продукт");
            }
            return idOrder;
        }
    }
}
