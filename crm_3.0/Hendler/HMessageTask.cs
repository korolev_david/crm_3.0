﻿using crm_3._0.Class;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Hendler
{
    public class HMessageTask
    {
        public ObservableCollection<MessageTask> msTasks { get; set; }
        Manager manager = new Manager();
        public HMessageTask(int idTask, Manager manager)
        {
            getMessage(idTask);
            this.manager = manager;
        }
        private async void getMessage(int idTask)
        {
            DataBase dataBase = new DataBase(manager.idManager);
            await Task.Run(() => { msTasks = dataBase.GetMessageTasks(idTask); });

        }
        public async void setMessage(int idTask, string txt, string name)
        {
            DataBase dataBase = new DataBase(manager.idManager);
            await Task.Run(() => { dataBase.SetMessageTask(idTask,txt,name); });
        }
    }
}
