﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Hendler
{
    class Coder
    {
        private string key = "1wx1gvn";

        public string getToken(int id, string method)
        {
            string data = "{\"time\":\"" + (DateTime.UtcNow.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds + "\", \"id\":\""+id+"\", \"method\" : \""+method+"\"}";

            return encode_(data);
        }
        private string encode_(string str)
        {
            string key = this.key;
            string newStr = string.Empty;
            var strUTF = Encoding.UTF8.GetBytes(str);
            var str64 = Convert.ToBase64String(strUTF);
            List<String> arr = new List<string>();
            int x = 0;
            while (x++ < str64.Length)
            {
                arr.Add(md5(md5(key + str64[x - 1]) + key));
                newStr += arr[x - 1][3].ToString() + arr[x - 1][6].ToString() + arr[x - 1][1].ToString() + arr[x - 1][2].ToString();
            }
            return newStr;
        }
        private string md5(string input)
        {
            using (var md5 = MD5.Create())
            {
                string result = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(input)))
                     .Replace("-", string.Empty).ToLower();
                return result;
            }

        }
    }
}
