﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace crm_3._0.Hendler
{
    class HendlerAuthorization
    {
        public string GetFile()
        {
            string messege = string.Empty;
            string pass_shfr = string.Empty;
            try
            {
                FileStream fstream = new FileStream(@"j2.inf", FileMode.OpenOrCreate, FileAccess.ReadWrite);

                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                pass_shfr = System.Text.Encoding.Default.GetString(array);
                fstream.Close();
                pass_shfr = pass_shfr.Replace(" ", string.Empty);
                if (pass_shfr == string.Empty)
                {
                    return "false";
                }
                else
                {
                    byte[] mybyte = System.Convert.FromBase64String(pass_shfr);
                    return System.Text.Encoding.UTF8.GetString(mybyte);
                }
            }
            catch
            {
                MessageBox.Show("Ошибка get_file ");
                return "false";
            }
        }
        public void SetFile(string login, string pass)
        {
            try
            {
                byte[] mybyte = System.Text.Encoding.UTF8.GetBytes(login + ":" + pass);
                string returntext = System.Convert.ToBase64String(mybyte);

                FileStream fstream = new FileStream(@"j2.inf", FileMode.Create, FileAccess.ReadWrite);
                byte[] array = System.Text.Encoding.Default.GetBytes(returntext);
                fstream.Write(array, 0, array.Length);
                fstream.Close();
            }
            catch
            {
                MessageBox.Show("Ошибка set_file ");
            }
        }
    }
}
