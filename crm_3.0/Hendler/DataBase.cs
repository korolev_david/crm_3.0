﻿using crm_3._0.Class;
using crm_3._0;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using xNet;
using Newtonsoft.Json.Linq;

namespace crm_3._0.Hendler
{
    class DataBase
    {
        private int unId;
        private Coder getCoder = new Coder();
        public DataBase (int unId)
        {
            this.unId = unId;
        }
        public Manager inLogin(string pass, string login)
        {
            Manager mng = new Manager();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getManeger"));
            try
            {
                reqParams["login"] = login;
                reqParams["pass"] = pass;
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3/getManeger.php", reqParams).ToString();
               
                if (str != "null")
                {
                    mng = JsonConvert.DeserializeObject<Manager>(str);
                }
                else
                {
                    mng.idManager = -1;
                    return mng;
                }
            }
            catch (Exception)
            {
                mng.idManager = -1;
                return mng;
            }

            return mng;

        }
        public List<CalendarData> GetCalendarDatas(int manager, DateTime date)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            DecompressData decompressData = new DecompressData();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getDataCalendar"));
            try
            {
                reqParams["dateBefore"] = date.ToString("yyyy-MM-dd");
                reqParams["managerId"] = manager;
              

                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getDataCalendar.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                return JsonConvert.DeserializeObject<List<CalendarData>>(str);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: GetCalendarDatas... " + ex.Message);
                return null;
            }

        }
        public double get_version()
        {
            var request = new HttpRequest();
            request.CharacterSet = Encoding.GetEncoding("UTF-8");
            string str = "false";
            double vrs = 0;
            var reqParams = new RequestParams();
          
            try
            {
                str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3/version/version.php", reqParams).ToString();
                vrs = Convert.ToDouble(str, CultureInfo.InvariantCulture);

            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка version: " + e.Message);
            }
            return vrs;
        }
        public List<MTask> GetMTasks(DateTime dateStart, DateTime dateLast, Manager manager)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            List<MTask> data = new List<MTask>();
            DecompressData decompressData = new DecompressData();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getTask"));
            try
            {
                
                reqParams["dateStart"] = dateStart.ToString("yyyy-MM-dd 00:00:00");
                reqParams["dateLast"] = dateLast.ToString("yyyy-MM-dd 23:59:59");
                reqParams["idManager"] = manager.idManager;

                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getTask.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                data = JsonConvert.DeserializeObject<List<MTask>>(str);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ошибка: GetMTask... " + ex.Message);
            }

            return data;
        }
        public List<MTask> GetMTasksUser(int idUser)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            List<MTask> data = new List<MTask>();
            DecompressData decompressData = new DecompressData();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getTaskUsers"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getTaskUsers.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                return JsonConvert.DeserializeObject<List<MTask>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: GetMTasksUser... " + ex.Message);
                return null;
            }
        }
        public UserInfo GetUserInfo(int idUser)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            UserInfo userInfo = new UserInfo();
            DecompressData decompressData = new DecompressData();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:clientInfo"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/clientInfo.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                userInfo = JsonConvert.DeserializeObject<UserInfo>(str);
            }
            catch
            {
                return null;
            }
            return userInfo;
        }
        public bool UpdateDataUser(int idUser, int attr, string value)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:clientInfo"));
            try
            {
                reqParams["idUser"] = idUser;
                reqParams["attr"] = attr;
                reqParams["value"] = value;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3/clientInfo.php", reqParams);
            }
            catch
            {
                return false;
            }

            return true;
        }
        public List<DataCourse> GetDataCourses(int idUser)
        {
            List<DataCourse> dataCourses = new List<DataCourse>();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            DecompressData decompressData = new DecompressData();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:courseInfo"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/courseInfo.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                dataCourses = JsonConvert.DeserializeObject<List<DataCourse>>(str);
            }
            catch
            {
                MessageBox.Show("Ошибка GetDataCourses");
                return null;
            }

            return dataCourses;
        }

        public ObservableCollection<MessageTask> GetMessageTasks(int idTask)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            ObservableCollection<MessageTask> messageTasks = new ObservableCollection<MessageTask>();
            DecompressData decompressData = new DecompressData();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:taskMessage"));
            try
            {
                reqParams["id_task"] = idTask;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/taskMessage.php", reqParams).ToString();
                
                str = decompressData.StartDecompress(str);
                messageTasks = JsonConvert.DeserializeObject<ObservableCollection<MessageTask>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: GetMTask... " + ex.Message);
            }

            return messageTasks;
        }
        public int SetMessageTask(int idTask, string text, string name)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:taskMessage"));
            try
            {
                reqParams["user"] = name;
                reqParams["text"] = text;
                reqParams["id_task"] = idTask;
                string id = request.Post("https://siwitpro.com/purnov/crm_3/crm_3/taskMessage.php", reqParams).ToString();
                return Convert.ToInt32(id); 
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: SetMessageTask... " + ex.Message);
            }
            return -1;
        }
        public bool UpdateTask(int idTask, string param, string updateData)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getTask"));
            try
            {
                reqParams["param"] = param;
                reqParams["idTask"] = idTask;
                reqParams["update"] = updateData;
                string answer = request.Post("https://siwitpro.com/purnov/crm_3/crm_3/getTask.php", reqParams).ToString();
                return Convert.ToBoolean(answer);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: UpdateTAsk... " + ex.Message);
            }
            return false;
        }
        public bool UpdateStageTask(int idStage, string param, string updateData)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getTask"));
            try
            {
                reqParams["param"] = param;
                reqParams["idStage"] = idStage;
                reqParams["update"] = updateData;
                string answer = request.Post("https://siwitpro.com/purnov/crm_3/crm_3/getTask.php", reqParams).ToString();
                return Convert.ToBoolean(answer);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: UpdateStageTask... " + ex.Message);
            }
            return false;
        }
        public int NextStage(int idStage, int idTask, int idStageStype)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:nextStage"));
            try
            {
                reqParams["stageType"] = idStageStype;
                reqParams["stageId"] = idStage;
                reqParams["taskId"] = idTask;
                string answer = request.Post("https://siwitpro.com/purnov/crm_3/crm_3/nextStage.php", reqParams).ToString();
                return Convert.ToInt32(answer);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: NextStage... " + ex.Message);
                return -1;
            }
           
        }
        public bool CloseTask(int idTask)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:taskClose"));
            try
            {
                reqParams["taskId"] = idTask;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3/taskClose.php", reqParams).ToString();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: CloseTask... " + ex.Message);
                return false;
            }

        }
        public List<Product> GetProduct()
        {
            List<Product> data = new List<Product>();
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getProduct"));
            try
            {
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getProduct.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                data = JsonConvert.DeserializeObject<List<Product>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: GetProduct... " + ex.Message);
                return null;
            }

            return data;
        }
        public DataCommentPage GetDataComment(int idUser)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            DecompressData decompressData = new DecompressData();
            DataCommentPage dataCommentPage = new DataCommentPage();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:commentHandler"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/commentHandler.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                dataCommentPage = JsonConvert.DeserializeObject<DataCommentPage>(str);

            }
            catch(Exception ex)
            {
                MessageBox.Show("GetDataComment: GetProduct... " + ex.Message);
                return null;
            }

            return dataCommentPage;

        }
        public int SetOrders(int idUser, int idManager, string productData, int idTask)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:createScore"));
            try
            {
                reqParams["idUser"] = idUser;
                reqParams["idManager"] = idManager;
                reqParams["productData"] = productData;
                reqParams["idTask"] = idTask;
                return Convert.ToInt32( request.Post("https://siwitpro.com/purnov/crm_3/crm_3/createScore.php", reqParams).ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show("SetOrders: GetProduct... " + ex.Message);
                return -1;
            }
           
        }
        public OrderProduct getOrderReg(int idOrder)
        {
            OrderProduct orderProducts = new OrderProduct();
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:EditOrder"));
            try
            {
                reqParams["idOrder"] = idOrder;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/EditOrder.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                orderProducts = JsonConvert.DeserializeObject<OrderProduct>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show("SetOrders: GetProduct... " + ex.Message);
                return null;
            }

            return orderProducts;
        }
        public Orders getOrder(int idOrder)
        {
            Orders order = new Orders();
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getOrder"));
            try
            {
                reqParams["idOrder"] = idOrder;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getOrder.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                order = JsonConvert.DeserializeObject<Orders>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show("SetOrders: GetProduct... " + ex.Message);
                return null;
            }

            return order;

        }
        public List<Orders> GetOrders(int idUser)
        {
            List<Orders> order = new List<Orders>();
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getOrders"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getOrders.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                order = JsonConvert.DeserializeObject<List<Orders>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show("SetOrders: GetOrders... " + ex.Message);
                return null;
            }
            return order;
        }
        public List<DataUrl> GetDataUrls(int idUser)
        {
            List<DataUrl> dataUrl = new List<DataUrl>();
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getUrl"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getUrl.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                dataUrl = JsonConvert.DeserializeObject<List<DataUrl>>(str);
            }
            catch
            {
                return null;
            }
            return dataUrl;

        }
        public bool editOrder (OrderProduct orderProduct)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            string data = string.Empty;
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getUrl"));
            try
            {
               

                foreach (Product item in orderProduct.products.FindAll(x => x.drop == true))
                {
                   
                    data += item.idRel + ";";
                }
                if (data != string.Empty)// Редактирование УДАЛЕНИЕ ПРОДУКТА
                {
                    reqParams["idOrder"] = orderProduct.idOrder;
                    reqParams["action"] = "drop";
                    reqParams["productData"] = data;
                    request.Post("https://siwitpro.com/purnov/crm_3/crm_3/EditOrder.php", reqParams);
                }
                data = string.Empty;
                reqParams = new RequestParams();
                request.AddHeader("token", getCoder.getToken(this.unId, "crm:getUrl"));
                foreach (Product item in orderProduct.products.FindAll(x => x.add == true))
                {
                    
                    data += item.idProduct + "," + item.sale + ";";
                }
                if (data != string.Empty) // Редактирование ДОБАВЛЕНИЕ НОВОГО ПРОДУКТА
                {
                    reqParams["idOrder"] = orderProduct.idOrder;
                    reqParams["productData"] = data;
                    reqParams["action"] = "add";
                    request.Post("https://siwitpro.com/purnov/crm_3/crm_3/EditOrder.php", reqParams);
                }
                data = string.Empty;
                reqParams = new RequestParams();
                request.AddHeader("token", getCoder.getToken(this.unId, "crm:getUrl"));
                foreach (Product item in orderProduct.products.FindAll(x => x.reg_sale == true))
                {
                   
                    data += item.idRel + "," + item.sale + ";";
                }
                if (data != string.Empty) // Редактирование ДОБАВЛЕНИЕ НОВОГО ПРОДУКТА
                {
                    reqParams["idOrder"] = orderProduct.idOrder;
                    reqParams["productData"] = data;
                    reqParams["action"] = "sale";
                    request.Post("https://siwitpro.com/purnov/crm_3/crm_3/EditOrder.php", reqParams);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }
        public bool EditName(int idUser, string name)
        {
            try
            {
                var request = new HttpRequest();
                var reqParams = new RequestParams();
                request.AddHeader("token", getCoder.getToken(this.unId, "crm:commentHandler"));
                reqParams["id_user"] = idUser;
                reqParams["action"] = "nameEdit";
                reqParams["name"] = name;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3/commentHandler.php", reqParams);
            }
            catch
            {
                return false;
            }
            return true;
        }
        public bool EditUTC(int idUser, string utc)
        {
            try
            {
                var request = new HttpRequest();
                var reqParams = new RequestParams();
                request.AddHeader("token", getCoder.getToken(this.unId, "crm:commentHandler"));
                reqParams["id_user"] = idUser;
                reqParams["action"] = "utcEdit";
                reqParams["utc"] = utc;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3/commentHandler.php", reqParams);
            }
            catch
            {
                return false;
            }
            return true;

        }
        public bool SendMessageUser(int idUser, string message, int attr, string name)
        {
            try
            {
                var request = new HttpRequest();
                var reqParams = new RequestParams();
                request.AddHeader("token", getCoder.getToken(this.unId, "crm:commentHandler"));
                reqParams["id_user"] = idUser;
                reqParams["action"] = "message";
                reqParams["message"] = message;
                reqParams["attr"] = attr;
                reqParams["name"] = name;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3/commentHandler.php", reqParams);
            }
            catch
            {
                return false;
            }
            return true;
        }
        public void setPlaning(int id_user, string type_task, int course_id, int count_day, string task_desc, string manager, string crm_task, string comment, string problem, string url_sc, string email,int idUrl)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:setPlanning"));
            try
            {
                reqParams["idUrl"] = idUrl;
                reqParams["comment"] = comment;
                reqParams["id_user"] = id_user;
                reqParams["type_task"] = type_task;
                reqParams["curse_id"] = course_id;
                reqParams["count_day"] = count_day;
                reqParams["task_desc"] = task_desc;
                reqParams["manager"] = manager;
                reqParams["crm_task"] = crm_task;
                reqParams["problem"] = problem;
                reqParams["url_sc"] = url_sc;
                request.Post("https://siwitpro.com/purnov/admin_lc/setPlanning.php", reqParams).ToString();
                if (problem == "novid_lk" || problem == "video_lk" || problem == "avtorizacyion_lk" || problem == "instal_lk")
                {
                    request = new HttpRequest();
                    reqParams["email"] = email;
                    reqParams["option"] = "problem";
                    request.Post("https://purnov.com/purnov/crm/massage/sendMsTask.php", reqParams);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        public List<CourseMain> GetCourseMains()
        {
            List<CourseMain> lst = new List<CourseMain>();
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getCourseMain"));
            try
            {
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3/getCourseMain.php").ToString();
                if (str != "null")
                {
                    lst = JsonConvert.DeserializeObject<List<CourseMain>>(str);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

            return lst;
        }
        public object GetMenagerUser(string search , int manager_id)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            DecompressData decompressData = new DecompressData();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getMenagerUserAll"));
            try
            {
                reqParams["search"] = search;
                reqParams["idManager"] = manager_id;
                string str =  request.Post("https://siwitpro.com/purnov/crm_3/crm_3/getMenagerUserAll.php",reqParams).ToString();

                try
                {
                    JObject.Parse(str);
                    return JsonConvert.DeserializeObject<InfoUser>(str);
                }
                catch
                {
                    str = decompressData.StartDecompress(str);
                    return JsonConvert.DeserializeObject<List<MTask>>(str);

                }
                
            }
            catch
            {
                return null;
            }
        }
        public List<TaskList> LstTask(DateTime dt_s, DateTime dt_e, int id_kind)
        {
            List<TaskList> lst_task = new List<TaskList>();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getDataPlannDescktop"));
            try
            {
                reqParams["idUrl"] = id_kind;
                reqParams["dt_s"] = dt_s.ToString("yyyy-MM-dd");
                reqParams["dt_e"] = dt_e.ToString("yyyy-MM-dd");
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/getDataPlannDescktop.php", reqParams).ToString();
                if (str != "null")
                {
                    lst_task = JsonConvert.DeserializeObject<List<TaskList>>(str);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

            return lst_task;
        }
        public List<Course_CL> GetСourses()
        {
            List<Course_CL> courses = new List<Course_CL>();
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:get_curse_all"));
            try
            {
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/get_curse_all.php").ToString();
                if (str != "null")
                {
                    courses = JsonConvert.DeserializeObject<List<Course_CL>>(str);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка интеренета " + ex.Message);
            }
            return courses;
        }
        public void SetChange(int idTask, int idChange)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            reqParams["idChange"] = idChange;
            reqParams["idTask"] = idTask;
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:changeTask"));
            try
            {
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/changeTask.php", reqParams);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        public List<ProductList> GetProductLists()
        {
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getProductID"));
            try
            {
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/getProductID.php").ToString();

                return JsonConvert.DeserializeObject<List<Class.ProductList>>(str);
            }
            catch
            {
                return null;
            }
        }
        public string GetFormData(int idUser)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            reqParams["id"] = idUser;
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:getFilteData"));
            try
            {
                DecompressData decompressData = new DecompressData();
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getFilteData.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                return str;
            }
            catch
            {
                return string.Empty;
            }
        }
        public bool CreatTask(int idUser, DateTime dateTouch, int productId, int stageId, int managerId, string them, string desc)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "crm:crateTask"));
            reqParams["idUser"] = idUser;
            reqParams["productID"] = productId;
            reqParams["stageID"] = stageId;
            reqParams["managerID"] = managerId;
            reqParams["them"] = them;
            reqParams["dateTouch"] = dateTouch.ToString("yyyy-MM-dd");  
            reqParams["desc"] = desc;
            try
            {
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/crateTask.php", reqParams);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }


        }

    }
}
