﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace crm_3._0.Hendler
{
   public class NotificationHendler
    {
        public DateTime dateTime { get; set; }
        public int idTask { get; set; }
        public string info { get; set; }

    }
   public class NotificationProcess
    {
        public List<NotificationHendler> notifications;

        public NotificationProcess()
        {
            notifications = GetFileData();
        }
        private List<NotificationHendler> GetFileData()
        {
            string data = string.Empty;
            try
            {
                using (FileStream fstream = new FileStream(@"notification.inf", FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    byte[] array = new byte[fstream.Length];
                    fstream.Read(array, 0, array.Length);
                    data = System.Text.Encoding.Default.GetString(array);
                    fstream.Close();
                }
                if (data != string.Empty)
                {
                    try
                    {
                       return JsonConvert.DeserializeObject<List<NotificationHendler>>(data);
                    }
                    catch
                    {
                        return new List<NotificationHendler>();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Запустите приложение от имени администратора!");
            }

            return new List<NotificationHendler>();
        }
        public void SetNotification(int idTask, DateTime date, string info)
        {
            notifications.Add(new NotificationHendler { dateTime = date, idTask = idTask, info = info });
            try
            {
                try
                {
                    string json = JsonConvert.SerializeObject(notifications);
                    byte[] mybyte = System.Text.Encoding.UTF8.GetBytes(json);
                    
                    using(FileStream fstream = new FileStream(@"notification.inf", FileMode.Create, FileAccess.ReadWrite))
                    {
                        byte[] array = System.Text.Encoding.Default.GetBytes(json);
                        fstream.Write(array, 0, array.Length);
                        fstream.Close();
                    }
                }
                catch
                {
                    MessageBox.Show("Ошибка записи");
                }
            }
            catch
            {
                MessageBox.Show("Запустите приложение от имени администратора!");
            }

        }
        public void RemoveNotificaion(NotificationHendler notificationRemove)
        {
            if (notifications != null && notificationRemove != null)
            {
                notifications.Remove(notificationRemove);
                try
                {
                    string json = JsonConvert.SerializeObject(notifications);
                    byte[] mybyte = System.Text.Encoding.UTF8.GetBytes(json);

                    using (FileStream fstream = new FileStream(@"notification.inf", FileMode.Create, FileAccess.ReadWrite))
                    {
                        byte[] array = System.Text.Encoding.Default.GetBytes(json);
                        fstream.Write(array, 0, array.Length);
                        fstream.Close();
                    }
                }
                catch
                {
                    MessageBox.Show("Ошибка записи");
                }
            }
        
        }

        public NotificationHendler GetNotification()
        {
            return notifications.Find(x => x.dateTime.ToString("dd.MM.yyyy hh:mmm") == DateTime.Now.ToString("dd.MM.yyyy hh:mmm") || x.dateTime <= DateTime.Now);
        }
    }
}
