﻿using crm_3._0.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Hendler
{
    class SortFilter
    {

        public List<MTask> GetSortTask(string product, string them, int numberTask, int priority, int stage, List<MTask> mTasks, string global )
        {
            if (global != string.Empty)
            {
                mTasks = mTasks.FindAll(x => x.nameUser.ToLower().Contains(global.ToLower()) || x.fileterData.ToLower().Contains(global.ToLower()));
            }
            if (product != string.Empty)
            {
                mTasks = mTasks.FindAll(x => x.nameProduct == product );
            }
            if (them != string.Empty)
            {
                mTasks = mTasks.FindAll(x => x.title.Contains(them));
            }
            if (numberTask != -1)
            {
                mTasks = mTasks.FindAll(x => x.idTask == numberTask);
            }
            if (priority != -1)
            {
                mTasks = mTasks.FindAll(x => x.rating == priority);
            }
            if (stage != -1)
            {
                mTasks = mTasks.FindAll(x => x.type == stage);
            }

            return mTasks;
        }
    }
}
