﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Class
{
    class CalendarData
    {
        [JsonProperty(PropertyName = "count")]
        public int count_ { get; set; }
        [JsonProperty(PropertyName = "date")]
        public DateTime date { get; set; }
    }
}
