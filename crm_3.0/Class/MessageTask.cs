﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Class
{
    public class MessageTask
    {
        [JsonProperty(PropertyName = "id_ms")]
        public int id_ms { get; set; }
        [JsonProperty(PropertyName = "text")]
        public string message { get; set; }
        [JsonProperty(PropertyName = "date")]
        public DateTime date { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
    }
}
