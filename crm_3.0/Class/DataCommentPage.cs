﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Class
{
    public class DataCommentPage
    {
       
        [JsonProperty(PropertyName = "Messages")]
        public List<Messages> messages { get; set; }
    }
    public class Messages
    {
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }
        [JsonProperty(PropertyName = "date")]
        public DateTime date { get; set; }
        [JsonProperty(PropertyName = "attr")]
        public int attr { get; set; }
    }
}
