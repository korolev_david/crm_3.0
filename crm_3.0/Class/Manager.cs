﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Class
{
    public class Manager
    {
        [JsonProperty(PropertyName = "name_manger")]
        public string ManagerName { get; set; }
        [JsonProperty(PropertyName = "id_mng")]
        public int idManager { get; set; }
        [JsonProperty(PropertyName = "object_type_id")]
        public int ManagerType { get; set; }
        [JsonProperty(PropertyName = "profel")]
        public string profel { get; set; }
    }
}
