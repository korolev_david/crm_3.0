﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Class
{
    class Orders
    {
        [JsonProperty(PropertyName = "idOrder")]
        public int idOrder { get; set; }
        [JsonProperty(PropertyName = "date")]
        public DateTime date { get; set; }
        [JsonProperty(PropertyName = "nameManager")]
        public string nameManager { get; set; }
        [JsonProperty(PropertyName = "product")]
        public string product { get; set; }
        [JsonProperty(PropertyName = "sum")]
        public double sum { get; set; }
        [JsonProperty(PropertyName = "sumSale")]
        public double sumSale { get; set; }
        [JsonProperty(PropertyName = "sale")]
        public int sale { get; set; }
        [JsonProperty(PropertyName = "payment")]
        public double payment { get; set; }
        [JsonProperty(PropertyName = "rest")]
        public double rest { get; set; }

        public double balance { get { return GetBalance(); } }
        public double saleSum { get { return GetSumSale(); } }

        public double GetBalance()
        {
            return sum - ((sum * sale / 100) - payment);
        }
        public double GetSumSale()
        {
            return sum - (sum * sale / 100);
        }
    }
}
