﻿using crm_3._0.Hendler;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace crm_3._0.Class
{
    public class MTask : TStage
    {
        [JsonProperty(PropertyName = "task_id")]
        public int idTask { get; set; }
        [JsonProperty(PropertyName = "tObject_id")]
        public int idUser { get; set; }
        [JsonProperty(PropertyName = "tProductId")]
        public int idProduct { get; set; }
        [JsonProperty(PropertyName = "tTitle")]
        public string title { get; set; }
        [JsonProperty(PropertyName = "tCompleted")] [JsonConverter(typeof(BoolConverter))]
        public bool tcompleted { get; set; }
        [JsonProperty(PropertyName = "tDescription")]
        public string description { get; set; }
        [JsonProperty(PropertyName = "tRating")]
        public int rating { get; set; }
        [JsonProperty(PropertyName = "tProductName")]
        public string nameProduct { get; set; }
        [JsonProperty(PropertyName = "UName")]
        public string nameUser { get; set; }
        [JsonProperty(PropertyName = "filterUser")]
        public string  fileterData { get; set; }
        [JsonProperty(PropertyName = "idOrder")]
        public int idOrder { get; set; }
        [JsonProperty(PropertyName = "countT")]
        public int countTask { get; set; }
        public bool flagProcess { get; set; } = false;
        public bool flagClose { get; set; } = false;
    }
}
