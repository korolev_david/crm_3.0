﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Class
{
    class UserInfo
    {
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "UTC")]
        public string utc { get; set; }
        [JsonProperty(PropertyName = "country")]
        public string country { get; set; }
        [JsonProperty(PropertyName = "sex")]
        public string sex { get; set; }
        [JsonProperty(PropertyName = "skype")]
        public string skype { get; set; }
        [JsonProperty(PropertyName = "birthday")]
        public string brithday { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string email { get; set; }
        [JsonProperty(PropertyName = "phone")]
        public string phone { get; set; }
        [JsonProperty(PropertyName = "ext_email")]
        public string extra_email_d { get; set; }
        [JsonProperty(PropertyName = "ext_phone")]
        public string extra_phone_d { get; set; }
        [JsonProperty(PropertyName = "date_created")]
        public DateTime date { get; set; }

        public string [] extra_email { get 
            {
                if (extra_email_d != null)
                {
                    return extra_email_d.Split(';');
                }
                else
                {
                    return null;
                }
               
            } }
        public string[] extra_phone { get 
            {
                if (extra_phone_d != null)
                {
                    return extra_phone_d.Split(';');
                }
                else
                    return null;
               
            } }

    }
    class DataCourse
    {
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "date")]
        public DateTime date { get; set; }
        [JsonProperty(PropertyName = "status")]
        public int status { get; set; }
    }
    class DataUrl
    {
        [JsonProperty(PropertyName = "url")]
        public string url { get; set; }
        [JsonProperty(PropertyName = "date")]
        public DateTime date { get; set; }
      

    }
}
