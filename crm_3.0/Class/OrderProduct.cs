﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Class
{
    class OrderProduct
    {
        [JsonProperty(PropertyName = "order_id")]
        public int idOrder { get; set; }
        [JsonProperty(PropertyName = "product")]
        public List<Product> products { get; set; } 

    }
}
