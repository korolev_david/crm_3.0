﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Class
{
    public  class InfoUser
    {
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "manager")]
        public string manager { get; set; }
        [JsonProperty(PropertyName = "phone")]
        public string phone { get; set; }
      
        public string email { get; set; }
        [JsonProperty(PropertyName = "cursUser")]
        public List<CourseInfoUser> courseInfoUsers { get; set; }

    }
    public class CourseInfoUser
    {
        [JsonProperty(PropertyName = "name_curs")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "date_cur")]
        public DateTime date { get; set; }
    }
}
