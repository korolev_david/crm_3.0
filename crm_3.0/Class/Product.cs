﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0.Class
{
    class Product:ProductEdit
    {
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "productId")]
        public int idProduct { get; set; }
        [JsonProperty(PropertyName = "cost")]
        public double cost { get; set; }
        [JsonProperty(PropertyName = "sale")]
        public int sale { get; set; }
        [JsonProperty(PropertyName = "rel_id")]
        public int idRel { get; set; }
    }

    abstract class ProductEdit
    {

        public bool reg_sale { get; set; } = false;
        public bool drop { get; set; } = false;
        public bool add { get; set; } = false;
    }
}
