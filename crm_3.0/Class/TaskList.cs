﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0
{
    public class TaskList
    {
        [JsonProperty(PropertyName = "date")]
        public DateTime date { get; set; }
        [JsonProperty(PropertyName = "name_user")]
        public string name_user { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string email { get; set; }
        [JsonProperty(PropertyName = "phone")]
        public string phone { get; set; }
        [JsonProperty(PropertyName = "manager")]
        public string manager { get; set; }
        [JsonProperty(PropertyName = "type_tsk")]
        public string type_task { get; set; }   
        [JsonProperty(PropertyName = "problem")]
        public string problem { get; set; }
        [JsonProperty(PropertyName = "name_curs")]
        public string course { get; set; }
        [JsonProperty(PropertyName = "count_d")]
        public string count_day { get; set; }
        [JsonProperty(PropertyName = "task_desc")]
        public string disc_task { get; set; }
        [JsonProperty(PropertyName = "id_planning")]
        public int id_task { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string status { get; set; }
        [JsonProperty(PropertyName = "executor")]
        public string executor { get; set; }
        [JsonProperty(PropertyName = "date_last_status")]
        public string date_last { get; set; }
        [JsonProperty(PropertyName = "comments_executor")]
        public string executor_comment { get; set; }


        public string StatusRename
        {
            get 
            {
                switch (this.status)
                {

                    case "queue":
                        return "В очереди";
                    case "in_progress":
                        return "Выполняется";
                    case "connection_mail":
                        return "Написал на почту. Ожидаю ответа";
                    case "done":
                        return "Выполненно";
                    case "rejected":
                        return "Отклонена";
                    case "pause":
                        return "Приостановлена";

                
                }
                return "В очереди";
            }
        }

        public int ColorStatus
        { 
         get 
            {
                switch (this.status)
                {

                    case "queue":
                        return 0;
                    case "in_progress":
                        return 1;
                    case "connection_mail":
                        return 2;
                    case "done":
                        return 3;
                    case "rejected":
                        return 4;
                    case "pause":
                        return 5;

                
                }
                return 0;
            }
        }
        public string EssenceTask
        {
            get
            {
                switch (this.problem)
                {
                    case "instal_lk":
                        return "Проблемы с установкой ЛК";
                    case "avtorizacyion_lk":
                        return "Проблемы с входом в ЛК";
                    case "video_lk":
                        return "Проблемы с воспроизведением видео";
                    case "novid_lk":
                        return "Не находит видеозаписи";
                    case "to_test":
                        return "Провести тестирование";
                    case "else":
                        return "Другое";
                }
                return "";
            }
        }
        public string TypeTask
        {
            get
            {
                switch (this.type_task)
                {
                    case "open_records":
                        return "Открыть записи";
                    case "close_records":
                        return "Закрыть записи";
                    case "suspend_learning":
                        return "Приостановить обучение";
                    case "extend_access":
                        return "Продлить доступ к записям";
                    case "change_kurs":
                        return "Перевести на другой курс";
                    case "contact_student":
                        return "Связаться с учеником";
                    case "edit_student":
                        return "Заменить номер";
                }
                return "";
            }
        }

        public string CloseTaskDate
        {
            get
            {
                DateTime date = new DateTime();
                if (DateTime.TryParse(this.date_last,  out date))
                {

                    return date.ToString("dd.MM.yyyy hh:mm");
                }
                return "";
            }
        }


    }
}
