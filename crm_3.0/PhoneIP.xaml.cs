﻿using crm_3._0.Class;
using crm_3._0.Hendler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using ZoiperAPI;

namespace crm_3._0
{
    /// <summary>
    /// Логика взаимодействия для PhoneIP.xaml
    /// </summary>
    public partial class PhoneIP : Window
    {
        public bool bHold = true;            ///< A temp variable holds if a call is holded
        public string lastCall = "";         ///< A variable that holds the call time span
        public int numCalls = 0;             ///< A variable that holds the current call
        public IZoiperCall callTemp;         ///< A tempoary call variable for increace the call list
        public DispatcherTimer newTimer;     ///< A timer for showing the call duration
        public ZoiperAPI.ZoiperAPI myZoiper;      ///< A phone variable
        public List<IZoiperCall> calls = new List<IZoiperCall>();   ///< A list of calls
        public List<bool> bTimes = new List<bool>();                ///< A list of flags for every call
        public IZoiperAccount tempAccount;
        int id_us;
        string phone;
        Manager mng_;
        string time_call = string.Empty;
        string name_user = string.Empty;
        bool check_call_hand = false;
        bool check_call = false;
        DateTime dt_now = new DateTime();
        MTask mTask;
        MainWindow mainW;
        DispatcherTimer timerStatus = new DispatcherTimer();
        int timeStep = 0;
        public PhoneIP(Manager manager, string dataPhone, MTask mTask, MainWindow mainW)
        {
            InitializeComponent();
            //mainW.workingLeads.Add(new WorkingLead { idUser = mTask.idUser, callUser = true });
            mng_ = manager;
            this.mTask = mTask;
            this.mainW = mainW;
            timerStatus.Interval = TimeSpan.FromSeconds(1);
            timerStatus.Tick += OnTimerTickPhone;

            string[] data = Regex.Split(dataPhone, "<data>");
            string[] phone = Array.FindAll(data, x => !x.Contains("@") && x.Contains("+"));
            if (Array.FindAll(phone, x => x.Contains(";")).Length > 0)
            {
                string dopPhone = phone[Array.FindIndex(phone, x => x.Contains(";"))];
                phone = Array.FindAll(phone, x => !x.Contains(";"));
                string[] dopEmailArr = dopPhone.Split(';');
                foreach (string item in dopEmailArr)
                    if (item != string.Empty) numberCallCombo.Items.Add(item);

                foreach (string item in phone)
                    if (item != string.Empty) numberCallCombo.Items.Add(item);
            }
            else if (phone.Length > 0)
            {
                foreach (var item in phone)
                    numberCallCombo.Items.Add(item);
            }
            if (numberCallCombo.Items.Count > 0)
            {
                numberCallCombo.SelectedIndex = 0;
            }
        }

        private void OnTimerTickPhone(object sender, EventArgs e)
        {
            timeStep++;
            if(timeStep == 15)
            {
                mTask.flagProcess = true;
                timerStatus.Stop();
            }
            
        }

        public void OnZoiperCallAccept(IZoiperCall iz)
        {
            this.Dispatcher.Invoke(() =>
            {
                bTimes[numCalls] = true;
                newTimer.Start();
                dt_now = DateTime.Now;


            });
        }
        public void OnTimerTick(Object sender, EventArgs args)
        {
            long tick = DateTime.Now.Ticks - dt_now.Ticks;
            DateTime watch = new DateTime();
            watch = watch.AddTicks(tick);
            txt_time.Text = String.Format("{0:0:mm:ss}", watch);
        }

        public void OnZoiperCallHang(IZoiperCall iz)
        {
            this.Dispatcher.Invoke(() =>
            {
                text_info.Text = "Звонок завершен...";
                time_call = txt_time.Text;
            });
            bTimes[numCalls] = false;
            newTimer.Stop();
        }

     


        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        bool check_set = true;
        private async void CallPhone_Click(object sender, RoutedEventArgs e)
        {
            if (numberCallCombo.SelectedIndex != -1)
            {
                check_call = true;
                text_info.Text = "";
                text_info.Text = "Звонок начался...";
                calls[numCalls] = myZoiper.Dial(phone, 0);
                bTimes[numCalls] = (true);
                check_call_hand = true;
                timerStatus.Start();
                if (check_set)
                {
                    check_set = false;
                    DataBase dataBase = new DataBase(mainW.manager.idManager);
                    mainW.workingLeads.Add(new WorkingLead { idUser = mTask.idUser, callUser = true });
                    await Task.Run(() => { dataBase.SetChange(mTask.idTask, 12); });
                }
            }
           
        }

        public void CallDown_Click(object sender, RoutedEventArgs e)
        {
            if (check_call_hand)
            {
                calls[numCalls].Hang();
            }
            check_call_hand = false;
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (check_call_hand)
            {
                calls[numCalls].Hang();
            }

            check_call_hand = false;
            //await Task.Factory.StartNew(get_call_inf);
            this.Close();
        }
        //void get_call_inf()
        //{
        //    if (check_call)
        //    {
        //        SetParam dateUpdate = new SetParam();
        //        dateUpdate.setDateDataBase(DateTime.Now, 137, UserSelect, client);
        //        UserSelect.dt_last_call = DateTime.Now;
        //        var request = new HttpRequest();
        //        var reqParams = new RequestParams();
        //        string set_messege = string.Empty;
        //        string content = string.Empty;
        //        try
        //        {
        //            reqParams["phone_in"] = phone.Trim('+');
        //            content = request.Post("https://purnov.com/api/binotel/checknew.php", reqParams).ToString();
        //            content = content.Replace("п»ї", "");
        //        }
        //        catch (Exception ex)
        //        {

        //            using (FileStream fs = new FileStream("log.txt", FileMode.Append, FileAccess.Write))
        //            {
        //                byte[] buff = Encoding.Default.GetBytes(@"get_call_inf: ->" + " " + ex.Data + "->" + ex.Message + " -> " + ex.Source + "    |    ");
        //                fs.Write(buff, 0, buff.Length);
        //                fs.Flush();
        //                fs.Close();
        //            }
        //        }
        //        if (content.Length != 0)
        //        {
        //            //ins_db.update_date_call(id_us);

        //            string[] mass_all = { "ANSWER", "CANCEL", "BUSY", "NOANSWER", "CONGESTION", "CHANUNAVAIL", "TRANSFER" };
        //            string[] mass_rus = { "Успешный звонок", "Неуспешный звонок по причине отмены звонка", "Неуспешный звонок по причине занятости", "Неуспешный звонок по причине не ответа", "Неуспешный звонок", "Неуспешный звонок", "Успешный звонок который был переведен" };

        //            for (int i = 0; i < mass_all.Length; i++)
        //            {

        //                if (content == mass_all[i])
        //                {
        //                    if (time_call == "")
        //                    {
        //                        set_messege = mng_.name_manger + ". " + mass_rus[i] + ". " + DateTime.Now + ";";
        //                    }
        //                    else
        //                    {
        //                        try
        //                        {
        //                            set_messege = mng_.name_manger + ". " + mass_rus[i] + ". " + "Длительность звонка: " + time_call + ". " + DateTime.Now + ";";
        //                            UserSelect.call_history += set_messege;
        //                            dateUpdate.setParamDataBase(UserSelect.call_history, 120, UserSelect, client);
        //                            //reqParams["phone"] = phone.Trim('+');
        //                            //reqParams["id_user"] = id_us;
        //                            //reqParams["name_user"] = name_user;
        //                            //string urls = request.Post("https://purnov.com/api/binotel/save_zapis.php", reqParams).ToString() ;
        //                            //if(urls != string.Empty)
        //                            //{
        //                            //    UserSelect.call_url_audio = urls;
        //                            //    dateUpdate.setParamDataBase(UserSelect.call_url_audio, 135, UserSelect, client);
        //                            //}

        //                        }
        //                        catch (Exception ex)
        //                        {

        //                            using (FileStream fs = new FileStream("log.txt", FileMode.Append, FileAccess.Write))
        //                            {
        //                                byte[] buff = Encoding.Default.GetBytes(@"get_call_inf: ->" + " " + ex.Data + "->" + ex.Message + " -> " + ex.Source + "    |    ");
        //                                fs.Write(buff, 0, buff.Length);
        //                                fs.Flush();
        //                                fs.Close();
        //                            }
        //                        }

        //                    }
        //                    break;
        //                }
        //            }

        //        }


        //    }
        //}

     
        private void numberCallCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            phone = numberCallCombo.Items[numberCallCombo.SelectedIndex].ToString();
        }

        private void Window_ContentRendered_1(object sender, EventArgs e)
        {
            try
            {

                myZoiper = new ZoiperAPI.ZoiperAPI();
                
                calls.Add(callTemp);
                bTimes.Add(false);
                myZoiper.UseAccount(mng_.profel);
                newTimer = new DispatcherTimer();
                newTimer.Interval = TimeSpan.FromSeconds(1);
                newTimer.Tick += OnTimerTick;
                myZoiper.OnZoiperCallAccept += OnZoiperCallAccept;
                myZoiper.OnZoiperCallHang += OnZoiperCallHang;
               
            }
            catch
            {
                this.Close();
                MessageBox.Show("Звонок с CRM не возможен.");
            }
        }

      
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (timeStep == 15)
            {
                mTask.flagProcess = true;
                timerStatus.Stop();
            }
            else
            {
                mTask.flagProcess = false;
                timerStatus.Stop();
            }
        }
    }
}
