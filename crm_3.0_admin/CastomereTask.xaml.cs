﻿using crm_3._0_admin.Handler;
using crm_3._0_admin.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0_admin
{
    /// <summary>
    /// Логика взаимодействия для CastomereTask.xaml
    /// </summary>
    public partial class CastomereTask : Window
    {
        DataBase db;
        List<TaskList> lst = new List<TaskList>();
        public CastomereTask(Manager manager )
        {
            InitializeComponent();
            db = new DataBase(manager.id);
        }

        private async void view_data_Click(object sender, RoutedEventArgs e)
        {
            
            var dt_s = dateStart.SelectedDate;  //dateStart.SelectedDate;
            var dt_e = dateEnd.SelectedDate;
            dt_e = (DateTime)dt_e.Value.AddDays(1);
            lst = await Task.Run(() => db.LstTask((DateTime)dt_s, (DateTime)dt_e, 3));
            task_data.ItemsSource = lst;
        }
    }
}
