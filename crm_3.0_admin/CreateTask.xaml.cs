﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using ExcelDataReader.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0_admin
{
    /// <summary>
    /// Логика взаимодействия для CreateTask.xaml
    /// </summary>
    public partial class CreateTask : Window
    {
        List<Users> users;
        DataBase dataBase;
        List<LogCreate> logCreates = new List<LogCreate>();
        List<Manager> mng = new List<Manager>();
        List<ProductList> prd = new List<ProductList>();
        public CreateTask(List<Users> users, Manager manager)
        {
            InitializeComponent();
            this.users = users;
            listViewLog.ItemsSource = logCreates;
            dataBase = new DataBase(manager.id);
        }

        private async void creationTask_Click(object sender, RoutedEventArgs e)
        {
           
            if (users != null && users.Count > 0)
            {
              
                if (dateCreation.SelectedDate != null && cmbxProduct.SelectedIndex != -1
                    && cmbxStage.SelectedIndex != -1 && cmbxManager.SelectedIndex != -1 &&
                    thems.Text != string.Empty && description.Text != string.Empty)
                {
                    SettingCreate.Visibility = Visibility.Collapsed;
                    StatCreation.Visibility = Visibility.Visible;
                    DateTime date = (DateTime)dateCreation.SelectedDate;
                    int productId = ((ProductList)cmbxProduct.SelectedItem).id;
                    int stageId = cmbxStage.SelectedIndex + 1;
                    int idManager = ((Manager)cmbxManager.SelectedItem).id;
                    string themStr = thems.Text;
                    string desc = description.Text;

                    for (int i = 0; i < users.Count; i++)
                    {
                      
                        if (users[i].managerInTask == null ? true: users[i].managerInTask.TrueForAll( x => x == idManager))
                        {
                            if  (users[i].productInTask == null ? true : !users[i].productInTask.Any(x => x == productId))
                            {
                                bool check = true;
                                await Task.Run(() => { check = dataBase.CreatTask(users[i].id, date, productId, stageId, idManager, themStr, desc); });
                                if(check)
                                {
                                    countCreate.Text = (int.Parse(countCreate.Text) + 1).ToString();
                                    users[i].taskData =  users[i].taskData == "0" ? productId + "|" + idManager + "|" + "1" : users[i].taskData + ";" + productId + "|" + idManager + "|" + "1";
                                   
                                }
                                else
                                {
                                    logCreates.Add(new LogCreate { name = "Дублирование задачи/неверный менеджер", desc = "У пользователь ID: " + users[i].id + " задача не создана" });
                                    listViewLog.Items.Refresh();
                                    countError.Text = (int.Parse(countError.Text) + 1).ToString();
                                }
                            }
                            else
                            {
                                logCreates.Add(new LogCreate { name = "Дублирование", desc = "У пользователь ID: " + users[i].id + " ecть уже эта задача" });
                                listViewLog.Items.Refresh();
                                countDuplicat.Text = (int.Parse(countDuplicat.Text)+1).ToString();
                            }
                        }
                        else
                        {
                            int idMng = 0;
                            foreach(int item in users[i].managerInTask)
                            {
                                idMng = item;
                            }
                            logCreates.Add(new LogCreate { name = "Несоответствие", desc = "У пользователь ID: "+ users[i].id+ " ecть задачи "+ mng.Find(x => x.id == idMng).name });
                            listViewLog.Items.Refresh();
                            countErrorManager.Text = (int.Parse(countErrorManager.Text) + 1).ToString();
                        }
                    
                    
                    }

                   
                }
                else
                    MessageBox.Show("Не все поля заполнены");
              
            }

        }

        private void creationClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private async void Window_ContentRendered(object sender, EventArgs e)
        {
           
            await Task.Run(() => { mng = dataBase.GetManagers(); });
            cmbxManager.ItemsSource = mng;
            await Task.Run(() => { prd = dataBase.GetProductLists(); });
            cmbxProduct.ItemsSource = prd;


        }

        private void thems_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (thems.Text.Length > 60)
            {
                
                thems.Text = thems.Text.Remove(60, thems.Text.Length - 60);

            }
        }

        private void description_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (description.Text.Length > 150)
            {

                description.Text = description.Text.Remove(150, description.Text.Length - 150);

            }
        }

        private void backCreation_Click(object sender, RoutedEventArgs e)
        {
            SettingCreate.Visibility = Visibility.Visible;
            StatCreation.Visibility = Visibility.Collapsed;
            countErrorManager.Text = "0";
            countDuplicat.Text = "0";
            countError.Text = "0";
            logCreates = new List<LogCreate>();
            listViewLog.ItemsSource = logCreates;
            listViewLog.Items.Refresh();
        }

        private void searchProduct_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if(searchProduct.Text != string.Empty)
            {
                cmbxProduct.ItemsSource = prd.FindAll(x => x.name.ToLower().Contains(searchProduct.Text.ToLower()));

            }
            else
                cmbxProduct.ItemsSource = prd;
        }
    }
}
