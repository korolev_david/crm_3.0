﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    public class CreatedProductClass
    {
        [JsonProperty(PropertyName = "id")]
        public int id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "cost")]
        public int coast { get; set; }
        [JsonProperty(PropertyName = "disk")]
        public string disk { get; set; }
        [JsonProperty(PropertyName = "rel")]
        public string rel_curse { get; set; }
        [JsonProperty(PropertyName = "date")]
        public string date { get; set; }
    }
    public class AddRefCurs
    {
        [JsonProperty(PropertyName = "id")]
        public int id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
    }
}
