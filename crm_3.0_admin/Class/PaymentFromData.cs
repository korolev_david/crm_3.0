﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    class PaymentFromData
    {
        public double comission { get; set; }
        public string confirm { get; set; }
        public string currency { get; set; }
        public string customer { get; set; }
        public DateTime date { get; set; }
        public DateTime date_payment { get; set; }
        public int id { get; set; }
        public string note { get; set; }
        public string SourceInput { get { return source == null ? "Источник не указан": source; } }
        public string source { get; set; }
        public double sum { get; set; }
        public string name { get; set; }
    }
}

