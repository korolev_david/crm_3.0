﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    class InvoiceExcel
    {
        [JsonProperty(PropertyName = "idOrder")]
        public int idOrder { get; set; }
        [JsonProperty(PropertyName = "date")]
        public DateTime date { get; set; }
        [JsonProperty(PropertyName = "nameManager")]
        public string nameManager { get; set; }
        [JsonProperty(PropertyName = "product")]
        public string product { get; set; }
        [JsonProperty(PropertyName = "sum")]
        public double sum { get; set; }
        [JsonProperty(PropertyName = "sumSale")]
        public double sumSale { get; set; }
        [JsonProperty(PropertyName = "payment")]
        public double payment { get; set; }
        [JsonProperty(PropertyName = "rest")]
        public double rest { get; set; }
        [JsonProperty(PropertyName = "dataSearch")]
        public string dataUser { get; set; }
        [JsonProperty(PropertyName = "sale")]
        public int sale { get; set; }
        [JsonProperty(PropertyName = "profit")]
        public double profit { get; set; }
    }

    class PaymentOrder
    {
        [JsonProperty(PropertyName = "payment")]
        public int payment { get; set; }
        [JsonProperty(PropertyName = "date")]
        public DateTime date { get; set; }

    }
    public class ProductOrders
    {
        public string nameProduct { get; set; }

    }
    public class ManagerOrders
    {
        public string nameManager { get; set; }
    }

}
