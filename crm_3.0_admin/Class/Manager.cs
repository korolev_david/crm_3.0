﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    public class Manager
    {
      
        [JsonProperty(PropertyName = "name_manger")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "id_mng")]
        public int id { get; set; }
        [JsonProperty(PropertyName = "acces")]
        public int acces { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string status { get; set; }
        [JsonProperty(PropertyName = "object_type_id")]
        public int type_id { get; set; }
    }
}
