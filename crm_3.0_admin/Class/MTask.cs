﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    public class MTask : TStage
    {
        [JsonProperty(PropertyName = "task_id")]
        public int idTask { get; set; }
        [JsonProperty(PropertyName = "tObject_id")]
        public int idUser { get; set; }
        [JsonProperty(PropertyName = "tProductId")]
        public int idProduct { get; set; }
        [JsonProperty(PropertyName = "tTitle")]
        public string title { get; set; }
        [JsonProperty(PropertyName = "tCompleted")]
        public int tcompleted { get; set; }
        [JsonProperty(PropertyName = "tDescription")]
        public string description { get; set; }
        [JsonProperty(PropertyName = "tRating")]
        public int rating { get; set; }
        [JsonProperty(PropertyName = "tProductName")]
        public string nameProduct { get; set; }
        [JsonProperty(PropertyName = "UName")]
        public string nameUser { get; set; }
        [JsonProperty(PropertyName = "filterUser")]
        public string fileterData { get; set; }
        [JsonProperty(PropertyName = "idOrder")]
        public int idOrder { get; set; }
        [JsonProperty(PropertyName = "managerName")]
        public string manager { get; set; }
        [JsonProperty(PropertyName = "tCreation")]
        public DateTime tCreation { get; set; }
        [JsonProperty(PropertyName = "managerId")]
        public int managerId { get; set; }
        public bool select { get; set; }
        public int state
        {
            get
            {

                if (tcompleted == 0 && date_touch.Date < DateTime.Now.Date)
                {
                    return 2;
                }
                else if (tcompleted == 2)
                {
                    return 3;
                }
                else
                    return tcompleted;
            }
        }
    }
}
