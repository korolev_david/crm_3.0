﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    public class TStage
    {
        [JsonProperty(PropertyName = "tsId")]
        public int idStage { get; set; }
        [JsonProperty(PropertyName = "tsType")]
        public int type { get; set; }
        [JsonProperty(PropertyName = "tsCompleted")]
        public int completed { get; set; }
        [JsonProperty(PropertyName = "tsDate_creation")]
        public DateTime date_creation { get; set; }
        [JsonProperty(PropertyName = "tsDate_touch")]
        public DateTime date_touch { get; set; }
    }
}
