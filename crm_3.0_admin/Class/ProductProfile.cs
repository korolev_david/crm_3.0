﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    class ProductProfile
    {
        [JsonProperty(PropertyName = "product_id")]
        public int product_id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "name_ip")]
        public string nameIp { get; set; }
        public bool select { get; set; }
    }
}
