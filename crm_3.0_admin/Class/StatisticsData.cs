﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    class StatisticsAll
    {
        [JsonProperty(PropertyName = "manager")]
        public string manager { get; set; }
        [JsonProperty(PropertyName = "call")]
        public int call { get; set; }
        [JsonProperty(PropertyName = "need")]
        public int need { get; set; }
        [JsonProperty(PropertyName = "present")]
        public int present { get; set; }
        [JsonProperty(PropertyName = "score")]
        public int score { get; set; }
        [JsonProperty(PropertyName = "payment")]
        public int payment { get; set; }
        [JsonProperty(PropertyName = "expired")]
        public int expired { get; set; }
        [JsonProperty(PropertyName = "close")]
        public int close { get; set; }
        [JsonProperty(PropertyName = "end")]
        public int end { get; set; }
    }
    class StatisticsRang
    {
        [JsonProperty(PropertyName = "manager")]
        public string manager { get; set; }
        [JsonProperty(PropertyName = "call")]
        public int call { get; set; }
        [JsonProperty(PropertyName = "need")]
        public int need { get; set; }
        [JsonProperty(PropertyName = "present")]
        public int present { get; set; }
        [JsonProperty(PropertyName = "score")]
        public int score { get; set; }
        [JsonProperty(PropertyName = "payment")]
        public int payment { get; set; }
        [JsonProperty(PropertyName = "expired")]
        public int expired { get; set; }
        [JsonProperty(PropertyName = "close")]
        public int close { get; set; }

    }
    class StatisticsRangProductStage
    {
        [JsonProperty(PropertyName = "product")]
        public string product { get; set; }
        [JsonProperty(PropertyName = "call")]
        public int call { get; set; }
        [JsonProperty(PropertyName = "need")]
        public int need { get; set; }
        [JsonProperty(PropertyName = "present")]
        public int present { get; set; }
        [JsonProperty(PropertyName = "score")]
        public int score { get; set; }
        [JsonProperty(PropertyName = "payment")]
        public int payment { get; set; }
        [JsonProperty(PropertyName = "expired")]
        public int expired { get; set; }
        [JsonProperty(PropertyName = "close")]
        public int close { get; set; }

    }
    class StatisticsRangCloseProduct
    {
        [JsonProperty(PropertyName = "product")]
        public string product { get; set; }
        [JsonProperty(PropertyName = "count")]
        public string count { get; set; }
        [JsonProperty(PropertyName = "change")]
        public string change { get; set; }
    }
    class StatisticsWithoutManager
    {
        [JsonProperty(PropertyName = "c_task")]
        public int count { get; set; }
        [JsonProperty(PropertyName = "c_from_task")]
        public int c_from { get; set; }
        [JsonProperty(PropertyName = "c_before_task")]
        public int c_before { get; set; }
    }
    class StatisticsChange
    {
        [JsonProperty(PropertyName = "manager")]
        public string manager { get; set; }
        [JsonProperty(PropertyName = "count")]
        public int count { get; set; }
        [JsonProperty(PropertyName = "chenge_id")]
        public int chenge_id { get; set; }
    }
    class StaticsUniqueTask
    {
        [JsonProperty(PropertyName = "manager")]
        public string manager { get; set; }
        [JsonProperty(PropertyName = "count")]
        public int count { get; set; }

    }
    class StatisticsLeadClass
    {

        [JsonProperty(PropertyName = "date")]
        public string date { get; set; }
        [JsonProperty(PropertyName = "count")]
        public int count { get; set; }
    }
    class StatisticsData
    {
        [JsonProperty(PropertyName = "all_information")]
        public List<StatisticsAll> statisticsAlls { get; set; }
        [JsonProperty(PropertyName = "UniqueTasksn")]
        public List<StaticsUniqueTask> staticsUniqueTasks { get; set; }
        [JsonProperty(PropertyName = "range")]
        public List<StatisticsRang> statisticsRangs { get; set; }
        [JsonProperty(PropertyName = "rangeChange")]
        public List<StatisticsChange> statisticsChanges { get; set; }
        [JsonProperty(PropertyName = "not_manager")]
        public StatisticsWithoutManager statisticsWithoutManager { get; set; }
    }

    class StatisticsPayment
    {
        [JsonProperty(PropertyName = "order_id")]
        public int order_id { get; set; }
        [JsonProperty(PropertyName = "manager")]
        public string maneger { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string email { get; set; }
        [JsonProperty(PropertyName = "payment")]
        public string payment { get; set; }
        [JsonProperty(PropertyName = "count_task")]
        public int count_task { get; set; }
        [JsonProperty(PropertyName = "date_creation_order")]
        public DateTime date_creation_order { get; set; }
        [JsonProperty(PropertyName = "date_creation_user")]
        public DateTime date_creation_user { get; set; }
    }

}
