﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    class IDProduct
    {
        [JsonProperty(PropertyName = "id_product")]
        public int id_product { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "hiden")]
        public int hiden { get; set; }
    }
}
