﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace crm_3._0_admin.Class
{
    public class Users
    {
        [JsonProperty(PropertyName = "id")]
        public int id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "dataSrc")]
        public string dataSrc { get; set; }
        [JsonProperty(PropertyName = "date")]
        public string dateSrc { get; set; }
        [JsonProperty(PropertyName = "courseId")]
        public string courseId { get; set; }
        [JsonProperty(PropertyName = "url")]
        public string url { get; set; }
        [JsonProperty(PropertyName = "task")]
        public string taskData { get; set; }
        public bool select { get; set; }
        public DateTime dateCreate
        {
            get
            {
                string[] src = dateSrc.Split(';');
                string searchCreate = Array.Find(src, x => x.ToString().Contains("|116"));
                return searchCreate != null ? DateTime.Parse(searchCreate.Substring(0, searchCreate.IndexOf('|'))) : DateTime.Parse("2015-01-01 00:00:00") ;
            }
        }
        public DateTime dateInSystem
        {
            get
            {
                string[] src = dateSrc.Split(';');
                string searchInSystem = Array.Find(src, x => x.ToString().Contains("|143"));
                return searchInSystem != null ? DateTime.Parse(searchInSystem.Substring(0, searchInSystem.IndexOf('|'))) : DateTime.Parse("2015-01-01 00:00:00");
            }
        }
        public string city
        {
            get
            {
                string[] src = dataSrc.Split(';');
                string searchCity = Array.Find(src, x => x.ToString().Contains("|89"));
                return searchCity != null ? searchCity.Substring(0, searchCity.IndexOf('|')) : "Not City";
            }
        }

        public string phone
        {
            get
            {
                string[] src = dataSrc.Split(';');
                string searchPh = Array.Find(src, x => x.ToString().Contains("|17"));
                return searchPh != null ? searchPh.Substring(0, searchPh.IndexOf('|')) : string.Empty;
            }
        }
        public string email
        { 
            get
            {
                string[] src = dataSrc.Split(';');
                string searchEmail = Array.Find(src, x => x.ToString().Contains("|58"));
                return searchEmail != null ? searchEmail.Substring(0, searchEmail.IndexOf('|')) : string.Empty;
            }
        }
        public List<string> urls
        {
            get 
            {
                string[] src = url.Split(';');
                return src.ToList<string>();
            }
        }
        public List<int> managerInTask
        {
            get
            {
                if(taskData != "0")
                {
                    string[] src = taskData.Split(';');
                    List<int> lst = new List<int>();
                    foreach(string item in src)
                    {
                        string[] itemSrc = item.Split('|');
                        if(itemSrc.Length < 3)
                        {
                            break;
                        }

                        lst.Add(int.Parse(itemSrc[1]));
                    }
                    return lst;
                }
                else
                {
                    return null;
                }
              
            }
        }
        public List<int> productInTask
        {
            get
            {
                if(taskData != "0")
                {
                    string[] src = taskData.Split(';');
                    List<int> lst = new List<int>();
                    foreach (string item in src)
                    {
                        string[] itemSrc = item.Split('|');
                        if (itemSrc.Length < 3)
                        {
                            break;
                        }
                        lst.Add(int.Parse(itemSrc[0]));
                    }
                    return lst;
                }
                else
                {
                    return null;
                }
            }
        }

    }
}
