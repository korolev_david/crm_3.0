﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    public class ProductList
    {
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "id_product")] 
        public int id { get; set; }
        [JsonProperty(PropertyName = "id")]
        private int duplicatId {set { id = value; } }
        
    }
}
