﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{


    class Course_CL
    {
        [JsonProperty(PropertyName = "obj_id")]
        public int id_course { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name_course { get; set; }
        [JsonProperty(PropertyName = "chast")]
        public List<Part> parts { get; set; }
        [JsonProperty(PropertyName = "parent_id")]
        public int form { get; set; }
        public bool IsExpanded { get; set; }
        public bool IsSelected { get; set; }

    }




    public class Part
    {
        [JsonProperty(PropertyName = "obj_id")]
        public int id_part { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name_part { get; set; }
        [JsonProperty(PropertyName = "lesson")]
        public List<Lesson> lessons { get; set; }
        public bool IsExpanded { get; set; }
        public bool IsSelected { get; set; }

    }

    public class Lesson
    {

        public int obj_id { get; set; }

        public int param_id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name_lesson { get; set; }

        public string url { get; set; }
        public bool IsExpanded { get; set; }
        public bool IsSelected { get; set; }
    }

    public class TradingOnline
    {
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "key")]
        public string key { get; set; }
        [JsonProperty(PropertyName = "time")]
        public string time { get; set; }
        [JsonProperty(PropertyName = "online")]
        public string online { get; set; }

    }
}
