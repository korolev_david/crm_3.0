﻿using crm_3._0_admin.Handler;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Class
{
    public class FillteUser
    {
       public List<UrlFilter> urls { get; set; }
       public List<CityFilter> city { get; set; }
       public List<FilterCourse> filterCourse { get; set; }
       
    
        public void SetUrls(List<Users> user )
        {
            if (user != null)
            {
                urls = user.SelectMany(x => x.urls).GroupBy(x => x).Select(st => new UrlFilter { url = st.Key, check = false }).ToList();
            }
           
        }
        public void SetCity(List<Users> user)
        {
            if (user != null)
            {
                city = user.GroupBy(x => x.city.ToString())
                            .Select(st => new CityFilter { city = st.Key, check = false }).ToList();
            }
            
        }
    }
    public class UrlFilter
    {
        public string url { get; set; }
        public bool check { get; set; }
    }
    public class CityFilter
    { 
       public string city { get; set; }
       public bool check { get; set; }
    }

    public class FilterCourse
    {
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "object_id")]
        public int id { get; set; }
        public bool check { get; set; }
    }
}
