﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageControl
{
    /// <summary>
    /// Логика взаимодействия для UrlR.xaml
    /// </summary>
    public partial class UrlR : Page
    {
        int idUser;
        List<DataUrl> urls = new List<DataUrl>();
        DataBase dataBase;
        public UrlR(int idUser, Manager manager)
        {
            InitializeComponent();
            this.idUser = idUser;
            dataBase = new DataBase(manager.id);
            Start();
        }
        private async void Start()
        {
            loadMini.Visibility = Visibility.Visible;
            await Task.Run(() => { urls = dataBase.GetDataUrls(idUser); });
            UrlData.ItemsSource = urls;
            UrlData.Items.Refresh();
            loadMini.Visibility = Visibility.Collapsed;
        }
    }
}
