﻿using crm_3._0_admin.Class;
using crm_3._0_admin.PageElement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageControl
{
    /// <summary>
    /// Логика взаимодействия для UserPage.xaml
    /// </summary>
    public partial class UserPage : Page
    {
        Users user;
        Manager manager;
        Management mainWnd;
        public UserPage(Users user, Manager manager)
        {
            InitializeComponent();
            this.user = user;
            this.manager = manager;
            ReplacingFrame.Content = new СommentR(user.id, manager.name, manager);
            BackTaskMenu.Visibility = Visibility.Hidden;

        }
        public UserPage(Users user, Manager manager, Management mainWnd)
        {
            InitializeComponent();
            this.user = user;
            this.manager = manager;
            ReplacingFrame.Content = new СommentR(user.id, manager.name, manager);
            this.mainWnd = mainWnd;

        }
        private void SelectButton()
        {
            Button[] btn = { CommentMenuBut, InfoUserMenuBut, ProductMenuBut, ScoreMenuBut, UrlMenuBut };
            foreach (Button elm in btn)
            {
                elm.Tag = 1.ToString();
            }
        }

        private void CommentMenuBut_Click(object sender, RoutedEventArgs e)
        {
           
            ReplacingFrame.Content = new СommentR(user.id, manager.name, manager);
            SelectButton();
            CommentMenuBut.Tag = 2.ToString();
        }

        private void InfoUserMenuBut_Click(object sender, RoutedEventArgs e)
        {
           
            ReplacingFrame.Content = new InfoUserR(user.id, mainWnd);
            SelectButton();
            InfoUserMenuBut.Tag = 2.ToString();
        }

        private void ProductMenuBut_Click(object sender, RoutedEventArgs e)
        {
            
            ReplacingFrame.Content = new ProductR(user.id, manager);
            SelectButton();
            ProductMenuBut.Tag = 2.ToString();
        }

        private void ScoreMenuBut_Click(object sender, RoutedEventArgs e)
        {
            ReplacingFrame.Content = new OrderR(user,manager, user.id);
            SelectButton();
            ScoreMenuBut.Tag = 2.ToString();
        }

        private void UrlMenuBut_Click(object sender, RoutedEventArgs e)
        {
            ReplacingFrame.Content = new UrlR(user.id, manager);
            SelectButton();
            UrlMenuBut.Tag = 2.ToString();
        }

        private void AddTaskBut_Click(object sender, RoutedEventArgs e)
        {
          
            Assistance assistance = new Assistance(user, manager);
            assistance.ShowDialog();
        }

        private void BackTaskMenu_Click(object sender, RoutedEventArgs e)
        {
            if (mainWnd != null)
            {

                mainWnd.TaskDataGrid_SelectionChanged(null, null);
            }
        }
    }
}
