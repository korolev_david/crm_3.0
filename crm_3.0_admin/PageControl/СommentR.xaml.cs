﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Annotations;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace crm_3._0_admin.PageControl
{
    /// <summary>
    /// Логика взаимодействия для СommentR.xaml
    /// </summary>
    public partial class СommentR : Page
    {
        //DispatcherTimer timer = new DispatcherTimer();
        //DateTime date = DateTime.Now.ToUniversalTime();
        DataBase dataBase;
        string manager_name = string.Empty;
        DataCommentPage dataComment = new DataCommentPage();
        private int idUser;
        public СommentR(int idUser, string nameM, Manager manager)
        {
            InitializeComponent();
            manager_name = nameM;
            this.idUser = idUser;
            dataBase = new DataBase(manager.id);
            Start();
            

        }
        private async void Start()
        {
            loadMini.Visibility = Visibility.Visible;
            await Task.Run(()=> { dataComment= dataBase.GetDataComment(idUser); });
            if(dataComment.messages != null)
            ObjectMessageLixtBox.ItemsSource = dataComment.messages.FindAll(x => x.attr ==128);
            if (VisualTreeHelper.GetChildrenCount(ObjectMessageLixtBox) > 0)
            {
                Decorator border = VisualTreeHelper.GetChild(ObjectMessageLixtBox as ListBox, 0) as Decorator;
                ScrollViewer scrollViewer = border.Child as ScrollViewer;
                scrollViewer.ScrollToEnd();
            }

            //nameUser_txt.Text = dataComment.name;
            //SetUtc();
            loadMini.Visibility = Visibility.Collapsed;

        }

        //private void TimerTick(object sender, EventArgs e)
        //{
        //    date = date.AddSeconds(1);
        //    TimeZ.Text = String.Format("Время клиента: {0}", date.ToString("HH:mm:ss")) ;
        //}

        //private void SetUtc()
        //{
        //    ItemCollection collection = utc.Items;
           
        //    foreach(ComboBoxItem item in collection)
        //    {
        //        if (item.Content.ToString() == dataComment.utc)
        //        {
                   
        //            item.IsSelected = true;
        //            timer.Start();
        //            break;
        //        }
        //    }
        //}
       
        //private void utc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var items = e.AddedItems;
        //    if(items.Count != 0)
        //    {
        //        ComboBoxItem utcSel = (ComboBoxItem)items[0];
        //        string utcStr = utcSel.Content.ToString();
        //        utcStr = utcStr.Replace("UTC", "");
        //        date = DateTime.Now.ToUniversalTime();
        //        date = date.AddHours(Convert.ToDouble(utcStr));
        //        TimeZ.Text = String.Format("Время клиента: {0}", date.ToString("HH:mm:ss"));
        //        timer.Start();
        //    }
        //    else
        //    {
        //        timer.Stop();
        //        TimeZ.Text = string.Empty;
        //    }
           

        //}

        private void commentChat_sel_Checked(object sender, RoutedEventArgs e)
        {
            if (dataComment.messages != null)
            {
                ObjectMessageLixtBox.ItemsSource = dataComment.messages.FindAll(x => x.attr == 128);
                ObjectMessageLixtBox.Items.Refresh();
            }
           
        }

        private void eventChat_sel_Checked(object sender, RoutedEventArgs e)
        {
            if (dataComment.messages != null)
            {
                ObjectMessageLixtBox.ItemsSource = dataComment.messages.FindAll(x => x.attr == 144);
                ObjectMessageLixtBox.Items.Refresh();
            }
            
        }

        //private async void editUTC_Click(object sender, RoutedEventArgs e)
        //{
        //    if (editUTC.Tag.ToString() == 2.ToString())
        //    {
        //        editUTC.Tag = 1.ToString();
        //        utc.IsEnabled = true;
                
        //    }
        //    else
        //    {
        //        ItemCollection collection = utc.Items;
        //        List<ComboBoxItem> boxItems =  collection.Cast<ComboBoxItem>().ToList();
        //        string UTC =  boxItems.Find(x => x.IsSelected == true).Content.ToString();
        //        bool answer = false;
        //        await Task.Run(() => { answer = dataBase.EditUTC(idUser, UTC);  });
        //        if (!answer)
        //        {
        //            MessageBox.Show("Ошибка. Данные не внесены");
        //            utc.SelectedIndex = -1;
        //        }
        //        editUTC.Tag = 2.ToString();
        //        utc.IsEnabled = false;
        //    }
        //}

        //private async void EditName_Click(object sender, RoutedEventArgs e)
        //{
        //    if (EditName.Tag.ToString() == 2.ToString())
        //    {
        //        EditName.Tag = 1.ToString();
        //        nameUser_txt.IsReadOnly = false;

        //    }
        //    else
        //    {
        //        bool answer = false;
        //        string name = nameUser_txt.Text;
        //        await Task.Run(() => { answer = dataBase.EditName(idUser, name); });
        //        if (!answer)
        //        {
        //            MessageBox.Show("Ошибка. Данные не внесены");
        //            nameUser_txt.Text = dataComment.name;
        //        }
                
        //        EditName.Tag = 2.ToString();
        //        nameUser_txt.IsReadOnly = true;
        //    }
        //}

        private async void sendMessage_Click(object sender, RoutedEventArgs e)
        {
            if (msgText.Text != string.Empty)
            {
                
                    string text = msgText.Text;
                    bool answer = false;
                    await Task.Run(() => { answer = dataBase.SendMessageUser(idUser, text, 128, manager_name); });
                    if (answer)
                    {
                        if (dataComment.messages == null) dataComment.messages = new List<Messages>();
                        dataComment.messages.Add(new Messages { attr = 128, name = manager_name, date = DateTime.Now, message = text });
                        commentChat_sel.IsChecked = true;
                        ObjectMessageLixtBox.ItemsSource = dataComment.messages.FindAll(x => x.attr == 128);
                        ObjectMessageLixtBox.Items.Refresh();
                        msgText.Text = string.Empty;
                        Decorator border = VisualTreeHelper.GetChild(ObjectMessageLixtBox as ListBox, 0) as Decorator;
                        ScrollViewer scrollViewer = border.Child as ScrollViewer;
                        
                        scrollViewer.ScrollToEnd();
                }
                    else
                    MessageBox.Show("Ошибка.Комментарий не внесен");
                
            }
        }

        private void msgText_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                sendMessage_Click(null, null);
            }
        }

        //private void Page_Unloaded(object sender, RoutedEventArgs e)
        //{
        //    timer.Stop();
        //    timer = null;
        //}
    }
}
