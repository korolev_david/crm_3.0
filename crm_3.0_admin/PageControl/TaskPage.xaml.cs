﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using crm_3._0_admin.PageElement;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageControl
{
    /// <summary>
    /// Логика взаимодействия для TaskPage.xaml
    /// </summary>
    public partial class TaskPage : Page
    {
        MTask TaskUser;
        Manager mng;
        ObservableCollection<MessageTask> tsMessage = new ObservableCollection<MessageTask>();
        DataBase dataBase;
        DataGrid dataGridTask;
        Management mainW;
        public TaskPage(MTask taskUs, DataGrid dataGridTask, Manager mng)
        {
            InitializeComponent();
            dataBase = new DataBase(mng.id);
            PaymentBut.Tag = 2.ToString();
            ScoreBut.Tag = 2.ToString();
            PresentationBut.Tag = 2.ToString();
            NeedsBut.Tag = 2.ToString();
            CallBut.Tag = 1.ToString();
            TaskUser = taskUs;
            this.mng = mng;
            this.dataGridTask = dataGridTask;
           
            SetParam();

        }
        public TaskPage(MTask taskUs, DataGrid dataGridTask, Manager mng, Management mangement)
        {
            InitializeComponent();
            PaymentBut.Tag = 2.ToString();
            ScoreBut.Tag = 2.ToString();
            PresentationBut.Tag = 2.ToString();
            NeedsBut.Tag = 2.ToString();
            CallBut.Tag = 1.ToString();
            TaskUser = taskUs;
            this.mng = mng;
            this.dataGridTask = dataGridTask;
            mainW = mangement;
            SetParam();

        }
        private async void SetParam()
        {
            DataBase dataBase = new DataBase(mng.id);
            titleBox.Text = TaskUser.title;
            productNameBox.Text = TaskUser.nameProduct;
            descriptionBox.Text = TaskUser.description;
            dateCreateBox.SelectedDate = TaskUser.date_creation;
            dateTochBox.SelectedDate = TaskUser.date_touch;
            numberTask.Text = TaskUser.idTask.ToString();
            rangStat.Text = TaskUser.rating.ToString();
            await Task.Run(() => {  tsMessage = dataBase.GetMessageTasks(TaskUser.idTask); });
            taskMessageLixtBox.ItemsSource = tsMessage;
            taskMessageLixtBox.Items.MoveCurrentToLast();
            taskMessageLixtBox.Items.Refresh();
            taskMessageLixtBox.ScrollIntoView(taskMessageLixtBox.Items.CurrentItem);
            FormationStage();

        }
     
   
        private void FormationStage()
        {
            Button[] buttons = { CallBut, NeedsBut, PresentationBut, ScoreBut, PaymentBut };
            for (int i = 0; i < TaskUser.type; i++)
            {
                buttons[i].Tag = 1;
            }
         
        }

        private void ContentInfoBut_Click(object sender, RoutedEventArgs e)
        {
            Users users = new Users();
            users.id = TaskUser.idUser;
            users.dataSrc = TaskUser.fileterData;
            
            mainW.rigth_menu.Content = new UserPage(users, mng, mainW);
        }

        private void textMessage_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                sendTaskMs_Click(null, null);

            }
        }

        private async void regeditThem_Click(object sender, RoutedEventArgs e)
        {
            if (regeditThem.Tag.ToString() == 0.ToString())
            {
                regeditThem.Tag = 1.ToString();
                titleBox.IsReadOnly = false;
            }
            else
            {
                regeditThem.Tag = 0.ToString();
                titleBox.IsReadOnly = true;
                if (TaskUser.title != titleBox.Text)
                {

                    bool checkAnswer = false;
                    string updateText = titleBox.Text;
                    await Task.Run(() => { checkAnswer = dataBase.UpdateTask(TaskUser.idTask, "title", updateText); });
                    if (!checkAnswer)
                    {
                        MessageBox.Show("Изменения не внесены");
                        titleBox.Text = TaskUser.title;
                    }
                    else
                    {
                        TaskUser.title = titleBox.Text;
                        dataGridTask.Items.Refresh();
                    }
                }
            }
        }

        private async void dateTochBox_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dateTochBox.SelectedDate != TaskUser.date_touch && TaskUser.state != 1 && TaskUser.state != 2)
            {
                DataBase dataBase = new DataBase(mng.id);
                bool checkAnswer = false;
                DateTime updateDate = (DateTime)dateTochBox.SelectedDate;
                await Task.Run(() => { checkAnswer = dataBase.UpdateStageTask(TaskUser.idStage, "date_touch", updateDate.ToString("yyyy-MM-dd")); });
                if (!checkAnswer)
                {
                    MessageBox.Show("Изменения не внесены");
                    dateTochBox.SelectedDate = TaskUser.date_touch;
                }
                else
                {
                    TaskUser.date_touch = (DateTime)dateTochBox.SelectedDate;
                    dataGridTask.Items.Refresh();
                }
            }
            else
            {
                dateTochBox.SelectedDate = TaskUser.date_touch;
            }
        }

        private async void sendTaskMs_Click(object sender, RoutedEventArgs e)
        {
            DataBase dataBase = new DataBase(mng.id);
            string text = textMessage.Text;
            int idMs = -1;
            await Task.Run(() => { idMs = dataBase.SetMessageTask(TaskUser.idTask, text, mng.name); });
            if (idMs != -1)
            {
                tsMessage.Add(new MessageTask { id_ms = idMs, date = DateTime.Now, message = text, name = mng.name });
                taskMessageLixtBox.Items.MoveCurrentToLast();
                taskMessageLixtBox.Items.Refresh();
                taskMessageLixtBox.ScrollIntoView(taskMessageLixtBox.Items.CurrentItem);
                textMessage.Text = string.Empty;
            }
        }
    }
}
