﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using crm_3._0_admin.Hendler;
using crm_3._0_admin.PageElement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageControl
{
    /// <summary>
    /// Логика взаимодействия для ScoreR.xaml
    /// </summary>
    public partial class OrderR : Page
    {
        Users user;
        int idUser;
        DataBase dataBase;
        List<Orders> orders = new List<Orders>();
        Manager manager;

        public OrderR(Users user, Manager manager, int idUser)
        {
            InitializeComponent();
            this.user = user;
            this.idUser = idUser;
            this.manager = manager;
            dataBase = new DataBase(manager.id);
            Start();
        }

        private async void Start()
        {
            loadMini.Visibility = Visibility.Visible;
            await Task.Run(() => { orders = dataBase.GetOrders(idUser); });
            OrdersDataGrid.ItemsSource = orders;
            //string[] data = Regex.Split(user.fileterData, "<data>");
            //string[] email = Array.FindAll(data, x => x.Contains("@"));

            //if (Array.FindAll(email, x => x.Contains(";")).Length > 0)
            //{
            //    string dopEmail = email[Array.FindIndex(email, x => x.Contains(";"))];
            //    email = Array.FindAll(email, x => !x.Contains(";"));
            //    string[] dopEmailArr = dopEmail.Split(';');
            //    foreach (string item in dopEmailArr)
            //        if (item != string.Empty) cmb_email.Items.Add(item);


            //}
            //foreach (string item in email)
            //    if (item != string.Empty) cmb_email.Items.Add(item);

            loadMini.Visibility = Visibility.Collapsed;
        }

        //private async void addOrder_Click(object sender, RoutedEventArgs e)
        //{

        //    OrderCreate order = new OrderCreate(main.manager, main, true);
        //    order.ShowDialog();
        //    await Task.Run(() => { orders = dataBase.GetOrders(idUser); });
        //    OrdersDataGrid.ItemsSource = orders;
        //    OrdersDataGrid.Items.Refresh();

        //}

        //private void sendMs_Click(object sender, RoutedEventArgs e)
        //{
        //    if (OrdersDataGrid.SelectedIndex != -1 && cmb_email.SelectedIndex != -1)
        //    {
        //        var item =(Orders)OrdersDataGrid.SelectedItem;
        //        List<Product> products = new List<Product>();
                
        //        string [] items = item.product.Split(';');
        //        foreach (string name in items)
        //        {
        //            products.Add(new Product { name = name });
        //        }
        //        SendMessage sendMessage = new SendMessage(item.idOrder, products, main, cmb_email.Items[cmb_email.SelectedIndex].ToString());
        //        sendMessage.SendMsOrders();
        //        sendMsPanel.Visibility = Visibility.Collapsed;
        //        cmb_email.SelectedIndex = -1;
        //        MessageBox.Show("Сообщение отправленно");


        //    }
            
        //}

        //private void cancelMs_Click(object sender, RoutedEventArgs e)
        //{
        //    sendMsPanel.Visibility = Visibility.Collapsed;
        //}

        //private void sendMsBut_Click(object sender, RoutedEventArgs e)
        //{
        //    if (OrdersDataGrid.SelectedIndex != -1)
        //    {
        //        sendMsPanel.Visibility = Visibility.Visible;
        //    }
        //}

       
    }
}
