﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using crm_3._0_admin.PageElement;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindowAdmin : Window
    {
        public MainWindowAdmin()
        {
            InitializeComponent();
        }
        public Manager mng;
      

        private void OrderDownload_Click(object sender, RoutedEventArgs e)
        {
            AccountantFrame.Content = null;
            AccountantFrame.Content = new OrderWork(mng);
        }

        private void PaymentManagement_Click(object sender, RoutedEventArgs e)
        {
            AccountantFrame.Content = null;
            AccountantFrame.Content = new UnloadingInvoice(mng);
        }

        private void AddProductBut_Click(object sender, RoutedEventArgs e)
        {
            MarketinFrame.Content = null;
            MarketinFrame.Content = new CreateProduct(mng);

        }

        private void EditProductBut_Click(object sender, RoutedEventArgs e)
        {
            MarketinFrame.Content = null;
            MarketinFrame.Content = new EditProduct(mng);
        }

        private void ListProductBut_Click(object sender, RoutedEventArgs e)
        {
            MarketinFrame.Content = null;
            MarketinFrame.Content = new ListProduct(mng);
        }

        private void VHProduct_Click(object sender, RoutedEventArgs e)
        {
            MarketinFrame.Content = null;
            MarketinFrame.Content = new VHProducts(mng);
        }


        private void AccountantBut_Click(object sender, RoutedEventArgs e)
        {
            panelMenu.Visibility = Visibility.Collapsed;
            Marketin.Visibility = Visibility.Collapsed;
            Accountant.Visibility = Visibility.Visible;
        }
        private void SettingPayment_Click(object sender, RoutedEventArgs e)
        {
            MarketinFrame.Content = null;
            MarketinFrame.Content = new PaymentProfile(mng);
        }
        private void MarketingBut_Click(object sender, RoutedEventArgs e)
        {
            panelMenu.Visibility = Visibility.Collapsed;
            Marketin.Visibility = Visibility.Visible;
            Accountant.Visibility = Visibility.Collapsed;
        }

        private void ManagerBut_Click(object sender, RoutedEventArgs e)
        {
            //panelMenu.Visibility = Visibility.Collapsed;
            ManagmentGrid.Visibility = Visibility.Visible;
            ManagmentFrame.Content = null;
            ManagmentFrame.Content = new Management(mng);

        }

        private async void btnInLogin_Click(object sender, RoutedEventArgs e)
        {
            if (password.Password != string.Empty && login.Text != string.Empty)
            {
                DataBase dataBase = new DataBase(1101);
                string login_ = login.Text;
                string pass_ = password.Password;
                mng = await Task.Run(() => dataBase.in_login(pass_, login_));
                if (mng.id == -1)
                {
                    error.Text = "Нет ответа от сервера.";
                }
                if (mng.status == "reject")
                {
                    error.Text = "Невырный пароль/логин.";
                }
                else if(mng.status == "confirm")
                {
                    
                    if (mng.acces == 1)
                    {
                        gridInLogin.Visibility = Visibility.Collapsed;
                        panelMenu.Visibility = Visibility.Visible;
                        AccountantBut.Visibility = Visibility.Visible;
                        MarketingBut.Visibility = Visibility.Visible;
                        ManagerBut.Visibility = Visibility.Visible;
                    }
                    else if (mng.acces == 2)
                    {
                        AccountantBut.Visibility = Visibility.Collapsed;
                        MarketingBut.Visibility = Visibility.Visible;
                        ManagerBut.Visibility = Visibility.Collapsed;
                    }
                    else if (mng.acces == 3)
                    {
                        AccountantBut.Visibility = Visibility.Visible;
                        MarketingBut.Visibility = Visibility.Collapsed;
                        ManagerBut.Visibility = Visibility.Collapsed;
                    }
                    else if (mng.acces == 4)
                    {
                        AccountantBut.Visibility = Visibility.Collapsed;
                        MarketingBut.Visibility = Visibility.Collapsed;
                        ManagerBut.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        MessageBox.Show("Неизвестный пользователь");
                        this.Close();
                    }

                    gridInLogin.Visibility = Visibility.Hidden;
                    panelMenu.Visibility = Visibility.Visible;

                    byte[] mybyte = System.Text.Encoding.UTF8.GetBytes(login.Text + ":" + password.Password);
                    string returntext = System.Convert.ToBase64String(mybyte);

                    FileStream fstream = new FileStream(@"info.inf", FileMode.Create, FileAccess.ReadWrite);
                    byte[] array = System.Text.Encoding.Default.GetBytes(returntext);
                    fstream.Write(array, 0, array.Length);
                    fstream.Close();
                  
                }
            }
        }
        private async void Window_ContentRendered(object sender, EventArgs e)
        {
            DataBase dataBase = new DataBase(11012);
            double version = 4.1;
            double check_vrs = await Task.Run(() => dataBase.get_version());
            if (check_vrs > version)
            {
                Process.Start(Assembly.GetExecutingAssembly().Location.Replace("SwiftCrmAdmin.exe", "Update-2.0.exe"));
                this.Close();
            }


            loadText.Visibility = Visibility.Visible;
            gridInLogin.Visibility = Visibility.Hidden;
            string getFile = string.Empty;
            FileStream fstream = new FileStream(@"info.inf", FileMode.OpenOrCreate, FileAccess.ReadWrite);

            byte[] array = new byte[fstream.Length];
            fstream.Read(array, 0, array.Length);
            getFile = System.Text.Encoding.Default.GetString(array);
            fstream.Close();
            if (getFile.Length > 0)
            {
              
                byte[] mybyte = System.Convert.FromBase64String(getFile);
                string log_pass = System.Text.Encoding.UTF8.GetString(mybyte);

                String[] substrings = log_pass.Split(':');

                mng = await Task.Run(() => dataBase.in_login(substrings[1], substrings[0]));
                if (mng.id == -1)
                {
                    error.Text = "Нет ответа от сервера.";
                    loadText.Visibility = Visibility.Hidden;
                    gridInLogin.Visibility = Visibility.Visible;
                }
                if (mng.status == "reject")
                {
                    error.Text = "Невырный пароль/логин.";
                    gridInLogin.Visibility = Visibility.Visible;
                }
                else if (mng.status == "confirm")
                {
                    if (mng.acces == 1)
                    {
                        gridInLogin.Visibility = Visibility.Hidden;
                        panelMenu.Visibility = Visibility.Visible;
                    }
                    else if (mng.acces == 2)
                    {
                        AccountantBut.Visibility = Visibility.Collapsed;
                        MarketingBut.Visibility = Visibility.Visible;
                        ManagerBut.Visibility = Visibility.Collapsed;
                    }
                    else if (mng.acces == 3)
                    {
                        AccountantBut.Visibility = Visibility.Visible;
                        MarketingBut.Visibility = Visibility.Collapsed;
                        ManagerBut.Visibility = Visibility.Collapsed;
                    }
                    else if (mng.acces == 4)
                    {
                        AccountantBut.Visibility = Visibility.Collapsed;
                        MarketingBut.Visibility = Visibility.Collapsed;
                        ManagerBut.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        MessageBox.Show("Неизвестный пользователь");
                        this.Close();
                    }
                    gridInLogin.Visibility = Visibility.Hidden;
                    panelMenu.Visibility = Visibility.Visible;
                }
            }
            else
            {
                gridInLogin.Visibility = Visibility.Visible;
            }
            loadText.Visibility = Visibility.Hidden;
        }

        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            byte[] mybyte = System.Text.Encoding.UTF8.GetBytes("");
            string returntext = System.Convert.ToBase64String(mybyte);

            FileStream fstream = new FileStream(@"info.inf", FileMode.Create, FileAccess.ReadWrite);
            byte[] array = System.Text.Encoding.Default.GetBytes(returntext);
            fstream.Write(array, 0, array.Length);
            fstream.Close();
            mng = null;
            gridInLogin.Visibility = Visibility.Visible;
            panelMenu.Visibility = Visibility.Hidden;
        }

        private void StatisticsPage_Click(object sender, RoutedEventArgs e)
        {
            MarketinFrame.Content = null;
            MarketinFrame.Content = new Statistics(mng);
        }

        private void StatisticsLeadPage_Click(object sender, RoutedEventArgs e)
        {
            MarketinFrame.Content = null;
            MarketinFrame.Content = new StatisticsLead(mng);
        }

        private void StatisticsProductPage_Click(object sender, RoutedEventArgs e)
        {
            MarketinFrame.Content = null;
            MarketinFrame.Content = new StatisticsProduct(mng);
            //MarketinFrame.Content = new СostAnalysis();
        }

        private void RequestForExpenses_Click(object sender, RoutedEventArgs e)
        {
            RequestForExpensesFrame.Content = null;
            RequestForExpensesFrame.Content = new PaymentFrom(mng);
            RequestForExpensesFrame.Visibility = Visibility;
            RequestForExpensesGrid.Visibility = Visibility.Visible;
        }

        private void backMenuMarketing_Click(object sender, RoutedEventArgs e)
        {
            panelMenu.Visibility = Visibility.Visible;
            Marketin.Visibility = Visibility.Collapsed;
            Accountant.Visibility = Visibility.Collapsed;
        }

        private void CostAnalysisBut_Click(object sender, RoutedEventArgs e)
        {
            AccountantFrame.Content = null;
            AccountantFrame.Content = new СostAnalysis(mng);
        }

        private void backMenuAccountant_Click(object sender, RoutedEventArgs e)
        {
            panelMenu.Visibility = Visibility.Visible;
            Marketin.Visibility = Visibility.Collapsed;
            Accountant.Visibility = Visibility.Collapsed;
        }

       
    }
}
