﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0_admin
{
    /// <summary>
    /// Логика взаимодействия для ReCreate.xaml
    /// </summary>
    public partial class ReCreate : Window
    {
        List<MTask> mTask;
        Manager manager;
        DataBase dataBase;
        public ReCreate(List<MTask> mTask, Manager manager)
        {
            InitializeComponent();
            this.mTask = mTask;
            this.manager = manager;
            dataBase = new DataBase(manager.id);
            countTask.Text = mTask.Count.ToString();
        }

        private async void createTask_Click(object sender, RoutedEventArgs e)
        {
            
            if (dateTouch.SelectedDate != null)
            {
                int doneT = 0;
                int falseT = 0;
                DateTime date = (DateTime)dateTouch.SelectedDate;
                stage1.Visibility = Visibility.Collapsed;
                stage2.Visibility = Visibility.Visible;
                foreach (var item in mTask)
                {
                   bool check = false;
                   await Task.Run(()=> {check = dataBase.CreatTask(item.idUser, date, item.idProduct, item.type, item.managerId, item.title, item.description); });
                   if(check)
                   {
                        createDone.Text = string.Format("Создано: {0}", ++doneT);
                   }
                   else
                   {
                        createFalse.Text = string.Format("Не создано: {0}", ++falseT);
                   }
                   
                }
            }
            else
            {
                MessageBox.Show("Выберите дату");
            }
           

        }
    }
}
