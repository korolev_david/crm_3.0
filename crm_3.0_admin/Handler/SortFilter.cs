﻿using crm_3._0_admin.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Handler
{
    class SortFilter
    {

        public List<MTask> GetSortTask(string product, string them, int numberTask, int priority, int stage, 
            List<MTask> mTasks, string global, DateTime dateTouchFrom_, DateTime dateTouchBefore_,DateTime dateCreatFrom_, DateTime dateCreateBefore_,int state, string manager)
        {
            if (global != string.Empty)
            {
                mTasks = mTasks.FindAll(x => x.nameUser.ToLower().Contains(global.ToLower()) || x.fileterData.ToLower().Contains(global.ToLower()));
            }
            if (manager != string.Empty)
            {
                if (manager == "Нет менеджера")
                {
                    mTasks = mTasks.FindAll(x => x.manager == null);
                }
                else
                {
                    mTasks = mTasks.FindAll(x => x.manager == manager);
                }
               
            }
            if (product != string.Empty)
            {
                mTasks = mTasks.FindAll(x => x.nameProduct == product);
            }
            if (them != string.Empty)
            {
                mTasks = mTasks.FindAll(x => x.title.ToLower().Contains(them.ToLower()));
            }
            if (numberTask != -1)
            {
                mTasks = mTasks.FindAll(x => x.idTask == numberTask);
            }
            if (priority != -1)
            {
                mTasks = mTasks.FindAll(x => x.rating == priority);
            }
            if (stage != -1)
            {
                mTasks = mTasks.FindAll(x => x.type == stage);
            }
            if (dateTouchFrom_ != Convert.ToDateTime("01.01.0001 0:00:00"))
            {
                mTasks = mTasks.FindAll(x => x.date_touch >= dateTouchFrom_.Date);
            }
            if (dateTouchBefore_ != Convert.ToDateTime("01.01.0001 0:00:00"))
            {
                mTasks = mTasks.FindAll(x => x.date_touch <= dateTouchBefore_.Date);
            }
            if (dateCreatFrom_ != Convert.ToDateTime("01.01.0001 0:00:00"))
            {
                mTasks = mTasks.FindAll(x => x.tCreation >= dateCreatFrom_.Date);
            }
            if (dateCreateBefore_ != Convert.ToDateTime("01.01.0001 0:00:00"))
            {
                mTasks = mTasks.FindAll(x => x.tCreation <= dateCreateBefore_.Date);
            }
            if (state != -1)
            {
                mTasks = mTasks.FindAll(x => x.state == state);
            }
            return mTasks;
        }
    }
}
