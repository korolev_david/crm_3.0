﻿using crm_3._0_admin.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using xNet;

namespace crm_3._0_admin.Hendler
{
    class SendMessage
    {
        int idOrder { get; set; }
        List<Product> products { get; set; }
        MTask mTask { get; set; }
        string email { get; set; }
        string formingProduct { get; set; }
        public SendMessage(int idOrder, List<Product>products, MTask mTask, string email)
        {
            this.idOrder = idOrder;
            this.products = products;
            this.mTask = mTask;
            this.email = email;
            formingStringProduct();

        }
        private void formingStringProduct()
        {
            foreach (var item in products)
                formingProduct += item.name + ",";

            formingProduct = formingProduct.Remove(formingProduct.Length - 1);
        }
        public bool SendMsOrders()
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            HashKey hashKey = new HashKey();
            string hashLink = hashKey.encode_(idOrder.ToString());
            try
            {
                reqParams["nameus"] = mTask.nameUser;
                reqParams["product"] = formingProduct;
                reqParams["link"] = hashLink;
                reqParams["email"] = email;
                ;
                request.Post("https://purnov.com/purnov/crm/massage/sendOrdersMs_auto.php", reqParams);
            }
            catch
            {
                MessageBox.Show("Ошибка отправки сообщения");
            }
          

            return true;
        }

    }
}
