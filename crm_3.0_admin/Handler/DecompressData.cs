﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm_3._0_admin.Handler
{
    class DecompressData
    {
        public string StartDecompress(string data)
        {
            byte[] mybyte = System.Convert.FromBase64String(data);
            byte[] byteDecompres = Decompress(mybyte);
            string json = System.Text.Encoding.UTF8.GetString(byteDecompres);

            return json;
        }
        private byte[] Decompress(byte[] img)
        {
            var to = new MemoryStream();
            var from = new MemoryStream(img);
            var compress = new GZipStream(from, CompressionMode.Decompress);
            compress.CopyTo(to);
            from.Close();
            return to.ToArray();
        }
    }
}
