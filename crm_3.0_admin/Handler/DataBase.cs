﻿using crm_3._0_admin.Class;
using crm_3._0_admin.PageElement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using xNet;

namespace crm_3._0_admin.Handler
{
    class DataBase
    {
        public int unId;
        public DataBase(int unId)
        {
            this.unId = unId;
        }
        private Coder getCoder = new Coder();
        public List<InvoiceExcel> GetInvoiceExcels()
        {

            List<InvoiceExcel> invoiceExcels = new List<InvoiceExcel>();
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getOrdersExel"));
            try
            {
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/getOrdersExel.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                invoiceExcels = JsonConvert.DeserializeObject<List<InvoiceExcel>>(str);
            }
            catch
            {
                return null;
            }
            return invoiceExcels;
        }
        public bool inBlockUser(int idUser)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:blockUser"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/blockUser.php", reqParams).ToString();
                return Convert.ToBoolean(str);
                
            }
            catch
            {
                return false;
            }

        }

       
        
        public List<TaskList> LstTask(DateTime dt_s, DateTime dt_e, int id_kind)
        {
            List<TaskList> lst_task = new List<TaskList>();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            try
            {
                reqParams["idUrl"] = id_kind;
                reqParams["dt_s"] = dt_s.ToString("yyyy-MM-dd");
                reqParams["dt_e"] = dt_e.ToString("yyyy-MM-dd");
                request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getDataPlannDescktop"));
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/getDataPlannDescktop.php", reqParams).ToString();
                if (str != "null")
                {
                    lst_task = JsonConvert.DeserializeObject<List<TaskList>>(str);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

            return lst_task;
        }
        public List<FilterCourse> GetFilterCourse()
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getCurse"));
            try
            {
           
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/getCurse.php", reqParams).ToString();
                return JsonConvert.DeserializeObject<List<FilterCourse>>(str);
            }
            catch
            {
                return null;
            }
        }
        public List<Users> getUsers()
        {
           
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getUser"));
            try
            {
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/getUser.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                return  JsonConvert.DeserializeObject<List<Users>>(str);
            }
            catch
            {
                return null;
            }
        }
        public List<PaymentOrder> getPayment(int idOrder)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            List<PaymentOrder> payment = new List<PaymentOrder>();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getPayment"));
            try
            {
                reqParams["order_id"] = idOrder;
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/getPayment.php", reqParams).ToString();
                if (str != "null")
                {
                    payment = JsonConvert.DeserializeObject<List<PaymentOrder>>(str);

                }
            }
            catch
            {
                return null;
            }


            return payment;
        }
        public List<ProductList> GetCourseMains()
        {
            List<ProductList> lst = new List<ProductList>();
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getCourseMain"));
            try
            {
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/getCourseMain.php").ToString();
                if (str != "null")
                {
                    lst = JsonConvert.DeserializeObject<List<ProductList>>(str);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

            return lst;
        }
        public List<IDProduct> GetProductsId()
        {
            List<IDProduct> lst_product = new List<IDProduct>();
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getProductID"));
            try
            {

                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/getProductID.php").ToString();
                if (str != "null")
                {
                    lst_product = JsonConvert.DeserializeObject<List<IDProduct>>(str);
                }
            }
            catch
            {
              
                return null;
            }

            return lst_product;
        }
        public void setPlaning(int id_user, string type_task, int course_id, int count_day, string task_desc, string manager, string crm_task, string comment, string problem, string url_sc, string email)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:setPlanning"));
            try
            {
                reqParams["idUrl"] = 1;
                reqParams["comment"] = comment;
                reqParams["id_user"] = id_user;
                reqParams["type_task"] = type_task;
                reqParams["curse_id"] = course_id;
                reqParams["count_day"] = count_day;
                reqParams["task_desc"] = task_desc;
                reqParams["manager"] = manager;
                reqParams["crm_task"] = crm_task;
                reqParams["problem"] = problem;
                reqParams["url_sc"] = url_sc;
                request.Post("https://siwitpro.com/purnov/admin_lc/setPlanning.php", reqParams).ToString();
                if (problem == "novid_lk" || problem == "video_lk" || problem == "avtorizacyion_lk" || problem == "instal_lk")
                {
                    request = new HttpRequest();
                    reqParams["email"] = email;
                    reqParams["option"] = "problem";
                    request.Post("https://purnov.com/purnov/crm/massage/sendMsTask.php", reqParams);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        public List<AddRefCurs> getRelProduct(int id)
        {
            List<AddRefCurs> courses = new List<AddRefCurs>();
            var reqParams = new RequestParams();
            var request = new HttpRequest();
            reqParams["id"] = id;
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:get_rel_product"));
            try
            {
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/get_rel_product.php", reqParams).ToString();
                if (str != "null")
                {
                    courses = JsonConvert.DeserializeObject<List<AddRefCurs>>(str);

                }
            }
            catch
            {
                return null;
            }
            return courses;
        }
        public void addRelCurse(int id_p, int id_obj)
        {
            var reqParams = new RequestParams();
            var request = new HttpRequest();
            reqParams["id_p"] = id_p;
            reqParams["id_obj"] = id_obj;
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:addRelProduct"));
            try
            {
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/addRelProduct.php", reqParams).ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка addRelCurse " + ex.Message);
            }

        }
        public void reg_product(string name, int cost, string disc, int id_product, string date)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:reg_product"));
            try
            {
                reqParams["product_id"] = id_product;
                reqParams["name"] = name;
                reqParams["cost"] = cost;
                reqParams["disc"] = disc;
                reqParams["date"] = date;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/reg_product.php", reqParams);
            }
            catch
            {
                MessageBox.Show("Ошибка рег");
            }
        }
        public List<CreatedProductClass> GetProducts()
        {
            List<CreatedProductClass> products = new List<CreatedProductClass>();
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getProduct"));
            try
            {
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/getProduct.php").ToString();
                if (str != "null")
                {
                    products = JsonConvert.DeserializeObject<List<CreatedProductClass>>(str);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка Базы Данных "+ex.Message);
            }
            return products;
        }
        public List<IDProduct> GetVHProduct()
        {
            List<IDProduct> products = new List<IDProduct>();
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:VHProductHendler"));
            try
            {
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/VHProductHendler.php").ToString();
                str = decompressData.StartDecompress(str);
                if (str != "null")
                {
                    products = JsonConvert.DeserializeObject<List<IDProduct>>(str);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка Базы Данных " + ex.Message);
            }
            return products;
        }
        public bool SetProductVH(int vh, int idProduct)
        {
            var reqParams = new RequestParams();
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:VHProductHendler"));
            try
            {
                if(vh == 0)
                {
                    vh = -1;
                }
                reqParams["hiden"] = vh;
                reqParams["idProduct"] = idProduct;
                string str =  request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/VHProductHendler.php", reqParams).ToString();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }
        public void dellRelCurs(int id_p, int id_obj)
        {
            var reqParams = new RequestParams();
            var request = new HttpRequest();
            reqParams["id_p"] = id_p;
            reqParams["id_obj"] = id_obj;
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:dell_rel"));
            try
            {
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/dell_rel.php", reqParams).ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка addRelCurse " + ex.Message);
            }

        }
        public List<Course_CL> GetСourses()
        {
            List<Course_CL> courses = new List<Course_CL>();
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:get_curse_all"));
            try
            {
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/get_curse_all.php").ToString();
                if (str != "null")
                {
                    courses = JsonConvert.DeserializeObject<List<Course_CL>>(str);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка интеренета " + ex.Message);
            }
            return courses;
        }
        public void add_product(string name, string coast, string disc, string id_rel, string day)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:add_prduct"));
            try
            {
                reqParams["name"] = name;
                reqParams["cost"] = coast;
                reqParams["disc"] = disc;
                reqParams["rel"] = id_rel;
                reqParams["day"] = day;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/add_prduct.php", reqParams);


            }

            catch 
            {
                MessageBox.Show("Ошибка Базы Данных");
             
            }


        }
        public bool dell_product(int id)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:dell_product"));
            bool check = true;
            try
            {
                reqParams["id_prd"] = id;
                string str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/dell_product.php", reqParams).ToString();
                if (str != string.Empty)
                {
                    check = Convert.ToBoolean(str);
                }

            }

            catch 
            {
                MessageBox.Show("Ошибка Базы Данных");

            }


            return check;
        }
        public void insertPayment(int id_order, double sum)
        {

            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:setPaymentsAdmin"));
            reqParams["id_order"] = id_order;
            reqParams["sum"] = sum;
            try
            {
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/setPaymentsAdmin.php", reqParams).ToString();
            }
            catch 
            {
                MessageBox.Show("Ошибка Базы Данных");
               
            }

        }
        public Orders getOrder(int idOrder)
        {
            Orders order = new Orders();
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getOrder"));
            try
            {
                reqParams["idOrder"] = idOrder;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getOrder.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                order = JsonConvert.DeserializeObject<Orders>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show("SetOrders: GetProduct... " + ex.Message);
                return null;
            }

            return order;

        }
        public List<Manager> GetManagers()
        {
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getAllManager"));
            try
            {
               
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/getAllManager.php").ToString();
                
                return  JsonConvert.DeserializeObject<List<Manager>>(str);
            }
            catch
            {
                return new List<Manager>();
            }
        }
        public List<ProductList> GetProductLists()
        {
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getProductID"));
            try
            {
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/getProductID.php").ToString();

                return JsonConvert.DeserializeObject<List<ProductList>>(str);
            }
            catch
            {
                return null;
            }
        }
        public bool CreatTask(int idUser, DateTime dateTouch, int productId, int stageId, int managerId, string them, string desc)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            reqParams["idUser"] = idUser;
            reqParams["productID"] = productId;
            reqParams["stageID"] = stageId;
            reqParams["managerID"] = managerId;
            reqParams["them"] = them;
            reqParams["dateTouch"] = dateTouch.ToString("yyyy-MM-dd");
            reqParams["desc"] = desc;

            try
            {
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/crateTask.php", reqParams);
                return true;
            }
            catch
            {
                //MessageBox.Show(ex.Message);
                return false;
            }


        }
        public List<MTask> GetTaskUsers()
        {
            var request = new HttpRequest();
            DecompressData decompressData = new DecompressData();
            string header = getCoder.getToken(this.unId, "admCRM:getTask");
            request.AddHeader("token", header);
            try
            {
             
              string str= request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/getTask.php").ToString();
              str = decompressData.StartDecompress(str);
              return  JsonConvert.DeserializeObject<List<MTask>>(str);
            }
            catch
            {
                return new List<MTask>();
            }

        }
        public bool AssingTask(string idList, int idManager)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:AssingTask"));
            try
            {
                reqParams["assingId"] = idList;
                reqParams["managerId"] = idManager;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/AssingTask.php", reqParams).ToString();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public UserInfo GetUserInfo(int idUser)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            UserInfo userInfo = new UserInfo();
            DecompressData decompressData = new DecompressData();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:clientInfo"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/clientInfo.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                userInfo = JsonConvert.DeserializeObject<UserInfo>(str);
            }
            catch
            {
                return null;
            }
            return userInfo;
        }
        public bool UpdateDataUser(int idUser, int attr, string value)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:clientInfo"));
            try
            {
                reqParams["idUser"] = idUser;
                reqParams["attr"] = attr;
                reqParams["value"] = value;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3/clientInfo.php", reqParams);
            }
            catch
            {
                return false;
            }

            return true;
        }
        public StatisticsData GetStatistics(DateTime dtFrom, DateTime dtBefore)
        {
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:Statistics"));
            try
            {
                reqParams["dateForm"] = dtFrom.ToString("yyyy-MM-dd"); 
                reqParams["dateBefore"] = dtBefore.ToString("yyyy-MM-dd"); 
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/Statistics.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                return JsonConvert.DeserializeObject<StatisticsData>(str);
            }
            catch(Exception ex)
            {
               MessageBox.Show(ex.Message);
                return null;
            }
        }
        public List<StatisticsRangProductStage> GetStatisticsRangProductStages(DateTime dtFrom, DateTime dtBefore)
        {
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:statisticProduct"));
            try
            {
                reqParams["dateFrom"] = dtFrom.ToString("yyyy-MM-dd");
                reqParams["dateBefore"] = dtBefore.ToString("yyyy-MM-dd");
                reqParams["change"] = 1.ToString();
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/statisticProduct.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                return JsonConvert.DeserializeObject<List<StatisticsRangProductStage>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

        }
        public List<StatisticsRangCloseProduct> GetStatisticsRangCloses(DateTime dtFrom, DateTime dtBefore)
        {
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:statisticProduct"));
            try
            {
                reqParams["dateFrom"] = dtFrom.ToString("yyyy-MM-dd");
                reqParams["dateBefore"] = dtBefore.ToString("yyyy-MM-dd");
                reqParams["change"] = 2.ToString();
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/statisticProduct.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                return JsonConvert.DeserializeObject<List<StatisticsRangCloseProduct>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

        }
        public List<StatisticsLeadClass> GetStatisticsLead(DateTime dtFrom, DateTime dtBefore)
        {

            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:statisticLead"));
            try
            {
                reqParams["dateForm"] = dtFrom.ToString("yyyy-MM-dd");
                reqParams["dateBefore"] = dtBefore.ToString("yyyy-MM-dd");
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/statisticLead.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                return JsonConvert.DeserializeObject<List<StatisticsLeadClass>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public List<Orders> GetOrders(int idUser)
        {
            List<Orders> order = new List<Orders>();
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getOrders"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getOrders.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                order = JsonConvert.DeserializeObject<List<Orders>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show("SetOrders: GetOrders... " + ex.Message);
                return null;
            }
            return order;
        }
        public List<DataCourse> GetDataCourses(int idUser)
        {
            List<DataCourse> dataCourses = new List<DataCourse>();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            DecompressData decompressData = new DecompressData();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:courseInfo"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/courseInfo.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                dataCourses = JsonConvert.DeserializeObject<List<DataCourse>>(str);
            }
            catch
            {
                MessageBox.Show("Ошибка GetDataCourses");
                return null;
            }

            return dataCourses;
        }
        public bool UpdateStageTask(int idStage, string param, string updateData)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getTask"));
            try
            {
                reqParams["param"] = param;
                reqParams["idStage"] = idStage;
                reqParams["update"] = updateData;
                string answer = request.Post("https://siwitpro.com/purnov/crm_3/crm_3/getTask.php", reqParams).ToString();
                return Convert.ToBoolean(answer);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: UpdateStageTask... " + ex.Message);
            }
            return false;
        }
        public bool UpdateTask(int idTask, string param, string updateData)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getTask"));
            try
            {
                reqParams["param"] = param;
                reqParams["idTask"] = idTask;
                reqParams["update"] = updateData;
                string answer = request.Post("https://siwitpro.com/purnov/crm_3/crm_3/getTask.php", reqParams).ToString();
                return Convert.ToBoolean(answer);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: UpdateTAsk... " + ex.Message);
            }
            return false;
        }
        public int SetMessageTask(int idTask, string text, string name)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:taskMessage"));
            try
            {
                reqParams["user"] = name;
                reqParams["text"] = text;
                reqParams["id_task"] = idTask;
                string id = request.Post("https://siwitpro.com/purnov/crm_3/crm_3/taskMessage.php", reqParams).ToString();
                return Convert.ToInt32(id);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: SetMessageTask... " + ex.Message);
            }
            return -1;
        }
        public ObservableCollection<MessageTask> GetMessageTasks(int idTask)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:taskMessage"));
            ObservableCollection<MessageTask> messageTasks = new ObservableCollection<MessageTask>();
            DecompressData decompressData = new DecompressData();
            try
            {
                reqParams["id_task"] = idTask;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/taskMessage.php", reqParams).ToString();

                str = decompressData.StartDecompress(str);
                messageTasks = JsonConvert.DeserializeObject<ObservableCollection<MessageTask>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: GetMTask... " + ex.Message);
            }

            return messageTasks;
        }
        public List<DataUrl> GetDataUrls(int idUser)
        {
            List<DataUrl> dataUrl = new List<DataUrl>();
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getUrl"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/getUrl.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                dataUrl = JsonConvert.DeserializeObject<List<DataUrl>>(str);
            }
            catch
            {
                return null;
            }
            return dataUrl;

        }
        public DataCommentPage GetDataComment(int idUser)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            DecompressData decompressData = new DecompressData();
            DataCommentPage dataCommentPage = new DataCommentPage();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:commentHandler"));
            try
            {
                reqParams["idUser"] = idUser;
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3/commentHandler.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                dataCommentPage = JsonConvert.DeserializeObject<DataCommentPage>(str);

            }
            catch (Exception ex)
            {
                MessageBox.Show("GetDataComment: GetProduct... " + ex.Message);
                return null;
            }

            return dataCommentPage;

        }
        public bool EditUTC(int idUser, string utc)
        {
            try
            {
                var request = new HttpRequest();
                var reqParams = new RequestParams();
                request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:commentHandler"));
                reqParams["id_user"] = idUser;
                reqParams["action"] = "utcEdit";
                reqParams["utc"] = utc;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3/commentHandler.php", reqParams);
            }
            catch
            {
                return false;
            }
            return true;

        }
        public bool EditName(int idUser, string name)
        {
            try
            {
                var request = new HttpRequest();
                var reqParams = new RequestParams();
                reqParams["id_user"] = idUser;
                reqParams["action"] = "nameEdit";
                reqParams["name"] = name;
                request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:commentHandler"));
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3/commentHandler.php", reqParams);
            }
            catch
            {
                return false;
            }
            return true;
        }
        public bool SendMessageUser(int idUser, string message, int attr, string name)
        {
            try
            {
                var request = new HttpRequest();
                var reqParams = new RequestParams();
                request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:commentHandler"));
                reqParams["id_user"] = idUser;
                reqParams["action"] = "message";
                reqParams["message"] = message;
                reqParams["attr"] = attr;
                reqParams["name"] = name;
                request.Post("https://siwitpro.com/purnov/crm_3/crm_3/commentHandler.php", reqParams);
            }
            catch
            {
                return false;
            }
            return true;
        }
        public Manager in_login(string pass, string login)
        {
            Manager lst_mng = new Manager();
            var request = new HttpRequest();
            var reqParams = new RequestParams();

            try
            {
                reqParams["login"] = login;
                reqParams["pass"] = pass;
                string str = request.Post("https://siwitpro.com/purnov/crm_3/acces/acess.php", reqParams).ToString();
                if (str != "null")
                {
                    lst_mng = JsonConvert.DeserializeObject<Manager>(str);
                }
                else                                
                {
                    lst_mng.id = -1;
                    return lst_mng;
                }
            }
            catch (Exception)
            {
                lst_mng.id = -1;
                return lst_mng;
            }

            return lst_mng;
        }

        public double get_version()
        {
            var request = new HttpRequest();
            request.CharacterSet = Encoding.GetEncoding("UTF-8");
            string str = "false";
            double vrs = 0;
            var reqParams = new RequestParams();
            try
            {
                str = request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/version/version.php", reqParams).ToString();
                vrs = Convert.ToDouble(str, CultureInfo.InvariantCulture);

            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка version: " + e.Message);
            }
            return vrs;
        }
        public List<PaymentFromData> getDataPaymentFrom()
        {
            var request = new HttpRequest();
            try
            {
                 string str = request.Post("https://purnov.com/payment-form/php/getTaskDataFromDatabace.php").ToString();
                 return JsonConvert.DeserializeObject<List<PaymentFromData>>(str);
            }
            catch (Exception)
            {
                return null;
            }

        }
        public List<PaymentFromData> getPaymentTampate()
        {
            var request = new HttpRequest();
            try
            {
                string str = request.Post("https://purnov.com/payment-form/php/getTemplates.php").ToString();
                return JsonConvert.DeserializeObject<List<PaymentFromData>>(str);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public PaymentFromData InsertPaymentFrom(string sum , string currency, string note, string customer, string templateName)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();

            reqParams["sum"] = sum;
            reqParams["currency"] = currency;
            reqParams["note"] = note;
            reqParams["customer"] = customer;
            reqParams["templateName"] = templateName;

            try
            {
                string str = request.Post("https://purnov.com/payment-form/php/insertPaymentData.php", reqParams).ToString();
                return JsonConvert.DeserializeObject<PaymentFromData>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        public void DellTamplate(int idTamplate)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();

            reqParams["id"] = idTamplate;
            reqParams["type"] = "delete-template";

            try
            {
                string str = request.Post("https://purnov.com/payment-form/php/index.php", reqParams).ToString();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
               
            }


        }

        public List<StatisticsPayment> GetStatisticsPayments(DateTime dtFrom, DateTime dtBefore)
        {
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:Statistics"));
            try
            {
                reqParams["dateForm"] = dtFrom.ToString("yyyy-MM-dd");
                reqParams["dateBefore"] = dtBefore.ToString("yyyy-MM-dd");
                reqParams["changeStat"] ="payment";
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/Statistics.php", reqParams).ToString();
                return JsonConvert.DeserializeObject<List<StatisticsPayment>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

        }

        public List<ProductProfile> GetProductProfiles()
        {
            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:getProfileProduct"));
            try
            {
                
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/getProfileProduct.php", reqParams).ToString();
                str = decompressData.StartDecompress(str);
                return JsonConvert.DeserializeObject<List<ProductProfile>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public List<PaymentProfileData> GetPaymentProfileDatas()
        {

            DecompressData decompressData = new DecompressData();
            var request = new HttpRequest();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:profilePaymentHandler"));
            try
            {
                string str = request.Get("https://siwitpro.com/purnov/crm_3/crm_3_admin/profilePaymentHandler.php").ToString();
                str = decompressData.StartDecompress(str);
                return JsonConvert.DeserializeObject<List<PaymentProfileData>>(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        public bool SetNewProfileData(int product_id, int profile_id)
        {
            var request = new HttpRequest();
            var reqParams = new RequestParams();
            request.AddHeader("token", getCoder.getToken(this.unId, "admCRM:profilePaymentHandler"));
            try
            {
                reqParams["idProduct"] = product_id;
                reqParams["idProfile"] = profile_id;

                request.Post("https://siwitpro.com/purnov/crm_3/crm_3_admin/profilePaymentHandler.php", reqParams);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

            
        }
         
    }
}
