﻿using crm_3._0_admin.Handler;
using crm_3._0_admin.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0_admin
{
    /// <summary>
    /// Логика взаимодействия для CourseTreeViewWnd.xaml
    /// </summary>
    public partial class CourseTreeViewWnd : Window
    {
        DataBase connect_;
        public CourseTreeViewWnd(Manager manager)
        {
            InitializeComponent();
            connect_ =  new DataBase(manager.id);
        }
        private void Back_fone_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private  async void Window_ContentRendered(object sender, EventArgs e)
        {
            
             
             treewee.ItemsSource = await Task.Run(() => connect_.GetСourses());
             load.Visibility = Visibility.Hidden;
        }

        private void close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
