﻿
using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace crm_3._0_admin
{
    /// <summary>
    /// Логика взаимодействия для Assistance.xaml
    /// </summary>
    public partial class Assistance : Window
    {
        List<ProductList> courseMains;
        DataBase dataBase;
        Users user;
        Manager manager;
        public Assistance(Users user, Manager manager)
        {
            InitializeComponent();
            this.user = user;
            this.manager = manager;
            name_user.Text = user.name;
            cmbRelations.Items.Add("Проблемы с установкой ЛК");
            cmbRelations.Items.Add("Проблемы с входом в ЛК");
            cmbRelations.Items.Add("Проблемы с воспроизведением видео");
            cmbRelations.Items.Add("Не находит видеозаписи");
            cmbRelations.Items.Add("Другое");
            dataBase = new DataBase(manager.id);
        }
      
        //UserData UserSelect;
        //Manager manager_;
        private async void sendRelations_Click(object sender, RoutedEventArgs e)
        {
            if (cmbRelations.SelectedIndex != -1)
            {
                string RelationsTask = cmbRelations.Items[cmbRelations.SelectedIndex].ToString();
                string problem = string.Empty;
                switch (RelationsTask)
                {
                    case "Проблемы с установкой ЛК":
                        problem = "instal_lk";
                        break;
                    case "Проблемы с входом в ЛК":
                        problem = "avtorizacyion_lk";
                        break;
                    case "Проблемы с воспроизведением видео":
                        problem = "video_lk";
                        break;
                    case "Не находит видеозаписи":
                        problem = "novid_lk";
                        break;
                    case "Другое":
                        problem = "else";
                        break;
                }
                string comment = commentRelations.Text;

                main_grid.IsEnabled = false;
                string emailUser = user.email;
               


                await Task.Run(() => dataBase.setPlaning(user.id, "contact_student", 0, 0, comment, manager.name, "", "", problem, "", emailUser));
                main_grid.IsEnabled = true;
                MessageBox.Show("Задача выставленна.");
            }
        }

        private void Back_fone_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            gridRelations.Visibility = Visibility.Visible;
            gridOpening.Visibility = Visibility.Hidden;
        }

        private async void relations_rb_Click(object sender, RoutedEventArgs e)
        {
            gridRelations.Visibility = Visibility.Hidden;
            gridOpening.Visibility = Visibility.Visible;
            if (courseMains == null)
            {
                courseMains = await Task.Run(() => dataBase.GetCourseMains());
                curse_open.ItemsSource = courseMains;
            }
        }

        private async void sendOpening_Click(object sender, RoutedEventArgs e)
        {
            if (curse_open.SelectedIndex != -1 && count_day.Text != string.Empty && url_screen.Text != string.Empty)
            {
                string count_day_tag = count_day.Text;
                ProductList course = (ProductList)curse_open.SelectedItem;
                string screen = set_id.Text + url_screen.Text;

                string emailUser = user.email;
              
                await Task.Run(() => dataBase.setPlaning(user.id, "open_records", course.id, Convert.ToInt32(count_day_tag), "Открыть запись", manager.name, "", "", "", screen, emailUser));
               
                MessageBox.Show("Задача выставленна.");
            }
            else
            {
                
               MessageBox.Show("заполните все поля.");
            }
        }

        private void search_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (search.Text != string.Empty)
            {
                curse_open.ItemsSource = courseMains.FindAll(x => x.name.Contains(search.Text));
            }
            else
            {
                curse_open.ItemsSource = courseMains;
            }
        }

        private void close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void count_day_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789 ,".IndexOf(e.Text) < 0;
        }
        public void setOrder(int idOrder)
        {
            set_id.Visibility = Visibility.Visible;
            relations_rb_Click(null, null);
            set_id.Text = String.Format("В соответствии счета {0}. ", idOrder.ToString());
            relations_rb.IsChecked = true;
        }
    }
}
