﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using crm_3._0_admin.PageControl;
using ExcelLibrary.Office.Excel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для Management.xaml
    /// </summary>
    public partial class Management : Page
    {
        public Management(Manager mng)
        {
            InitializeComponent();
            dataBase = new DataBase(mng.id);
            openTask_Click(null, null);
            manager = mng;
           

        }
        public List<Users> users = new List<Users>();
        DataBase dataBase;
        List<MTask> taskUsers = new List<MTask>();
        FillteUser filterUser;
        public Manager manager = new Manager();
        private SortFilter sortFilter = new SortFilter();
        public List<MTask> productSearchList = new List<MTask>();
        private List<Manager> mng = new List<Manager>();

        private void cmbSearchCourse_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (cmbSearchCourse.Items.Count != 0)
            {
                cmbSearchCourse.ItemsSource = filterUser.filterCourse.FindAll(x => x.name.ToLower().Trim().Contains(cmbSearchCourse.Text.ToLower().Trim()));
            }
        }

        private void cmbSearchCourse_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbSearchCourse.SelectedIndex = -1;
        }

        private void cmbSearchUrl_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (cmbSearchUrl.Items.Count != 0)
            {
                cmbSearchUrl.ItemsSource = filterUser.urls.FindAll(x => x.url.ToLower().Trim().Contains(cmbSearchUrl.Text.ToLower().Trim()));
            }
        }

        private void cmbSearchUrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbSearchUrl.SelectedIndex = -1;
        }

        private void cmbSearchCity_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (cmbSearchCity.Items.Count != 0)
            {
                cmbSearchCity.ItemsSource = filterUser.city.FindAll(x => x.city.ToLower().Trim().Contains(cmbSearchCity.Text.ToLower().Trim()));
            }
        }

        private void cmbSearchCity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbSearchCity.SelectedIndex = -1;
        }

        private void chkCours_Unchecked(object sender, RoutedEventArgs e)
        {
            FilterHendler();
          

        }

        private void chkCours_Checked(object sender, RoutedEventArgs e)
        {
            FilterHendler();
         

        }
        private void chkCountry_Checked(object sender, RoutedEventArgs e)
        {
            FilterHendler();
           
        }

        private void chkCountry_Unchecked(object sender, RoutedEventArgs e)
        {
            FilterHendler();
          
        }

        private void chkUrl_Unchecked(object sender, RoutedEventArgs e)
        {
            FilterHendler();
          
        }

        private void chkUrl_Checked(object sender, RoutedEventArgs e)
        {
            FilterHendler();
         
        }
        private void dateHitFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterHendler();
        }

        private void dateHitBefore_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterHendler();
        }

        private void dateCreateUsFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterHendler();
        }

        private void dateCreateUsBefore_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterHendler();
        }
        private void FilterHendler()
        {
            if (users.Count != 0)
            {
                List<Users> searchIn = users;
                List<string> searchCrs = new List<string>();
                List<string> searchUrl = new List<string>();
                List<string> searchCity = new List<string>();
                foreach (var item in filterUser.filterCourse.FindAll(x => x.check == true))
                {
                    searchCrs.Add("|" + item.id.ToString() + "|");
                }
                foreach (var item in filterUser.city.FindAll(x => x.check == true))
                {
                    searchCity.Add(item.city);
                }
                foreach (var item in filterUser.urls.FindAll(x => x.check == true))
                {
                    searchUrl.Add(item.url);
                }
                if (searchUrl.Count != 0)
                {

                    var duobleList = searchIn;
                    for(int i = 0; i < searchUrl.Count; i++)
                    {
                        if (i == 0)
                        {
                            searchIn = searchIn.FindAll(x => x.url.Contains(searchUrl[i]));
                        }
                        else
                        {
                            searchIn.AddRange(duobleList.FindAll(x => x.url.Contains(searchUrl[i])));
                        }

                    }
                    //searchIn = searchIn.FindAll(x => searchUrl.TrueForAll(y => x.url.Contains(y))).ToList();
                }
                if (searchCity.Count != 0)
                {
                    var duobleList = searchIn;
                    for (int i = 0; i < searchCity.Count; i++)
                    {
                        if (i == 0)
                        {
                            searchIn = searchIn.FindAll(x => x.city.Contains(searchCity[i]));
                        }
                        else
                        {
                            searchIn.AddRange(duobleList.FindAll(x => x.city.Contains(searchCity[i])));
                        }

                    }
                    //searchIn = searchIn.FindAll(x => searchCity.TrueForAll(y => x.city.Contains(y))).ToList();
                }
                if (searchCrs.Count != 0)
                {
                    var duobleList = searchIn;
                    for (int i = 0; i < searchCrs.Count; i++)
                    {
                        if (i == 0)
                        {
                            searchIn = searchIn.FindAll(x => x.courseId.Contains(searchCrs[i]));
                        }
                        else
                        {
                            searchIn.AddRange(duobleList.FindAll(x => x.courseId.Contains(searchCrs[i])));
                        }

                    }
                    //searchIn = searchIn.FindAll(x => searchCrs.TrueForAll(y => x.courseId.Contains(y))).ToList();
                }
                if (dateHitFrom.SelectedDate != null)
                {
                    searchIn = searchIn.FindAll(x => x.dateInSystem.Date >= (DateTime)dateHitFrom.SelectedDate);
                }
                if (dateHitBefore.SelectedDate != null)
                {
                    searchIn = searchIn.FindAll(x => x.dateInSystem.Date <= (DateTime)dateHitBefore.SelectedDate);
                }
                if (dateCreateUsFrom.SelectedDate != null)
                {
                    searchIn = searchIn.FindAll(x => x.dateCreate.Date >= (DateTime)dateCreateUsFrom.SelectedDate);
                }
                if (dateCreateUsBefore.SelectedDate != null)
                {
                    searchIn = searchIn.FindAll(x => x.dateCreate.Date <= (DateTime)dateCreateUsBefore.SelectedDate);
                }
                userDataGrid.ItemsSource = searchIn;
            }
           
          
        }

        private void dropFilterUser_Click(object sender, RoutedEventArgs e)
        {
            filterUser.city.FindAll(x => x.check == true).ToList().ForEach(c => c.check = false);
            filterUser.urls.FindAll(x => x.check == true).ToList().ForEach(c => c.check = false);
            filterUser.filterCourse.FindAll(x => x.check == true).ToList().ForEach(c => c.check = false);
            cmbSearchCourse.Items.Refresh();
            cmbSearchUrl.Items.Refresh();
            cmbSearchCity.Items.Refresh();
            userDataGrid.ItemsSource = users;

           
          
            dateHitFrom.SelectedDate = null;
            dateHitBefore.SelectedDate = null;
            dateCreateUsFrom.SelectedDate = null;
            dateCreateUsBefore.SelectedDate = null;
        }

        private void searchUserPanel_LostFocus(object sender, RoutedEventArgs e)
        {
            if(searchUserPanel.Text == string.Empty)
            {
                searchUserPanel.Text = "Ф.И.О, Email, Телефон, @ID";
            }
        }

        private void searchUserPanel_GotFocus(object sender, RoutedEventArgs e)
        {
            if (searchUserPanel.Text == "Ф.И.О, Email, Телефон, @ID")
            {
                searchUserPanel.Text = string.Empty;
            }
        }

        private void searchUserPanel_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (searchUserPanel.Text == string.Empty)
            {
                searchUserPanel.Text = "Ф.И.О, Email, Телефон, @ID";
            }
        }

        private void searchUserPanel_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (searchUserPanel.Text != string.Empty)
            {
                if (searchUserPanel.Text.IndexOf('@') == 0 )
                {
                    string find = searchUserPanel.Text.Replace("@", "");
                    userDataGrid.ItemsSource = users.FindAll(x => x.id.ToString() == find);
                }
                else
                {
                    userDataGrid.ItemsSource = users.FindAll(x => x.phone.ToLower().Contains(searchUserPanel.Text.ToLower()) || x.name.ToLower().Contains(searchUserPanel.Text.ToLower()) || x.email.ToLower().Contains(searchUserPanel.Text.ToLower()));
                }
              
            }
            else
                userDataGrid.ItemsSource = users;
        }

        private void FilterUserBut_Click(object sender, RoutedEventArgs e)
        {
            if (filterGridPanel.Visibility == Visibility.Collapsed)
            {
                filterGridPanel.Visibility = Visibility.Visible;
            }
            else
                filterGridPanel.Visibility = Visibility.Collapsed;
        }

        private void createTask_Click(object sender, RoutedEventArgs e)
        {
            if(users.FindAll(x=>x.select == true).Count > 0)
            {
                if (users.FindAll(x => x.select == true).Count <= 51)
                {
                    CreateTask createTask = new CreateTask(users.FindAll(x => x.select == true), manager);
                    createTask.Show();
                }
                else
                {
                    MessageBox.Show("Превышен лимит на открытие. Макс. 50 пользователей");
                }
            }
            else
            {
                MessageBox.Show("Выберите кому открыть задачу");
            }
        }

        private async void openTask_Click(object sender, RoutedEventArgs e)
        {
            openTask.Tag = 2.ToString();
            openUser.Tag = 0.ToString();
            countTask.Visibility = Visibility.Visible;
            countUser.Visibility = Visibility.Collapsed;
            panelTask.Visibility = Visibility.Visible;
            panelUser.Visibility = Visibility.Collapsed;
            AnimloadUser.Visibility = Visibility.Visible;
            SelectUser.Visibility = Visibility.Hidden;

            await Task.Run(() => { taskUsers = dataBase.GetTaskUsers(); });
            await Task.Run(() => { mng = dataBase.GetManagers(); });
            managerAssing.ItemsSource = mng;
            mng.Add(new Manager { id = -1, name = "Нет менеджера", acces = 103 });
            searchManager.ItemsSource = mng;
            TaskDataGrid.ItemsSource = taskUsers;
            productSearch.ItemsSource = productSearchList = taskUsers.GroupBy(x => x.nameProduct)
                        .Select(x => x.First())
                        .ToList();
            priorityCmbx.ItemsSource = taskUsers.GroupBy(x => x.rating)
                      .Select(x => x.First())
                      .ToList();
            AnimloadUser.Visibility = Visibility.Collapsed;
        }

        private async void openUser_Click(object sender, RoutedEventArgs e)
        {
            openTask.Tag = 0.ToString();
            openUser.Tag = 2.ToString();

            panelTask.Visibility = Visibility.Collapsed;
            panelUser.Visibility = Visibility.Visible;
            countTask.Visibility = Visibility.Collapsed;
            countUser.Visibility = Visibility.Visible;
            SelectUser.Visibility = Visibility.Visible;

            AnimloadUser.Visibility = Visibility.Visible;
            filterUser = new FillteUser();

            await Task.Run(() => filterUser.filterCourse = dataBase.GetFilterCourse());
            await Task.Run(() => { users = dataBase.getUsers(); });
       
            userDataGrid.ItemsSource = users;

            filterUser.SetCity(users);
            filterUser.SetUrls(users);
            cmbSearchCourse.ItemsSource = filterUser.filterCourse;
            cmbSearchUrl.ItemsSource = filterUser.urls;
            cmbSearchCity.ItemsSource = filterUser.city;
            AnimloadUser.Visibility = Visibility.Collapsed;
        }

        private void searchProduct_txt_LostFocus(object sender, RoutedEventArgs e)
        {
            if (searchProduct_txt.Text == string.Empty)
            {
                searchProduct_txt.Text = "Поиск по продукту...";
            }
        }

        private void searchProduct_txt_GotFocus(object sender, RoutedEventArgs e)
        {
            if (searchProduct_txt.Text == "Поиск по продукту...")
            {
                searchProduct_txt.Text = string.Empty;
            }
        }



        private void searchProduct_txt_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (searchProduct_txt.Text != string.Empty)
            {
                productSearch.ItemsSource = productSearchList.FindAll(x => x.nameProduct.ToLower().Contains(searchProduct_txt.Text.ToLower()));
            }
            else
            {
                productSearch.ItemsSource = productSearchList;
            }

        }

        private void searchProduct_txt_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (searchProduct_txt.Text == string.Empty)
            {
                searchProduct_txt.Text = "Поиск по продукту...";
            }
        }

        private void productSearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Fillter();
        }

        private void searchThems_LostFocus(object sender, RoutedEventArgs e)
        {
            if (searchThems.Text == string.Empty)
            {
                searchThems.Text = "Тема";
            }
        }

        private void searchIdTask_LostFocus(object sender, RoutedEventArgs e)
        {
            if (searchIdTask.Text == string.Empty)
            {
                searchIdTask.Text = "Номер задачи";
            }
        }

        private void searchThems_GotFocus(object sender, RoutedEventArgs e)
        {
            if (searchThems.Text == "Тема")
            {
                searchThems.Text = string.Empty;
            }
        }

        private void searchIdTask_GotFocus(object sender, RoutedEventArgs e)
        {
            if (searchIdTask.Text == "Номер задачи")
            {
                searchIdTask.Text = string.Empty;
            }
        }

        private void searchThems_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (searchThems.Text == string.Empty)
            {
                searchThems.Text = "Тема";
            }
        }

        private void searchIdTask_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (searchIdTask.Text == string.Empty)
            {
                searchIdTask.Text = "Номер задачи";
            }
        }
        
        private void dateTouchFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            Fillter();
        }

        private void dateTouchBefore_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            Fillter();
        }

        private void dropFilter_Click(object sender, RoutedEventArgs e)
        {
            searchManager.SelectedIndex = -1;
            productSearch.SelectedIndex = -1;
            priorityCmbx.SelectedIndex = -1;
            stateCmbx.SelectedIndex = -1;
            searchThems.Text = "Тема";
            searchIdTask.Text = "Номер задачи";
            typePosition.SelectedIndex = -1;
            dateTouchFrom.SelectedDate = null;
            dateTouchBefore.SelectedDate = null;
            dateCreatFrom.SelectedDate = null;
            dateCreateBefore.SelectedDate = null;

            //TaskDataGrid.ItemsSource = taskUsers;
            Fillter();
            
        }

        private void searchIdTask_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        private void searchIdTask_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            string str = searchIdTask.Text;
            int value;
            if (int.TryParse(string.Join("", str.Where(c => char.IsDigit(c))), out value))
            {
                searchIdTask.Text = value.ToString();
               
            }
            Fillter();
        }

        private void searchThems_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            Fillter();
        }

        private void priorityCmbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Fillter();
        }

        private void typePosition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (typePosition.SelectedIndex != -1)
            {
                Fillter();
            }
        }


        private void Fillter()
        {
            string product = string.Empty;
            string manager = string.Empty;
            string them = string.Empty;
            int numberTask = -1;
            int priority = -1;
            int stage = -1;
            string global = string.Empty;
            int state = -1;
            DateTime dateTouchFrom_ = new DateTime();
            DateTime dateTouchBefore_ = new DateTime();

            DateTime dateCreatFrom_ = new DateTime();
            DateTime dateCreateBefore_ = new DateTime();

            if (productSearch.SelectedIndex != -1)
            {
                product = ((MTask)productSearch.SelectedItem).nameProduct;
            }
            if (searchManager.SelectedIndex != -1)
            {
                manager = ((Manager)searchManager.SelectedItem).name;
            }
            if (searchThems.Text != "Тема")
            {
                them = searchThems.Text;
            }
            if (typePosition.SelectedIndex != -1)
            {
                ComboBoxItem item = (ComboBoxItem)typePosition.ItemContainerGenerator.ContainerFromItem(typePosition.SelectedItem);
                stage = Convert.ToInt32(item.Tag.ToString());
            }
            if (stateCmbx.SelectedIndex != -1)
            {
                ComboBoxItem item = (ComboBoxItem)stateCmbx.ItemContainerGenerator.ContainerFromItem(stateCmbx.SelectedItem);
                state = Convert.ToInt32(item.Tag.ToString());
            }
            if (priorityCmbx.SelectedIndex != -1)
            {

                priority = ((MTask)priorityCmbx.SelectedItem).rating;
            }
            if (searchIdTask.Text != "Номер задачи" && searchIdTask.Text != string.Empty)
            {
                numberTask = Convert.ToInt32(searchIdTask.Text);
            }
            if (search.Text != "Телефон, email")
            {
                global = search.Text;
            }
            if (dateTouchFrom.SelectedDate != null)
            {
                dateTouchFrom_ = (DateTime)dateTouchFrom.SelectedDate;
            }
            if (dateTouchBefore.SelectedDate != null)
            {
                dateTouchBefore_ = (DateTime)dateTouchBefore.SelectedDate;
            }
            if (dateCreatFrom.SelectedDate != null)
            {
                dateCreatFrom_ = (DateTime)dateCreatFrom.SelectedDate;
            }
            if (dateCreateBefore.SelectedDate != null)
            {
                dateCreateBefore_ = (DateTime)dateCreateBefore.SelectedDate;
            }
            TaskDataGrid.ItemsSource = sortFilter.GetSortTask(product, them, numberTask, priority, stage, taskUsers, global, dateTouchFrom_, dateTouchBefore_, dateCreatFrom_, dateCreateBefore_,state, manager);
            TaskDataGrid.Items.Refresh();

           
        }

        private void search_GotFocus(object sender, RoutedEventArgs e)
        {
            if (search.Text == "Телефон, email")
            {
                search.Text = string.Empty;
            }
        }

        private void search_LostFocus(object sender, RoutedEventArgs e)
        {
            if (search.Text == string.Empty )
            {
                search.Text = "Телефон, email";
            }
        }

        

        private void search_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            Fillter();
        }

        private void search_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (search.Text == string.Empty)
            {
                search.Text = "Телефон, email";
            }
        }

        private async void AssingMng_Click(object sender, RoutedEventArgs e)
        {
            mainGrid.IsEnabled = false;
           
            if (taskUsers.FindAll(x => x.select == true).Count > 0 && managerAssing.SelectedIndex != -1)
            {
                string idList = string.Empty;
                var selectItem = taskUsers.FindAll(x => x.select == true);
                await Task.Run(() =>
                {

                    for (int i = 0; i < selectItem.Count; i++)
                    {
                        var SearchTask = taskUsers.FindAll(x => x.state == 0 && x.idUser == selectItem[i].idUser);

                        if (selectItem[i].state != 1 && selectItem[i].state != 3)
                        {

                            if (i == selectItem.Count - 1)
                            {
                                idList = idList + selectItem[i].idUser;
                            }
                            else
                                idList = idList + selectItem[i].idUser + ",";

                        }
                    }
                });
                if (idList != string.Empty)
                {
                    int idManager = ((Manager)managerAssing.SelectedItem).id;
                    string name = ((Manager)managerAssing.SelectedItem).name;
                    bool check = false;
                    await Task.Run(() =>
                    {
                        check = dataBase.AssingTask(idList, idManager);
                    });
                    if (check)
                    {
                        await Task.Run(() =>
                        {
                            foreach (var item in selectItem)
                            {
                                if (item.state != 1 && item.state != 2)
                                {
                                    item.manager = name;
                                    foreach (var lstItem in taskUsers.FindAll(x => x.idUser == item.idUser))
                                    {
                                        if (lstItem.state != 1 && lstItem.state != 3 && lstItem.state != 2)
                                        {
                                            lstItem.manager = name;
                                        }

                                    }
                                }


                            }

                        });
                        TaskDataGrid.Items.Refresh();

                    }
                    else
                    {
                        MessageBox.Show("Ошибка");
                    }

                }
            }
            else
            {
                MessageBox.Show("Не все условия выполнены...");
            }
            mainGrid.IsEnabled = true;
        }

        private void stateCmbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Fillter();
        }

        private void FilterBut_Click_1(object sender, RoutedEventArgs e)
        {
            if (gridFilter.Visibility == Visibility.Collapsed)
            {
                gridFilter.Visibility = Visibility.Visible;
            }
            else
                gridFilter.Visibility = Visibility.Collapsed;
        }

        public void TaskDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TaskDataGrid.SelectedIndex != -1)
            {
                MTask mTask = (MTask)TaskDataGrid.SelectedItem;
                rigth_menu.Content = new TaskPage(mTask, TaskDataGrid, manager, this);
            }
        }

        private void userDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (userDataGrid.SelectedIndex != -1)
            {
               Users selUser = (Users)userDataGrid.SelectedItem;
               rigth_menu.Content = new UserPage(selUser,  manager, this);
            }
        }

        private void TaskPanelOP_Click(object sender, RoutedEventArgs e)
        {
            TaskWnd taskWnd = new TaskWnd(2,manager);
            taskWnd.Show();
        }

        private void TaskPanelOM_Click(object sender, RoutedEventArgs e)
        {
            TaskWnd taskWnd = new TaskWnd(1,manager);
            taskWnd.Show();
        }

        private void ShowCours_Click(object sender, RoutedEventArgs e)
        {
            CourseTreeViewWnd courseTreeViewWnd = new CourseTreeViewWnd(manager);
            courseTreeViewWnd.Show();
        }

        private void searchManager_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (searchManager.SelectedIndex != -1)
            {
                Fillter();
            }
          
        }

        private void dropSelectUser_Click(object sender, RoutedEventArgs e)
        {
            if (SelectUser.Tag.ToString() == 1.ToString())
            {
                SelectUser.Tag = 4.ToString();
                var visTable = (List<Users>)userDataGrid.ItemsSource;
                visTable.ForEach(x => x.select = true);
                userDataGrid.Items.Refresh();
            }
            else
            {
                SelectUser.Tag = 1.ToString();
                users.ForEach(x => x.select = false);
                userDataGrid.Items.Refresh();
            }
            
        }

        private void SelectTask_Click(object sender, RoutedEventArgs e)
        {
            if (SelectTask.Tag.ToString() == 1.ToString())
            {
                SelectTask.Tag = 4.ToString();
                var visTable = (List<MTask>)TaskDataGrid.ItemsSource;
                visTable.ForEach(x => x.select = true);
                TaskDataGrid.Items.Refresh();
            }
            else
            {
                SelectTask.Tag = 1.ToString();
                taskUsers.ForEach(x => x.select = false);
                TaskDataGrid.Items.Refresh();
            }
        }

        private void ShowCastomere_Click(object sender, RoutedEventArgs e)
        {
            CastomereTask castomereTask = new CastomereTask(manager);
            castomereTask.ShowDialog();
            castomereTask.Close();
        }

        private void backMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
            this.Content = null;
        }

        private void unlodingExсel_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Excel File|*.xls";
            if (saveFileDialog.ShowDialog() == true)
            {
                string fileSave = saveFileDialog.FileName;
                var Items = (List<MTask>)TaskDataGrid.ItemsSource;
                saveInFile(fileSave, Items);
            }
               

        }

        private void saveInFile(string path, List<MTask> tasks)
        {
            string filename = path;
            Workbook workbook = new Workbook();
            Worksheet sheet = new Worksheet("Data");
            workbook.Worksheets.Add(sheet);

            for (int i = 0; i < tasks.Count; i++)
            {
                sheet.Cells[i, 0] = new Cell(tasks[i].idTask);
                sheet.Cells[i, 1] = new Cell(tasks[i].idUser);
                sheet.Cells[i, 2] = new Cell(tasks[i].nameUser);
                sheet.Cells[i, 3] = new Cell(tasks[i].date_touch.ToString("dd.MM.yyyy"));
                sheet.Cells[i, 4] = new Cell(tasks[i].date_creation.ToString("dd.MM.yyyy"));
                sheet.Cells[i, 5] = new Cell(tasks[i].nameProduct);
                sheet.Cells[i, 6] = new Cell(tasks[i].title);
                sheet.Cells[i, 7] = new Cell(tasks[i].manager);
                sheet.Cells[i, 8] = new Cell(tasks[i].rating);
                sheet.Cells[i, 9] = new Cell(tasks[i].type);
                sheet.Cells[i, 10] = new Cell(tasks[i].state);
                sheet.Cells[i, 11] = new Cell(tasks[i].fileterData);


            }
            workbook.Save(filename);
            MessageBox.Show("Сохранено");
        }

        private void reСreate_Click(object sender, RoutedEventArgs e)
        {
            if(TaskDataGrid.Items.Count != 0)
            {
                var items = (List<MTask>)TaskDataGrid.ItemsSource;
                var reCreateList = items.FindAll(x => x.select == true);
                if (reCreateList.Count > 0)
                {
                    ReCreate reCreate = new ReCreate(reCreateList, manager);
                    reCreate.ShowDialog();
                    reCreate.Close();
                }
                else
                {
                    MessageBox.Show("Список пуст");
                }
            }
        }
    }
}
