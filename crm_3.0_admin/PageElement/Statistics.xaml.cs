﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для Statistics.xaml
    /// </summary>
    public partial class Statistics : Page
    {
        public Statistics(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
            Start();
        }
        StatisticsData statisticsData = new StatisticsData();
        DataBase dataBase;
        private async void Start()
        {
           
            
            DateTime before= (DateTime)fromDt.SelectedDate;
            DateTime from = (DateTime)beforeDt.SelectedDate;
          
            await Task.Run(() => { statisticsData = dataBase.GetStatistics(from, before); });
            if (statisticsData != null)
            {
                selectChanger.SelectedIndex = -1;
                managerStat.ItemsSource = statisticsData.statisticsAlls;
                managerStat.Items.Refresh();
                notManger.Text = String.Format("Без менеджера задач: {0}", statisticsData.statisticsWithoutManager.count);
                notMangerEnd.Text = String.Format("Без менеджера задач (Просроченных): {0}", statisticsData.statisticsWithoutManager.c_from);
                notMangerNow.Text = String.Format("Без менеджера задач (Активных): {0}", statisticsData.statisticsWithoutManager.c_before);
                statRangs.ItemsSource = statisticsData.statisticsRangs;
                statRangs.Items.Refresh();
                selectChanger.SelectedIndex = 9;
            }
            else
            {
                managerStat.ItemsSource =null;
                managerStat.Items.Refresh();
                statRangs.ItemsSource = null;
                statRangs.Items.Refresh();
                statChange.ItemsSource = null;
                statChange.Items.Refresh();
            }
           
        }

        private void selectChanger_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items.Count != 0)
            {
                ComboBoxItem utcSel = (ComboBoxItem)items[0];
                if (statisticsData.statisticsChanges != null)
                {
                    statChange.ItemsSource = statisticsData.statisticsChanges.FindAll(x => x.chenge_id == Convert.ToInt32(utcSel.Tag.ToString()));
                    statChange.Items.Refresh();
                }
                else
                {
                    statChange.ItemsSource =null;
                    statChange.Items.Refresh();
                }
              
            }
           
        }

        private async void show_Click(object sender, RoutedEventArgs e)
        {
            if (statPayment.Visibility == Visibility.Visible)
            {
                DateTime before = (DateTime)fromDt.SelectedDate;
                DateTime from = (DateTime)beforeDt.SelectedDate;
                List<StatisticsPayment> stat = new List<StatisticsPayment>();
                await Task.Run(() => {  stat = dataBase.GetStatisticsPayments(from, before); });
                statPayment.ItemsSource = stat;
            }
            else
            {
                Start();
            }
        }

        private void UniqueTasks_Click(object sender, RoutedEventArgs e)
        {
            statChange.ItemsSource = statisticsData.staticsUniqueTasks;
            statChange.Items.Refresh();
        }

        private void stat_stage_Click(object sender, RoutedEventArgs e)
        {
            VisabilityHendler();
            statRangs.Visibility = Visibility.Visible;
        }

        private void stat_event_Click(object sender, RoutedEventArgs e)
        {
            VisabilityHendler();
            stat_event_grid.Visibility = Visibility.Visible;
        }

        private void stat_payment_Click(object sender, RoutedEventArgs e)
        {
            VisabilityHendler();
            statPayment.Visibility = Visibility.Visible;
        }

        private void VisabilityHendler()
        {
            stat_event_grid.Visibility = Visibility.Hidden;
            statRangs.Visibility = Visibility.Hidden;
            statPayment.Visibility = Visibility.Hidden;
        }
    }
}
