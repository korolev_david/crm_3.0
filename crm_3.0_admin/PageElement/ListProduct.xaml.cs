﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для ListProduct.xaml
    /// </summary>
    public partial class ListProduct : Page
    {
        DataBase dataBase;
        public ListProduct(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
            Start();
        }
        List<IDProduct> iDProducts = new List<IDProduct>();
        private async void Start()
        {
           
            iDProducts = await Task.Run(() => dataBase.GetProductsId());
            list_product_grid.ItemsSource = iDProducts;
            list_product_grid.Items.Refresh();
          
        }
        private void search_product_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (search_product.Text != string.Empty)
            {
                list_product_grid.ItemsSource = iDProducts.FindAll(x => x.name.ToLower().Contains(search_product.Text.ToLower()));
                list_product_grid.Items.Refresh();
            }
            else
            {
                list_product_grid.ItemsSource = iDProducts;
                list_product_grid.Items.Refresh();
            }

        }
    }
}
