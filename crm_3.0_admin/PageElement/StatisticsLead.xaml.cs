﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для StatisticsLead.xaml
    /// </summary>
    public partial class StatisticsLead : Page
    {
        public StatisticsLead(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
        }
        DataBase dataBase;
        private async void show_Click(object sender, RoutedEventArgs e)
        {
            List<StatisticsLeadClass> statisticsLeadClasses = new List<StatisticsLeadClass>();
           
            DateTime before = (DateTime)beforeDt.SelectedDate;
            DateTime from = (DateTime)fromDt.SelectedDate; 
            await Task.Run(() => { statisticsLeadClasses = dataBase.GetStatisticsLead(from, before); });
            leadStat.ItemsSource = statisticsLeadClasses;
            leadStat.Items.Refresh();

        }
    }
}
