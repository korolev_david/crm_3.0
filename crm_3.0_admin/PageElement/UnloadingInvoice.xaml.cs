﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using ExcelLibrary.Office.Excel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для UnloadingInvoice.xaml
    /// </summary>
    public partial class UnloadingInvoice : Page
    {
        List<InvoiceExcel> ordersAlls = new List<InvoiceExcel>();
        DataBase dataBase;
    
        public UnloadingInvoice(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
            Start();
        }
        private async void Start()
        {
            loadMini.Visibility = Visibility.Visible;
            await Task.Run(() => { ordersAlls = dataBase.GetInvoiceExcels(); });
            orders_all.ItemsSource = ordersAlls;
            Payment_count.Text = ordersAlls.FindAll(x => x.payment > 0 && x.rest <= 0).Count.ToString();
            Unpaid_count.Text = ordersAlls.FindAll(x => x.payment <= 0 && x.rest > 0).Count.ToString();
            not_fully_paid.Text = ordersAlls.FindAll(x => x.payment > 0 && x.rest > 0).Count.ToString();
            sum_paid.Text = ordersAlls.Sum(x => x.payment).ToString();
            if (ordersAlls != null)
            { 
            manager_sel.ItemsSource =from order in ordersAlls
            group order by order.nameManager into g
            select new ManagerOrders { nameManager = g.Key };
            }
           

            loadMini.Visibility = Visibility.Collapsed;
        }

        private void idOrdersSearch_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        private void idOrdersSearch_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (idOrdersSearch.Text != string.Empty)
            {
                orders_all.ItemsSource = ordersAlls.FindAll(x => x.idOrder == Convert.ToInt32(idOrdersSearch.Text));
                orders_all.Items.Refresh();
            }
            else
            {
                orders_all.ItemsSource = ordersAlls;
                orders_all.Items.Refresh();
            }
        }
        private void sortOrders()
        {
            List<InvoiceExcel> lstOrder = ordersAlls;
            string manager = string.Empty;
            string product = string.Empty;
            int sum = 0;
            DateTime datestr = new DateTime();
            DateTime dateEnd = new DateTime();
            if (manager_sel.SelectedIndex != -1)
            {
                manager = ((ManagerOrders)manager_sel.SelectedItem).nameManager;
                lstOrder = lstOrder.FindAll(x => x.nameManager == manager);
            }
            //if (product_sel.SelectedIndex != -1)
            //{
            //    product = ((ProductOrders)product_sel.SelectedItem).nameProduct;
            //}
            if (productSearch.Text != string.Empty)
            {
                lstOrder = lstOrder.FindAll(x => x.product.ToLower().Contains( productSearch.Text.ToLower()));
            }
            if (sum_sel.Text != string.Empty)
            {
                int number = 0;
                bool isNum = int.TryParse(sum_sel.Text, out number);
                sum = number;
                lstOrder = lstOrder.FindAll(x => x.sumSale == sum);
            }
            if (data_start.SelectedDate != null)
            {
                datestr = (DateTime)data_start.SelectedDate;
                if (datestr != Convert.ToDateTime("01.01.0001 0:00:00"))
                {
                    lstOrder = lstOrder.FindAll(x => x.date.Date >= datestr);
                }
            }
            if (date_end.SelectedDate != null)
            {
                dateEnd = (DateTime)date_end.SelectedDate;
                if (dateEnd != Convert.ToDateTime("01.01.0001 0:00:00"))
                {
                    lstOrder = lstOrder.FindAll(x => x.date.Date <= dateEnd);
                }
            }
            if (lstOrder.Count != 0)
            {  
                sum_paid.Text = lstOrder.Sum(x => x.payment).ToString();
                Payment_count.Text = lstOrder.FindAll(x => x.payment > 0 && x.rest <= 0).Count.ToString();
                Unpaid_count.Text = lstOrder.FindAll(x => x.payment <= 0 && x.rest > 0).Count.ToString();
                not_fully_paid.Text = lstOrder.FindAll(x => x.payment > 0 && x.rest > 0).Count.ToString();
            }
           
            orders_all.ItemsSource = lstOrder;
            orders_all.Items.Refresh();
        }

        private void Manager_sel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            sortOrders();
        }

        //private void Product_sel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{

        //}

        private void Sum_sel_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        private void Sum_sel_KeyUp(object sender, KeyEventArgs e)
        {
            sortOrders();
        }

        private void Data_start_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            sortOrders();
        }

        private void Date_end_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            sortOrders();
        }

        private async void orders_all_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (orders_all.SelectedIndex != -1)
            {
                payment_panel.Visibility = Visibility.Visible;
                var item = (InvoiceExcel)orders_all.SelectedItem;
                paymentStat.ItemsSource = await Task.Run(() => dataBase.getPayment(item.idOrder));
                paymentStat.Items.Refresh();
            }
        }

        private void closePanelPayment_Click(object sender, RoutedEventArgs e)
        {
            payment_panel.Visibility = Visibility.Collapsed;
        }

        private async void InExcel_orders_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            //saveFileDialog.AddExtension = true;
            saveFileDialog.Filter = "Excel File|*.xls";
            if (saveFileDialog.ShowDialog() == true)
            {
                if (orders_all.Items.Count > 0)
                {
                    try
                    {
                        string filename = saveFileDialog.FileName.ToString();
                        List<InvoiceExcel> ordersAlls = (List<InvoiceExcel>)orders_all.ItemsSource;
                        await Task.Run(() => saveInFile(filename, ordersAlls));

                    }
                    catch
                    {
                        MessageBox.Show("Ошибка сохранения");
                    }

                }
                else
                {
                    MessageBox.Show("Список пуст!");
                }

            }
        }
        private void saveInFile(string path, List<InvoiceExcel> ordersAlls)
        {
            string filename = path;
            Workbook workbook = new Workbook();
            Worksheet sheet = new Worksheet("Data");
            workbook.Worksheets.Add(sheet);

            for (int i = 0; i < ordersAlls.Count; i++)
            {
                sheet.Cells[i, 0] = new Cell(ordersAlls[i].idOrder);
                sheet.Cells[i, 1] = new Cell(ordersAlls[i].nameManager);
                sheet.Cells[i, 2] = new Cell(ordersAlls[i].date.Date.ToString());
                sheet.Cells[i, 3] = new Cell(ordersAlls[i].sumSale);
                sheet.Cells[i, 4] = new Cell(ordersAlls[i].sum);
                sheet.Cells[i, 5] = new Cell(ordersAlls[i].product);
                sheet.Cells[i, 6] = new Cell(ordersAlls[i].payment);
                sheet.Cells[i, 7] = new Cell(ordersAlls[i].rest);
                sheet.Cells[i, 8] = new Cell(ordersAlls[i].dataUser);
                sheet.Cells[i, 9] = new Cell(ordersAlls[i].sale);
                sheet.Cells[i, 10] = new Cell(ordersAlls[i].profit);


            }
            workbook.Save(filename);
            MessageBox.Show("Сохранено");
        }

      
        private void DropDate_Click(object sender, RoutedEventArgs e)
        {
            data_start.SelectedDate = null;
            date_end.SelectedDate = null;
            manager_sel.SelectedIndex = -1;
            productSearch.Text = string.Empty;
            sum_sel.Text = string.Empty;
        }

        private void productSearch_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            sortOrders();
        }
    }
}
