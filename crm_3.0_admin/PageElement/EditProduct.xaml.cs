﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для CreatedProduct.xaml
    /// </summary>
    public partial class EditProduct : Page
    {
        public EditProduct(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
            Start();
        }
        DataBase dataBase;
        private async void Start()
        {
            sel_product_reg.ItemsSource = await Task.Run(() => dataBase.GetProducts());
            curse_rel_reg.ItemsSource = await Task.Run(() => dataBase.GetСourses());
        }
        private async void Sel_product_reg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sel_product_reg.SelectedItem != null)
            {
              
                name_curse_reg.Text = ((CreatedProductClass)sel_product_reg.SelectedItem).name;
                discr_reg.Text = ((CreatedProductClass)sel_product_reg.SelectedItem).disk;
                //rel_curse_txt.Text = ((Product)sel_product_reg.SelectedItem).rel_curse;
                cost_reg.Text = ((CreatedProductClass)sel_product_reg.SelectedItem).coast.ToString();
                time_reg.Text = ((CreatedProductClass)sel_product_reg.SelectedItem).date.ToString();
                int id = ((CreatedProductClass)sel_product_reg.SelectedItem).id;
                listGrid_add_reg.ItemsSource = await Task.Run(() => dataBase.getRelProduct(id));
                listGrid_add_reg.Items.Refresh();
            }
        }

        private void Time_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        private void Cost_reg_PreviewTextInput_1(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789 ,".IndexOf(e.Text) < 0;
        }

        private void Curse_rel_reg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (curse_rel_reg.SelectedIndex != -1)
            {
                var item = (Course_CL)curse_rel_reg.SelectedItem;
                lessonSel_reg.ItemsSource = item.parts;
            }
        }

        private async void Add_curs_reg_Click(object sender, RoutedEventArgs e)
        {
            if (sel_product_reg.SelectedIndex != -1 && curse_rel_reg.SelectedIndex != -1)
            {
               
                int id_p = ((CreatedProductClass)sel_product_reg.SelectedItem).id;
                int id_obj = ((Course_CL)curse_rel_reg.SelectedItem).id_course;

                await Task.Run(() => dataBase.addRelCurse(id_p, id_obj));
                int id = ((CreatedProductClass)sel_product_reg.SelectedItem).id;

                listGrid_add_reg.ItemsSource = await Task.Run(() => dataBase.getRelProduct(id));
                listGrid_add_reg.Items.Refresh();
            }
        }

        private async void Add_lesson_reg_Click(object sender, RoutedEventArgs e)
        {
            if (sel_product_reg.SelectedIndex != -1 && lessonSel_reg.SelectedIndex != -1)
            {
                
                int id_p = ((CreatedProductClass)sel_product_reg.SelectedItem).id;
                int id_obj = ((Part)lessonSel_reg.SelectedItem).id_part;

                await Task.Run(() => dataBase.addRelCurse(id_p, id_obj));
                int id = ((CreatedProductClass)sel_product_reg.SelectedItem).id;

                listGrid_add_reg.ItemsSource = await Task.Run(() => dataBase.getRelProduct(id));
                listGrid_add_reg.Items.Refresh();
            }
        }

        private async void Dell_add_curse_rel_Click(object sender, RoutedEventArgs e)
        {
            if (listGrid_add_reg.SelectedIndex != -1 && sel_product_reg.SelectedIndex != -1)
            {
               
                int id_p = ((CreatedProductClass)sel_product_reg.SelectedItem).id;
                int id_obj = ((AddRefCurs)listGrid_add_reg.SelectedItem).id;
                await Task.Run(() => dataBase.dellRelCurs(id_p, id_obj));
                int id = ((CreatedProductClass)sel_product_reg.SelectedItem).id;

                listGrid_add_reg.ItemsSource = await Task.Run(() => dataBase.getRelProduct(id));
                listGrid_add_reg.Items.Refresh();
            }
        }

        private async void Save_reg_Click(object sender, RoutedEventArgs e)
        {
            if (sel_product_reg.SelectedItem != null)
            {
                
                
                string name = name_curse_reg.Text;
                string disc = discr_reg.Text;
                string date = time_reg.Text;
                int id_pr = ((CreatedProductClass)sel_product_reg.SelectedItem).id;
                int cost = Convert.ToInt32(cost_reg.Text);
                main.IsEnabled = false;
                await Task.Run(() => dataBase.reg_product(name, cost, disc, id_pr, date));
                sel_product_reg.ItemsSource = await Task.Run(() => dataBase.GetProducts());
                //curse_rel_reg.ItemsSource = await Task.Run(() => dataBase.get_curse());
                main.IsEnabled = true;
            }
        }

    }
}
