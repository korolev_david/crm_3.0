﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для СostAnalysis.xaml
    /// </summary>
    public partial class СostAnalysis : Page
    {
        public СostAnalysis(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
            Start();
        }
        DataBase dataBase;
        List<PaymentFromData> paymentFroms = new List<PaymentFromData>();
        private async void Start()
        {
            await Task.Run(() => paymentFroms = dataBase.getDataPaymentFrom());
            dataFromPayment.ItemsSource = paymentFroms;
        }

        private void awaitingBut_Click(object sender, RoutedEventArgs e)
        {
            if (dateFrom.SelectedDate != null && dateBefore.SelectedDate != null)
            {
                dataFromPayment.ItemsSource = paymentFroms.FindAll(x => x.confirm == null && x.date.Date > dateFrom.SelectedDate && x.date.Date < dateBefore.SelectedDate);
                dataFromPayment.Items.Refresh();
            }
            else
            {
                dataFromPayment.ItemsSource = paymentFroms.FindAll(x => x.confirm == null);
                dataFromPayment.Items.Refresh();
            }
           
        }

        private void confirmBut_Click(object sender, RoutedEventArgs e)
        {
            if (dateFrom.SelectedDate != null && dateBefore.SelectedDate != null)
            {
                dataFromPayment.ItemsSource = paymentFroms.FindAll(x => x.confirm == "confirm" && x.date.Date > dateFrom.SelectedDate && x.date.Date < dateBefore.SelectedDate);
                dataFromPayment.Items.Refresh();
            }
            else
            {
                dataFromPayment.ItemsSource = paymentFroms.FindAll(x => x.confirm == "confirm");
                dataFromPayment.Items.Refresh();
            }
            
        }

        private void rejectBut_Click(object sender, RoutedEventArgs e)
        {
            if (dateFrom.SelectedDate != null && dateBefore.SelectedDate != null)
            {
                dataFromPayment.ItemsSource = paymentFroms.FindAll(x => x.confirm == "reject" && x.date.Date > dateFrom.SelectedDate && x.date.Date < dateBefore.SelectedDate);
                dataFromPayment.Items.Refresh();
            }
            else
            {
                dataFromPayment.ItemsSource = paymentFroms.FindAll(x => x.confirm == "reject");
                dataFromPayment.Items.Refresh();
            }
            
        }

        private void allBut_Click(object sender, RoutedEventArgs e)
        {
            dataFromPayment.ItemsSource = paymentFroms;
            dataFromPayment.Items.Refresh();
        }

     
        private void fillter_Click(object sender, RoutedEventArgs e)
        {
            if (dateFrom.SelectedDate != null && dateBefore.SelectedDate != null)
            {

                dataFromPayment.ItemsSource = paymentFroms.FindAll(x => x.date.Date > dateFrom.SelectedDate && x.date.Date < dateBefore.SelectedDate);

            }
            else
                MessageBox.Show("Выберите дату");
        }


      

        private void dataFromPayment_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var item = (List<PaymentFromData>)dataFromPayment.ItemsSource;

            confirm.Text = item.FindAll(x => x.confirm == "confirm").Count.ToString();
            reject.Text = item.FindAll(x => x.confirm == "reject").Count.ToString();
            awating.Text = item.FindAll(x => x.confirm == null).Count.ToString();

            totalAmountRub.Text = item.FindAll(x => x.currency == "rub").Sum(y => y.sum).ToString();
            totalAmountUah.Text = item.FindAll(x => x.currency == "uah").Sum(y => y.sum).ToString();
            totalAmountEur.Text = item.FindAll(x => x.currency == "eur").Sum(y => y.sum).ToString();
            totalAmountUsd.Text = item.FindAll(x => x.currency == "dol").Sum(y => y.sum).ToString();

            totalCommissionRub.Text = item.FindAll(x => x.currency == "rub").Sum(y => y.comission).ToString();
            totalCommissionUah.Text = item.FindAll(x => x.currency == "uah").Sum(y => y.comission).ToString();
            totalCommissionEur.Text = item.FindAll(x => x.currency == "eur").Sum(y => y.comission).ToString();
            totalCommissionUsd.Text = item.FindAll(x => x.currency == "dol").Sum(y => y.comission).ToString();
        }

        private void dropFillter_Click(object sender, RoutedEventArgs e)
        {
            dateFrom.SelectedDate = null;
            dateBefore.SelectedDate = null;
        }
    }
    
}
