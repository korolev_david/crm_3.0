﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для CreateProduct.xaml
    /// </summary>
    public partial class CreateProduct : Page
    {
        List<AddRefCurs> addRefCurs = new List<AddRefCurs>();
        DataBase dataBase;
        public CreateProduct(Manager mng)
        {
            InitializeComponent();
            dataBase = new DataBase(mng.id);
            Start();
        }
        private async void Start()
        {
            curse_rel.ItemsSource = await Task.Run(() => dataBase.GetСourses());
        }

        private void Coast_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        private void Time_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        private void Add_curs_Click(object sender, RoutedEventArgs e)
        {
            if (curse_rel.SelectedIndex != -1)
            {
                var item = (Course_CL)curse_rel.SelectedItem;
                addRefCurs.Add(new AddRefCurs { id = item.id_course, name = item.name_course });

                listGrid_add.ItemsSource = addRefCurs;
                listGrid_add.Items.Refresh();
            }
        }

        private void Curse_rel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (curse_rel.SelectedIndex != -1)
            {
                var item = (Course_CL)curse_rel.SelectedItem;
                lessonSel.ItemsSource = item.parts;
            }
        }

        private void Add_lesson_Click(object sender, RoutedEventArgs e)
        {
            if (lessonSel.SelectedIndex != -1)
            {
                var item = (Part)lessonSel.SelectedItem;
                addRefCurs.Add(new AddRefCurs { id = item.id_part, name = item.name_part });
                listGrid_add.ItemsSource = addRefCurs;
                listGrid_add.Items.Refresh();
            }
        }

        private void Dell_add_curse_Click(object sender, RoutedEventArgs e)
        {
            if (listGrid_add.SelectedItem != null)
            {
                addRefCurs.Remove((AddRefCurs)listGrid_add.SelectedItem);
                listGrid_add.Items.Refresh();
            }
        }

        private async void Add_product_Click(object sender, RoutedEventArgs e)
        {
            if (name_product.Text != string.Empty && coast.Text != string.Empty && curse_rel.SelectedItem != null)
            {
                products_add.IsEnabled = false;
                string name = name_product.Text;
                string cost = coast.Text;
                string dis = disc.Text;
                string day = time.Text;

             
                string id_curse = string.Empty;
                for (int i = 0; i < addRefCurs.Count; i++)
                {
                    id_curse += addRefCurs[i].id.ToString() + ";";
                }
                await Task.Run(() => dataBase.add_product(name, cost, dis, id_curse, day));
                dell_product.ItemsSource = await Task.Run(() => dataBase.GetProducts());

                name_product.Text = string.Empty;
                coast.Text = string.Empty;
                disc.Text = string.Empty;
                products_add.IsEnabled = true;
                listGrid_add.ItemsSource = null;
                listGrid_add.Items.Refresh();
                time.Text = string.Empty;
                curse_rel.SelectedIndex = -1;
                lessonSel.SelectedIndex = -1;
            }
            else
            {
                MessageBox.Show("Заполните все поля");
            }
        }

        private async void Dell_products_Click(object sender, RoutedEventArgs e)
        {
            if (dell_product.SelectedIndex != -1)
            {
                var item = (CreatedProductClass)dell_product.SelectedItem;
             
                bool check = await Task.Run(() => dataBase.dell_product(item.id));
                if (!check)
                {
                    MessageBox.Show("Продукт невозможно удалить так как на нём есть открытые счета");
                }
                else
                {
                    dell_product.ItemsSource = await Task.Run(() => dataBase.GetProducts());
                }

            }
        }
    }
}
