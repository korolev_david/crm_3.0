﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для StatisticsProduct.xaml
    /// </summary>
    public partial class StatisticsProduct : Page
    {
        public StatisticsProduct(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
        }
        DataBase dataBase;


        private async void show1_Click(object sender, RoutedEventArgs e)
        {
            List<StatisticsRangProductStage> statisticsRangProductStages = new List<StatisticsRangProductStage>();
            DateTime before = (DateTime)beforeDt1.SelectedDate;
            DateTime from = (DateTime)fromDt1.SelectedDate;
            await Task.Run(() => { statisticsRangProductStages = dataBase.GetStatisticsRangProductStages(from, before); });
            stageProduct.ItemsSource = statisticsRangProductStages;
            stageProduct.Items.Refresh();
        }
        List<StatisticsRangCloseProduct> StatisticsCloseProduct = new List<StatisticsRangCloseProduct>();
        private async void show2_Click(object sender, RoutedEventArgs e)
        {
           
            DateTime before = (DateTime)beforeDt2.SelectedDate;
            DateTime from = (DateTime)fromDt2.SelectedDate;
            await Task.Run(() => { StatisticsCloseProduct = dataBase.GetStatisticsRangCloses(from, before); });
            cmbxСause.Items.Clear();
            var items = StatisticsCloseProduct.GroupBy(x => x.change)
                        .Select(x => x.First())
                        .ToList();
            foreach (var item in items)
            {
                cmbxСause.Items.Add(item.change);
            }
            if (cmbxСause.Items.Count > 0)
            {
                cmbxСause.SelectedIndex = 0;
            }
            
        }

        private void cmbxСause_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbxСause.SelectedIndex != -1)
            {
                string search = cmbxСause.Items[cmbxСause.SelectedIndex].ToString();
                statProductClose.ItemsSource = StatisticsCloseProduct.FindAll(x => x.change == search);
                statProductClose.Items.Refresh();
            }

        }
    }
}
