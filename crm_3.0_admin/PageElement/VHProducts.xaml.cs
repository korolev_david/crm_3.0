﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для VHProducts.xaml
    /// </summary>
    public partial class VHProducts : Page
    {
        DataBase dataBase;
        public VHProducts(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
            Start();
        }
        List<IDProduct> products = new List<IDProduct>(); 
        private async void Start()
        {
            products = await Task.Run(() => dataBase.GetVHProduct());
            vhDataGrid.ItemsSource = products;
        }

        private async void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (vhDataGrid.SelectedIndex != -1)
            {
                var item =  (IDProduct)vhDataGrid.SelectedItem;
                vhDataGrid.SelectedIndex = -1;
                bool check = await Task.Run(() => dataBase.SetProductVH(item.hiden, item.id_product));
                if (!check)
                {
                    item.hiden = 0;
                    MessageBox.Show("Данные не изменены");
                    vhDataGrid.Items.Refresh();
                }
            }
        }

        private async void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (vhDataGrid.SelectedIndex != -1)
            {
                var item = (IDProduct)vhDataGrid.SelectedItem;
                vhDataGrid.SelectedIndex = -1;
                bool check = await Task.Run(() => dataBase.SetProductVH(item.hiden,item.id_product));
                if(!check)
                {
                    item.hiden = 1;
                    MessageBox.Show("Данные не изменены");
                    vhDataGrid.Items.Refresh();
                }
            }
          
        }

        private void search_GotFocus(object sender, RoutedEventArgs e)
        {
            if (search.Text == "Поиск")
            {
                search.Text = string.Empty;
            }    
        }

        private void search_LostFocus(object sender, RoutedEventArgs e)
        {
            if (search.Text == string.Empty)
            {
                search.Text = "Поиск";
            }
        }

        private void search_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (search.Text == "Поиск")
            {
                search.Text = string.Empty;
            }

        }

        private void search_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (search.Text != string.Empty)
            {
                vhDataGrid.ItemsSource = products.FindAll(x => x.name.Contains(search.Text));
            }
            else
            {
                vhDataGrid.ItemsSource = products;
            }
        }
        
    }
}
