﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для PaymentProfile.xaml
    /// </summary>
    public partial class PaymentProfile : Page
    {
        List<ProductProfile> productProfiles = new List<ProductProfile>();
        List<PaymentProfileData> paymentProfileDatas = new List<PaymentProfileData>();
        DataBase dataBase ;
        public PaymentProfile(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
            GetData();
        }

        private async void GetData()
        {
            await Task.Run(() => { productProfiles = dataBase.GetProductProfiles(); });
          
            list_product_grid.ItemsSource = productProfiles;
            await Task.Run(() => { paymentProfileDatas = dataBase.GetPaymentProfileDatas(); });
            profilePaymentCmbx.ItemsSource = paymentProfileDatas;
        }

        private void search_product_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (search_product.Text != string.Empty)
            {
                list_product_grid.ItemsSource = productProfiles.FindAll(x => x.name.ToLower().Contains(search_product.Text.ToLower()));
            }
            else
            {
                list_product_grid.ItemsSource = productProfiles;
            }
        }

        private async void changeProfilePayment_Click(object sender, RoutedEventArgs e)
        {
            if (productProfiles.FindAll(x => x.select == true).Count != 0 && profilePaymentCmbx.SelectedIndex != -1)
            {
                var selectCmbx = (PaymentProfileData)profilePaymentCmbx.SelectedItem;
                foreach (var item in productProfiles.FindAll(x => x.select == true))
                {
                    bool check = false;
                    await Task.Run(() => { check = dataBase.SetNewProfileData(item.product_id, selectCmbx.id_profile); });
                    if (check)
                    {
                        productProfiles.Find(x => x == item).nameIp = selectCmbx.name;
                        list_product_grid.Items.Refresh();
                    }
                }
            }
            else
            {
                MessageBox.Show("Выберите продукт/ИП");
            }
        }

        private void selectAll_Checked(object sender, RoutedEventArgs e)
        {
            productProfiles.ForEach(x => x.select = true);
            list_product_grid.Items.Refresh();
            
        }

        private void selectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            productProfiles.ForEach(x => x.select = false);
            list_product_grid.Items.Refresh();
        }
    }
}
