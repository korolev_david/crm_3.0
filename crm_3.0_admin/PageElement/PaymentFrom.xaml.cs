﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{

    public partial class PaymentFrom : Page
    {
        public PaymentFrom(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
            Start();
        }

        DataBase dataBase;
        List<PaymentFromData> paymentFroms = new List<PaymentFromData>();
        List<PaymentFromData> paymentTamplates = new List<PaymentFromData>();

        private async void Start()
        {
            currency_cmbx.Items.Add("RUB");
            currency_cmbx.Items.Add("UAH");
            currency_cmbx.Items.Add("EUR");

           
            await Task.Run(() => paymentFroms = dataBase.getDataPaymentFrom());
            dataFromPayment.ItemsSource = paymentFroms.FindAll(x => x.confirm == null);
            await Task.Run(() => paymentTamplates = dataBase.getPaymentTampate());
            cmbxTamplateList.ItemsSource = paymentTamplates;
        }

        private void cmbxTamplateList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbxTamplateList.SelectedIndex != -1)
            {
                var item = (PaymentFromData)cmbxTamplateList.SelectedItem;
                requisites.Text = item.note;
                amount.Text = item.sum.ToString();
                currency_cmbx.Text = item.currency.ToUpper();
            }
        }

        private void saveTampate_Checked(object sender, RoutedEventArgs e)
        {
            gridNameTamplate.Visibility = Visibility.Visible;
        }

        private void saveTampate_Unchecked(object sender, RoutedEventArgs e)
        {
            gridNameTamplate.Visibility = Visibility.Hidden;
        }

        private async void sendForm_Click(object sender, RoutedEventArgs e)
        {
            if (currency_cmbx.SelectedIndex != -1 && amount.Text != string.Empty && requisites.Text != string.Empty)
            {
                string sum = amount.Text;
                string currency = currency_cmbx.Items[currency_cmbx.SelectedIndex].ToString();
                string note = requisites.Text;
                string customer = "Веклич Дмитрий";
                string templateName = nameTamplate.Text;

                await Task.Run(() => paymentFroms.Insert(0, dataBase.InsertPaymentFrom(sum,currency,note,customer,templateName)));
                dataFromPayment.ItemsSource = paymentFroms.FindAll(x => x.confirm == null);
                dataFromPayment.Items.Refresh();

                MessageBox.Show("Заявка принята.");
                if (templateName != string.Empty)
                {
                    await Task.Run(() => paymentTamplates = dataBase.getPaymentTampate());
                    cmbxTamplateList.ItemsSource = paymentTamplates;
                }
            }         
        }

        private async void dellTamplaet_Click(object sender, RoutedEventArgs e)
        {
            if (cmbxTamplateList.SelectedIndex != -1)
            { 
                var item = (PaymentFromData)cmbxTamplateList.SelectedItem;
                dataBase.DellTamplate(item.id);
                cmbxTamplateList.SelectedIndex = -1;
                MessageBox.Show("Шаблон удален.");
                await Task.Run(() => paymentTamplates = dataBase.getPaymentTampate());
                cmbxTamplateList.ItemsSource = paymentTamplates;

            }
        }

        private void backMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Content = null;
            this.Visibility = Visibility.Collapsed;
        }
    }
}
