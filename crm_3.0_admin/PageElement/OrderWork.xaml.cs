﻿using crm_3._0_admin.Class;
using crm_3._0_admin.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace crm_3._0_admin.PageElement
{
    /// <summary>
    /// Логика взаимодействия для OrderWork.xaml
    /// </summary>
    public partial class OrderWork : Page
    {
        Orders order = new Orders();
        DataBase dataBase;
        public OrderWork(Manager manager)
        {
            InitializeComponent();
            dataBase = new DataBase(manager.id);
        }

        private void id_score_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789 ,".IndexOf(e.Text) < 0;
        }

        private async void searchScore_Click(object sender, RoutedEventArgs e)
        {
            if (id_score.Text != string.Empty)
            {
                
                int id_ord = Convert.ToInt32(id_score.Text);
                order = await Task.Run(() => dataBase.getOrder(id_ord));
               
                OrdersDataGrid.ItemsSource = new List<Orders> { order };
                OrdersDataGrid.Items.Refresh();
                sum_add.Text = string.Empty;
                if (order != null)
                {
                    add_sum.Visibility = Visibility.Visible;
                }
                else
                {
                    add_sum.Visibility = Visibility.Hidden;
                }
            }
        }

        private void sum_add_KeyUp(object sender, KeyEventArgs e)
        {
            if (sum_add.Text != string.Empty)
            {
                if (order.sumSale < Convert.ToInt32(sum_add.Text))
                {
                    sum_add.Text = order.sumSale.ToString();
                }
            }
        }

        private async void add_payment_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Подтвердите внесение суммы: " + sum_add.Text + " RUB", "Внимание !", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                int sum = Convert.ToInt32(sum_add.Text);
                await Task.Run(() => dataBase.insertPayment(order.idOrder, sum));
                order = await Task.Run(() => dataBase.getOrder(order.idOrder));
                OrdersDataGrid.ItemsSource = new List<Orders> { order };
                OrdersDataGrid.Items.Refresh();
                sum_add.Text = string.Empty;
            }
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789 ,".IndexOf(e.Text) < 0;
        }
    }
}
